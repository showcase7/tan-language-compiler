import sys
from collections import namedtuple, deque

# ==============================================================================
#                               Lexer
# ==============================================================================

class Span:
    __slots__ = ["start", "end"]
    def __init__(self, start, end):
        self.start = start
        self.end = end
    
    def until(self, other_span):
        return Span(min(self.start, other_span.start), max(self.end, other_span.end))
    
    def line_info(self, text):
        lineno = 1
        line_start = 0
        pos = self.start
        for i, ch in enumerate(text):
            if i == pos:
                break
            if ch == '\n':
                lineno += 1
                line_start = i + 1
        col = len(text[line_start:pos]) + 1
        rem_lines = text[line_start:].splitlines()
        line = rem_lines[0] if rem_lines else ""
        return (lineno, col, line)
    
    def __repr__(self):
        return "{}..{}".format(self.start, self.end)
    
    def __str__(self):
        return repr(self)

"""pub fn show_source_span(source: &str, span: Span) {
    let (lineno, col, line) = line_info(span.start, &source);
    
    if lineno > 1 {
        let prev = source.lines().skip(lineno - 2).next().unwrap();
        println!("{:03}: {}", lineno - 1, prev);
    }
    
    println!("{:03}: {}", lineno, line);
    let len = span.end - span.start;
    let pad = iter::repeat(' ').take(col-1).collect::<String>();
    let mark = if len > 1 {
        iter::repeat('~').take(len - 1).collect::<String>()
    } else {
        String::new()
    };
    println!("     {}^{}", pad, mark);
    
    if let Some(line) = source.lines().skip(lineno).next() {
        println!("{:03}: {}", lineno+1, line);
    }
}"""


class Token:
    def __init__(self, kind, text, span):
        self.kind = kind
        self.text = text
        self.span = span
    
    def __str__(self):
        return f"Token(kind: {self.kind}, text: {self.text}, span: {self.span})"
    
    def __repr__(self):
        return str(self)

class TokenKind:
    Ident = "identifier"
    Num = "number"
    Unknown = "unknown"
    Space = "space"
    Newline = "newline"
    Comment = "comment"
    Eof = "end-of-file"


SYMBOLS = {
    "=": "Eq",
    "+": "Plus",
    "-": "Minus",
    "*": "Star",
    "/": "Slash",
    "%": "Percent",
    "+=": "PlusEq",
    "-=": "MinusEq",
    "*=": "StarEq",
    "/=": "SlashEq",
    "%=": "PercentEq",
    "==": "EqEq",
    "!=": "NotEq",
    "<": "Lt",
    "<=": "LtEq",
    ">": "Gt",
    ">=": "GtEq",
    "&&": "And",
    "||": "Or",
    ":": "Colon",
    "->": "Arrow",
    ",": "Comma",
    ".": "Dot",
    "..": "DotDot",
    "[": "BrackOpen",
    "]": "BrackClose",
    "(": "ParOpen",
    ")": "ParClose",
    "!": "Not",
}

MAX_SYMBOL_LEN = max(map(len, SYMBOLS.values()))
SYMBOL_STARTS = set(map(lambda s: s[0], SYMBOLS))

KEYWORDS = {
    "let",
    "mut",
    "fn",
    "for",
    "in",
    "while",
    "if",
    "else",
    "end",
    "do",
    "true",
    "false",
    "nil",
    "return",
    "break",
    "continue",
    "match",
    "type",
    "of",
    "new",
    "where",
    "panic",
}

for sym, name in SYMBOLS.items():
    setattr(TokenKind, name, sym)

for kw in KEYWORDS:
    setattr(TokenKind, kw.capitalize(), kw)

setattr(TokenKind, "True_", "true")
setattr(TokenKind, "False_", "false")

class Ref:
    __slots__ = ["value"]
    def __init__(self):
        self.value = None

def try_read_symbol(text, start, ref):
    sublen = MAX_SYMBOL_LEN
    while sublen > 0:
        s = text[start : start+sublen]
        if s in SYMBOLS:
            ref.value = s
            return True
        sublen -= 1
    return False

def can_start_ident(ch):
    return ch.isalpha() or ch == "_"

def can_continue_ident(ch):
    return ch.isalpha() or ch == "_"

def try_read_ident(text, start, ref):
    if can_start_ident(text[start]):
        i = start + 1
        while i < len(text) and can_continue_ident(text[i]):
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_space(text, start, ref):
    if text[start] == "\n":
        ref.value = "\n"
        return True
    elif text[start].isspace():
        i = start + 1
        while i < len(text) and text[i].isspace():
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_comment(text, start, ref):
    if text[start] == "#":
        i = start + 1
        while i < len(text) and text[i] != "\n":
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_number(text, start, ref):
    if text[start].isdigit():
        i = start + 1
        while i < len(text) and text[i].isdigit():
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def can_start_token(ch):
    return ch.isalpha() or ch.isspace() or ch == "#" or ch in SYMBOL_STARTS

class RawLexer:
    def __init__(self, text):
        self.text = text
        self.ref = Ref()
        self.start = 0
    
    def send_token(self, kind, text):
        end = self.start+len(text)
        token = Token(kind, text, Span(self.start, end))
        self.start = end
        return token
    
    def next_token(self):
        if self.start >= len(self.text):
            return self.send_token(TokenKind.Eof, "")
        
        elif try_read_symbol(self.text, self.start, self.ref):
            kind = getattr(TokenKind, SYMBOLS[self.ref.value])
            return self.send_token(kind, self.ref.value)
        
        elif try_read_ident(self.text, self.start, self.ref):
            if self.ref.value in KEYWORDS:
                kind = getattr(TokenKind, self.ref.value.capitalize())
                return self.send_token(kind, self.ref.value)
            else:
                return self.send_token(TokenKind.Ident, self.ref.value)
        
        elif try_read_space(self.text, self.start, self.ref):
            if self.ref.value == "\n":
                return self.send_token(TokenKind.Newline, self.ref.value)
            else:
                return self.send_token(TokenKind.Space, self.ref.value)
        
        elif try_read_comment(self.text, self.start, self.ref):
            return self.send_token(TokenKind.Comment, self.ref.value)
        
        elif try_read_number(self.text, self.start, self.ref):
            return self.send_token(TokenKind.Num, self.ref.value)
        
        else:
            i = self.start + 1
            while i < len(self.text) and not can_start_token(self.text[i]):
                i += 1
            return self.send_token(TokenKind.Unknown, self.text[self.start:i])


def should_be_filtered(token):
    return token.kind == TokenKind.Space or token.kind == TokenKind.Comment

class Lexer:
    def __init__(self, text):
        self.raw = RawLexer(text)
        self.prev_start = 0
        self.peek_tok = None
        self.was_newline = False
    
    def next(self):
        if self.peek_tok is None:
            token = self.raw.next_token()
            while should_be_filtered(token):
                token = self.raw.next_token()
            self.prev_start = token.span.start
            
        else:
            token = self.peek_tok
            self.peek_tok = None
        
        if token.kind == TokenKind.Newline:
            self.was_newline = True
        else:
            self.was_newline = False
        return token
    
    def peek(self):
        if self.peek_tok != None:
            return self.peek_tok
        else:
            token = self.raw.next_token()
            while should_be_filtered(token):
                token = self.raw.next_token()
            self.prev_start = token.span.start
            self.peek_tok = token
            return self.peek_tok
    
    def try_eat(self, kind, token_out):
        peek = self.peek()
        if peek.kind == kind:
            token_out.text = peek.text
            token_out.kind = peek.kind
            token_out.span = peek.span
            self.next()
            return True
        
        # If we cannot eat an expected newline, but there has just been one
        elif kind == TokenKind.Newline and self.was_newline: # TODO: assign
            self.was_newline = False
            return True
        
        # Skip newlines like whitespace in the lexer, if it is not the expected token.
        elif self.peek().kind == TokenKind.Newline:
            self.next()
            return self.try_eat(kind, token_out)
        
        else:
            return False
    
    def fail_expect(self, expected):
        tok = self.next()
        lineno, col, line = tok.span.line_info(self.raw.text)
        print(f"{lineno}:{col}: {line}\n{lineno}:{col}: Expected {expected}, type declaration or expression, found '{tok.text}'")
        raise Exception("Parse failed")
    
    def expect(self, kind, description):
        if kind == TokenKind.Newline and self.was_newline:
            return self.skip_tok
        
        if self.peek().kind != kind:
            self.fail_expect(description)

        return self.next()

# ==============================================================================
#                               AST
# ==============================================================================

def fmt_fields(fields):
    parts = []
    parts.append("(")
    last = len(fields) - 1
    for i, (field, typ) in enumerate(fields):
        parts.append(field)
        parts.append(": ")
        parts.append(typ.__name__)
        if i != last:
            parts.append(", ")
    parts.append(")")
    return "".join(parts)

class AstNode:
    pass

class Expr(AstNode):
    pass

def ast_node(name, supertype, fields):
    class Node(supertype):
        __name__ = name
        __slots__ = list(map(lambda f: f[0], fields))
        def __init__(self, *args):
            super()
            if len(args) != len(fields):
                raise Exception("Ast node '{}' takes args {}, got {}".format(
                    name, fmt_fields(fields), args
                ))
            
            for i, arg in enumerate(args):
                field, typ = fields[i]
                if not isinstance(arg, typ):
                    raise Exception("Ast node '{}' expected arg '{}' to be of type {}, got '{}' ({})".format(
                        name, field, typ.__name__, arg, type(arg).__name__
                    ))
                setattr(self, field, arg)
        
        def __repr__(self):
            parts = [name]
            parts.append("(")
            last = len(fields) - 1
            for i, (field, _) in enumerate(fields):
                parts.append(field)
                parts.append(": ")
                parts.append(str(getattr(self, field)))
                if i != last:
                    parts.append(", ")
            parts.append(")")
            return "".join(parts)
        
        def __str__(self):
            return repr(self)
        
        def fmt_into(self, sb, indent):
            sb.append(name)
            sb.append(" {\n")
            indent += 1
            last = len(fields) - 1
            for i, (field, _) in enumerate(fields):
                sb.append(indent * 2 * " ")
                sb.append(field)
                sb.append(": ")
                value = getattr(self, field)
                if isinstance(value, AstNode):
                    value.fmt_into(sb, indent)
                
                elif isinstance(value, list):
                    sb.append("[")
                    if value:
                        sb.append("\n")
                    for item in value:
                        sb.append((indent+1) * 2 * " ")
                        if isinstance(item, AstNode):
                            item.fmt_into(sb, indent+1)
                        else:
                            sb.append(str(item))
                        sb.append(",\n")
                    if value:
                        sb.append(indent * 2 * " ")
                    sb.append("]")
                else:
                    sb.append(str(value))
                
                if i != last:
                    sb.append(",")
                sb.append("\n")
            indent -= 1
            sb.append(indent * 2 * " ")
            sb.append("}")
        
        def fmt(self, indent=0):
            sb = []
            self.fmt_into(sb, indent)
            return "".join(sb)
            
        
    return Node

Let = ast_node("Let", Expr, [
    ("is_mut", bool),
    ("ident", str),
    ("expr", Expr),
    ("span", Span),
])

Op = ast_node("Op", Expr, [
    ("op", str),
    ("args", list),
    ("span", Span),
])

Num = ast_node("Num", Expr, [
    ("value", int),
    ("span", Span),
])

Bool = ast_node("Bool", Expr, [
    ("value", bool),
    ("span", Span),
])

Nil = ast_node("Nil", Expr, [
    ("span", Span),
])

Assign = ast_node("Assign", Expr, [
    ("loc", Expr),
    ("op", str),
    ("value", Expr),
    ("span", Span),
])

Un = ast_node("Un", Expr, [
    ("op", str),
    ("inner", Expr),
    ("span", Span),
])

Panic = ast_node("Panic", Expr, [
    ("message", str),
    ("span", Span)
])

TypeDec = ast_node("TypeDec", AstNode, [
    ("ident", str),
    ("fields", list),
    ("span", Span),
])

FunDec = ast_node("FunDec", AstNode, [
    ("ident", str),
    ("argdefs", list),
    ("ret", str),
    ("body", list),
    ("span", Span),
])

If = ast_node("If", Expr, [
    ("cond", Expr),
    ("ifbody", list),
    ("elsebody", list),
    ("span", Span),
])

Access = ast_node("Access", Expr, [
    ("loc", str),
    ("span", Span),
])

Type = ast_node("Type", AstNode, [
    ("name", str),
    ("args", list),
    ("span", Span),
])

New = ast_node("New", Expr, [
    ("type", Type),
    ("assignments", list),
    ("span", Span),
])

Call = ast_node("Call", Expr, [
    ("callee", Expr),
    ("args", list),
    ("span", Span),
])

Member = ast_node("Member", Expr, [
    ("object", Expr),
    ("field", str),
    ("span", Span),
])

For = ast_node("For", Expr, [
    ("var", str),
    ("iterable", object),
    ("body", list),
    ("span", Span),
])

Range = ast_node("Range", AstNode, [
    ("start", Expr),
    ("end", Expr),
])

While = ast_node("While", Expr, [
    ("cond", Expr),
    ("body", list),
    ("span", Span),
])

Do = ast_node("Do", Expr, [
    ("body", list),
    ("span", Span),
])

Indexing = ast_node("Indexing", Expr, [
    ("target", Expr),
    ("index", Expr),
    ("span", Span),
])

Panic = ast_node("Panic", Expr, [
    ("message", str),
    ("span", Span),
])

# ==============================================================================
#                             Utilities
# ==============================================================================

BINOPS = {
    "+", "-", "*", "/", "%",
    "==", "!=", "<", "<=", ">", ">=", 
    "||", "&&",
}

def is_binop(token):
    return token.text in BINOPS

PRECEDENCE = {
    "*": 3,
    "/": 3,
    "%": 3,
    "+": 4,
    "-": 4,
    "<": 6,
    "<=": 6,
    ">": 6,
    ">=": 6,
    "==": 7,
    "!=": 7,
    "&&": 11,
    "||": 12,
}

def order_binary_expr(stack):
    expr_stack = []
    op_stack = []
    
    def reduce_two():
        op = op_stack.pop()
        right = expr_stack.pop()
        left = expr_stack.pop()
        span = left.span.until(right.span)
        expr_stack.append(Op(op, [left, right], span))
    
    exprs = deque()
    ops = []
    for i in range(0, len(stack)):
        if i % 2 == 0:
            exprs.append(stack[i])
        else:
            ops.append(stack[i])
    
    expr_stack.append(exprs.popleft())
    for op in ops:
        prec = PRECEDENCE[op]
        while op_stack:
            prev_prec = PRECEDENCE[op_stack[-1]]
            if prev_prec < prec:
                reduce_two()
            else:
                break
        
        expr_stack.append(exprs.popleft())
        op_stack.append(op)
    
    while op_stack:
        reduce_two()
    
    return expr_stack.pop()

# ==============================================================================
#                               Parser
# ==============================================================================


ref = Ref()
token = Token(TokenKind.Eof, "", Span(0, 0))


def parse_if(lexer, ref):
    if not lexer.try_eat(TokenKind.If, token):
        return False
    
    start = token.span
    if not parse_expr(lexer, ref):
        lexer.fail_expect("condition expression after 'if'")
    cond = ref.value
    lexer.expect(TokenKind.Do, "'do' after condition")
    ifbody = parse_exprs(lexer)
    if lexer.try_eat(TokenKind.Else, token):
        elsebody = parse_exprs(lexer)
    else:
        elsebody = []
    end = lexer.expect(TokenKind.End, "'end' to end 'if'-statement")
    span = start.until(end.span)
    ref.value = If(cond, ifbody, elsebody, span)
    return True


def parse_num(lexer, ref):
    if lexer.try_eat(TokenKind.Num, token):
        ref.value = Num(int(token.text), token.span)
        return True
    else:
        return False


def parse_bool(lexer, ref):
    if lexer.try_eat(TokenKind.True_, token):
        ref.value = Bool(True, token.span)
        return True
    elif lexer.try_eat(TokenKind.False_, token):
        ref.value = Bool(False, token.span)
        return True
    else:
        return False


def parse_var(lexer, ref):
    if lexer.try_eat(TokenKind.Ident, token):
        ref.value = Access(token.text, token.span)
        return True
    else:
        return False


def parse_let(lexer, ref):
    if not lexer.try_eat(TokenKind.Let, token):
        return False
    
    first = token.span
    if lexer.try_eat(TokenKind.Mut, token):
        mut = True
    else:
        mut = False
    
    ident = lexer.expect(TokenKind.Ident, "variable name after 'let'").text
    lexer.expect(TokenKind.Eq, "'=' after variable name")
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after '='")
    expr = ref.value
    span = first.until(expr.span)
    ref.value = Let(mut, ident, expr, span)
    return True


def parse_type(lexer, ref):
    if not lexer.try_eat(TokenKind.Ident, token):
        return False
    name = token.text
    start = token.span
    args = []
    if lexer.try_eat(TokenKind.BrackOpen, token):
        if not parse_type(lexer, ref):
            fail_expect("type after '['")
        args.append(ref.value)
        while True:
            if not lexer.try_eat(TokenKind.Comma, token):
                break
            elif not parse_type(lexer, ref):
                break
            args.append(ref.value)
        end = lexer.expect(TokenKind.BrackClose, "closing ']'")
        span = start.until(end.span)
    else:
        span = start
    
    ref.value = Type(name, args, span)
    return True


def parse_field_assignment(lexer, ref):
    if not lexer.try_eat(TokenKind.Ident, token):
        return False
    field = token.text
    lexer.expect(TokenKind.Eq, "'=' after field name")
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after '='")
    expr = ref.value
    ref.value = (field, expr)
    return True


def parse_new(lexer, ref):
    if not lexer.try_eat(TokenKind.New, token):
        return False
    
    first = token.span
    if not parse_type(lexer, ref):
        lexer.fail_expect("type after 'new'")
    
    typ = ref.value
    
    if lexer.try_eat(TokenKind.End, token):
        span = first.until(token.span)
        ref.value = New(typ, [], span)
        return True

    lexer.expect(TokenKind.Where, "'where' or 'end' after type")
    if not parse_field_assignment(lexer, ref):
        lexer.fail_expect("field assignment")
    
    assignments = [ref.value]
    while True:
        if not lexer.try_eat(TokenKind.Newline, token):
            break
        if not parse_field_assignment(lexer, ref):
            break
        assignments.append(ref.value)
    
    end = lexer.expect(TokenKind.End, "'end' to end 'new'-expression")
    span = first.until(end.span)
    ref.value = New(typ, assignments, span)
    return True


def parse_pargroup(lexer, ref):
    if not lexer.try_eat(TokenKind.ParOpen, token):
        return False
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after '('")
    expr = ref.value
    lexer.expect(TokenKind.ParClose, "closing ')' after expression")
    ref.value = expr
    return True


def parse_for(lexer, ref):
    if not lexer.try_eat(TokenKind.For, token):
        return False
    start = token.span
    var = lexer.expect(TokenKind.Ident, "var after 'for").text
    lexer.expect(TokenKind.In, "'in' after loop variable")
    if not parse_expr(lexer, ref):
        lexer.fail_expect("expression after 'in'")
    first = ref.value
    if lexer.try_eat(TokenKind.DotDot, token):
        if not parse_expr(lexer, ref):
            lexer.fail_expect("expression after '..'")
        second = ref.value
        iterable = Range(first, second)
    else:
        iterable = first
    lexer.expect(TokenKind.Do, "'do' after iterable expression")
    body = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end 'for'-statement")
    span = start.until(end.span)
    ref.value = For(var, iterable, body, span)
    return True


def parse_while(lexer, ref):
    if not lexer.try_eat(TokenKind.While, token):
        return False
    start = token.span
    if not parse_expr(lexer, ref):
        lexer.fail_expect("condition expression after 'while'")
    cond = ref.value
    lexer.expect(TokenKind.Do, "'do' after condition")
    body = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end 'while'-statement")
    span = start.until(end.span)
    ref.value = While(cond, body, span)
    return True


def parse_do(lexer, ref):
    if not lexer.try_eat(TokenKind.Do, token):
        return False
    start = token.span
    exprs = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end 'do'-expression")
    span = start.until(end.span)
    ref.value = Do(exprs, span)
    return True


def parse_nil(lexer, ref):
    if lexer.try_eat(TokenKind.Nil, token):
        ref.value = Nil(token.span)
        return True
    else:
        return False


def parse_panic(lexer, ref):
    if not lexer.try_eat(TokenKind.Panic, token):
        return False
    start = token.span
    message = lexer.expect(TokenKind.Ident, "identifier as panic message")
    span = start.until(message.span)
    ref.value = Panic(message.text, span)
    return True


ATOMS = [
    parse_num, parse_bool, parse_nil, parse_var, parse_if, parse_let, 
    parse_pargroup, parse_new, parse_for, parse_while, parse_do, parse_panic,
]
def parse_atom(lexer, ref):
    for atom_parser in ATOMS:
        if atom_parser(lexer, ref):
            return True
    
    return False


UNOPS = {TokenKind.Minus, TokenKind.Plus, TokenKind.Not}
def parse_unary_ops(lexer):
    ops = []
    while True:
        if lexer.peek().kind in UNOPS:
            ops.append(lexer.next().text)
        else:
            break
    
    return ops


def parse_single(lexer, ref):
    ops = parse_unary_ops(lexer)
    if not parse_atom(lexer, ref):
        if ops:
            lexer.fail_expect("expression after unary operators")
        else:
            return False
    
    expr = ref.value

    # Parse trailing attributes
    while True:
        # Call
        if lexer.try_eat(TokenKind.ParOpen, token):
            args = []
            while True:
                if not parse_expr(lexer, ref):
                    break
                args.append(ref.value)
                if not lexer.try_eat(TokenKind.Comma, token):
                    break

            end = lexer.expect(TokenKind.ParClose, token)
            span = expr.span.until(end.span)
            expr = Call(expr, args, span)
        
        # Member access
        elif lexer.try_eat(TokenKind.Dot, token):
            if not lexer.try_eat(TokenKind.Ident, token):
                lexer.fail_expect("field name after '.'")
            field = token.text
            span = expr.span.until(token.span)
            expr = Member(expr, field, span)
        
        # Indexing
        elif lexer.try_eat(TokenKind.BrackOpen, token):
            if not parse_expr(lexer, ref):
                lexer.fail_expect("index expression after '['")
            index = ref.value
            end = lexer.expect(TokenKind.BrackClose, "closing ']' after index expression")
            span = expr.span.until(end.span)
            expr = Indexing(expr, index, span)

        else:
            break

    
    # Apply unary operators after the trail modifiers
    ops.reverse()
    for op in ops:
        expr = Op(op, [expr], expr.span) # bad span usage
    
    ref.value = expr
    return True


BINOPS = {
    TokenKind.Plus, TokenKind.Minus, TokenKind.Slash, TokenKind.Star,
    TokenKind.Lt, TokenKind.LtEq, TokenKind.Gt, TokenKind.GtEq, TokenKind.EqEq, TokenKind.NotEq,
    TokenKind.And, TokenKind.Or,
}
def parse_binop(lexer, ref):
    if lexer.peek().kind in BINOPS:
        ref.value = lexer.next().text
        return True

    return False


ASSOPS = {
    TokenKind.Eq, TokenKind.PlusEq, TokenKind.MinusEq, 
    TokenKind.SlashEq, TokenKind.StarEq, TokenKind.PercentEq,
}
def parse_assop(lexer, ref):
    if lexer.peek().kind in ASSOPS:
        ref.value = lexer.next().text
        return True
    
    return False


def parse_expr(lexer, ref):
    if not parse_single(lexer, ref):
        return False

    first = ref.value
    if parse_assop(lexer, ref):
        loc = first
        op = ref.value
        if not parse_single(lexer, ref):
            lexer.fail_expect("expression")
        
        expr = ref.value
        ref.value = Assign(loc, op, expr, first.span.until(expr.span))
        return True
    
    parts = [first]
    while True:
        if not parse_binop(lexer, ref):
            break
        
        parts.append(ref.value)
        if not parse_single(lexer, ref):
            lexer.fail_expect("expression")
        
        parts.append(ref.value)

    expr = order_binary_expr(parts)
    ref.value = expr
    return True


def parse_exprs(lexer):
    exprs = []
    while True:
        while lexer.try_eat(TokenKind.Newline, token):
            continue
        
        if parse_expr(lexer, ref):
            exprs.append(ref.value)
            if not lexer.try_eat(TokenKind.Newline, token):
                break
        else:
            break
    
    return exprs


def parse_typed_ident(lexer, ref):
    if not lexer.try_eat(TokenKind.Ident, token):
        return False
    ident = token.text
    lexer.expect(TokenKind.Colon, "':' after identifier")
    if not parse_type(lexer, ref):
        lexer.fail_expect("type after ':'")
    typ = ref.value
    ref.value = (ident, typ)
    return True


def parse_argdefs(lexer):
    if not parse_typed_ident(lexer, ref):
        return []
    
    argdefs = [ref.value]
    while True:
        if not lexer.try_eat(TokenKind.Comma, token):
            break
        if not parse_typed_ident(lexer, ref):
            break
        argdefs.append(ref.value)
    
    return argdefs
    


def parse_fundec(lexer, ref):
    if not lexer.try_eat(TokenKind.Fn, token):
        return False
    
    start = token.span
    name = lexer.expect(TokenKind.Ident, "function name").text
    lexer.expect(TokenKind.ParOpen, "'('")
    argdefs = parse_argdefs(lexer)
    lexer.expect(TokenKind.ParClose, "')'")
    ret = ""
    if not lexer.try_eat(TokenKind.Newline, token): # Check for newline first so it isn't eaten.
        if lexer.try_eat(TokenKind.Arrow, token):
            ret = lexer.expect(TokenKind.Ident, "return type").text
            lexer.expect(TokenKind.Newline, "newline after function signature")
    
    body = parse_exprs(lexer)
    end = lexer.expect(TokenKind.End, "'end' to close function declaration")
    span = start.until(end.span)
    ref.value = FunDec(name, argdefs, ret, body, span)
    return True


def parse_fields(lexer):
    print("Parsing fields...")
    if not parse_typed_ident(lexer, ref):
        print("  Could not parse first typed ident")
        return []
    
    fields = [ref.value]
    while True:
        if not lexer.try_eat(TokenKind.Newline, token):
            print("  No newline separator")
            break
        if not parse_typed_ident(lexer, ref):
            print("  No typed ident")
            break
        fields.append(ref.value)
    
    return fields
        


def parse_typedec(lexer, ref):
    if not lexer.try_eat(TokenKind.Type, token):
        return False
    
    start = token.span
    name = lexer.expect(TokenKind.Ident, "type name").text
    if lexer.try_eat(TokenKind.End, token):
        span = start.until(token.span)
        ref.value = TypeDec(name, [], span)
        return True
    
    lexer.expect(TokenKind.Of, "'of' or 'end' after type name")
    fields = parse_fields(lexer)
    end = lexer.expect(TokenKind.End, "'end' to end type declaration")
    span = start.until(end.span)
    ref.value = TypeDec(name, fields, span)
    return True


def parse_toplevel(lexer):
    items = []
    while True:
        if lexer.try_eat(TokenKind.Eof, token):
            break
        
        if parse_fundec(lexer, ref):
            items.append(ref.value)

        elif parse_typedec(lexer, ref):
            items.append(ref.value)

        elif parse_expr(lexer, ref):
            items.append(ref.value)
        
        else:
            lexer.fail_expect("function declaration")

    return items


def parse(text):
    lexer = Lexer(text)
    items = parse_toplevel(lexer)
    lexer.expect(TokenKind.Eof, "end-of-file")
    return items





# ==============================================================================
#                               Main / CLI
# ==============================================================================
"""
Notes: use the 'immutables' Python library for HAMTs to represent state in the 
program analysis part.

"""


def main():
    args = sys.argv[1:]
    if len(args) < 1:
        print("Usage: python3 simpleparse.py <tanfile>")
        sys.exit(1)

    with open(args[0]) as f:
        source = f.read()
    
    lexer = Lexer(source)
    print("Tokens:")
    while True:
        token = lexer.next()
        text = token.text if token.kind != TokenKind.Newline else '\\n'
        print(f"  '{text}' ({token.kind}) [{token.span}]")
        if token.kind == TokenKind.Eof:
            break
    print("")

    items = parse(source)

    print("Top-level items:")
    for item in items:
        print(f"  {item.fmt(indent=1)}")


if __name__ == "__main__":
    main()

