# Tan language compiler
## Description
This is a project to write a compiler for my personal programming language, at this point codenamed "tanuki".

It can compile programs written in the language into bytecode for a stack-based VM and run it.

There's also an end-to-end test runner that runs the compiler on all the code samples in the test folder and checks their results and errors against expected values.

A description of the language syntax can be found in `grammar.txt`, and samples of language usage can be found in the `test` folder.

## Building and running the project
The project requires the Rust package manager `cargo` to build.

The project can be built through `$ cargo build --release`.

The project can then either be run from the generated executables in the `target/release` folder, or through `$ cargo run --release --bin <tan | test> -- <args...>`.

To run the compiler and print a lot of debug information for the test at `test/arith/arith.tan`, run
`$ cargo run --release --bin tan -- debug test/arith/arith.tan`

To run the end-to-end tests, run 
`$ cargo run --release --bin test`.

## License
This project is available under the Universal Permissive License, Version 1.0 ([LICENSE.txt]) or https://oss.oracle.com/licenses/upl/ .
