
import sys
from collections import namedtuple, deque

# ==============================================================================
#                               Lexer
# ==============================================================================

class Span:
    __slots__ = ["start", "end"]
    def __init__(self, start, end):
        self.start = start
        self.end = end
    
    def until(self, other_span):
        return Span(min(self.start, other_span.start), max(self.end, other_span.end))
    
    def __repr__(self):
        return "{}..{}".format(self.start, self.end)
    
    def __str__(self):
        return repr(self)

Token = namedtuple("Token", ["kind", "text", "span"])

class TokenKind:
    Ident = "identifier"
    Num = "number"
    Unknown = "unknown"
    Space = "space"
    Newline = "newline"
    Comment = "comment"
    Eof = "end-of-file"


SYMBOLS = {
    "+": "Plus",
    "-": "Minus",
    "*": "Star",
    "/": "Slash",
    "+=": "PlusEq",
    "-=": "MinusEq",
    "*=": "StarEq",
    "/=": "SlashEq",
    "==": "EqEq",
    "!=": "NotEq",
    "<": "Lt",
    "<=": "LtEq",
    ">": "Gt",
    ">=": "GtEq",
    "&&": "AndAnd",
    "||": "OrOr",
    ":": "Colon",
    "->": "Arrow",
    ",": "Comma",
    ".": "Dot",
    "[": "BrackOpen",
    "]": "BrackClose",
    "(": "ParOpen",
    ")": "ParClose",
}

MAX_SYMBOL_LEN = max(map(len, SYMBOLS.values()))
SYMBOL_STARTS = set(map(lambda s: s[0], SYMBOLS))

KEYWORDS = {
    "let",
    "mut",
    "fun",
    "for",
    "if",
    "else",
    "end",
    "in",
    "do",
    "true",
    "false",
    "nil",
    "return",
    "break",
    "continue",
    "match",
}

for sym, name in SYMBOLS.items():
    setattr(TokenKind, name, sym)

for kw in KEYWORDS:
    setattr(TokenKind, kw.capitalize(), kw)

class Ref:
    __slots__ = ["value"]
    def __init__(self):
        self.value = None

def try_read_symbol(text, start, ref):
    sublen = MAX_SYMBOL_LEN
    while sublen > 0:
        s = text[start : start+sublen]
        if s in SYMBOLS:
            ref.value = s
            return True
        sublen -= 1
    return False

def can_start_ident(ch):
    return ch.isalpha() or ch == "_"

def can_continue_ident(ch):
    return ch.isalpha() or ch == "_"

def try_read_ident(text, start, ref):
    if can_start_ident(text[start]):
        i = start + 1
        while i < len(text) and can_continue_ident(text[i]):
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_space(text, start, ref):
    if text[start] == "\n":
        ref.value = "\n"
        return True
    elif text[start].isspace():
        i = start + 1
        while i < len(text) and text[i].isspace():
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_comment(text, start, ref):
    if text[start] == "#":
        i = start + 1
        while i < len(text) and text[i] != "\n":
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def try_read_number(text, start, ref):
    if text[start].isdigit():
        i = start + 1
        while i < len(text) and text[i].isdigit():
            i += 1
        ref.value = text[start:i]
        return True
    else:
        return False

def can_start_token(ch):
    return ch.isalpha() or ch.isspace() or ch == "#" or ch in SYMBOL_STARTS

class RawLexer:
    def __init__(self, text):
        self.text = text
        self.ref = Ref()
        self.start = 0
    
    def send_token(self, kind, text):
        end = self.start+len(text)
        token = Token(kind, text, Span(self.start, end))
        self.start = end
        return token
    
    def next_token(self):
        if self.start >= len(self.text):
            return self.send_token(TokenKind.Eof, "")
        
        elif try_read_symbol(self.text, self.start, self.ref):
            kind = getattr(TokenKind, SYMBOLS[self.ref.value])
            return self.send_token(kind, self.ref.value)
        
        elif try_read_ident(self.text, self.start, self.ref):
            if self.ref.value in KEYWORDS:
                kind = getattr(TokenKind, self.ref.value.capitalize())
                return self.send_token(kind, self.ref.value)
            else:
                return self.send_token(TokenKind.Ident, self.ref.value)
        
        elif try_read_space(self.text, self.start, self.ref):
            if self.ref.value == "\n":
                return self.send_token(TokenKind.Newline, self.ref.value)
            else:
                return self.send_token(TokenKind.Space, self.ref.value)
        
        elif try_read_comment(self.text, self.start, self.ref):
            return self.send_token(TokenKind.Comment, self.ref.value)
        
        elif try_read_number(self.text, self.start, self.ref):
            return self.send_token(TokenKind.Num, self.ref.value)
        
        else:
            i = self.start + 1
            while i < len(self.text) and not can_start_token(self.text[i]):
                i += 1
            return self.send_token(TokenKind.Unknown, self.text[self.start:i])


def should_be_filtered(token):
    return token.kind == TokenKind.Space or token.kind == TokenKind.Comment

class Lexer:
    def __init__(self, text):
        self.raw = RawLexer(text)
        self.prev_start = 0
    
    def next_token(self):
        token = self.raw.next_token()
        while should_be_filtered(token):
            token = self.raw.next_token()
        self.prev_start = token.span.start
        return token
    
    def undo(self):
        self.raw.start = self.prev_start

# ==============================================================================
#                               AST
# ==============================================================================

def fmt_fields(fields):
    parts = []
    parts.append("(")
    last = len(fields) - 1
    for i, (field, typ) in enumerate(fields):
        parts.append(field)
        parts.append(": ")
        parts.append(typ.__name__)
        if i != last:
            parts.append(", ")
    parts.append(")")
    return "".join(parts)

class AstNode:
    pass

class Expr(AstNode):
    pass

class Stmt(AstNode):
    pass

def ast_node(name, supertype, fields):
    class Node(supertype):
        __name__ = name
        __slots__ = list(map(lambda f: f[0], fields))
        def __init__(self, *args):
            super()
            if len(args) != len(fields):
                raise Exception("Ast node '{}' takes args {}, got {}".format(
                    name, fmt_fields(fields), args
                ))
            
            for i, arg in enumerate(args):
                field, typ = fields[i]
                if not isinstance(arg, typ):
                    raise Exception("Ast node '{}' expected arg '{}' to be of type {}, got '{}' ({})".format(
                        name, field, typ.__name__, arg, type(arg).__name__
                    ))
                setattr(self, field, arg)
        
        def __repr__(self):
            parts = [name]
            parts.append("(")
            last = len(fields) - 1
            for i, (field, _) in enumerate(fields):
                parts.append(field)
                parts.append(": ")
                parts.append(str(getattr(self, field)))
                if i != last:
                    parts.append(", ")
            parts.append(")")
            return "".join(parts)
        
        def __str__(self):
            return repr(self)
        
        def fmt_into(self, sb, indent):
            sb.append(name)
            sb.append(" {\n")
            indent += 1
            last = len(fields) - 1
            for i, (field, _) in enumerate(fields):
                sb.append(indent * 2 * " ")
                sb.append(field)
                sb.append(": ")
                value = getattr(self, field)
                if isinstance(value, AstNode):
                    value.fmt_into(sb, indent)
                else:
                    sb.append(str(value))
                
                if i != last:
                    sb.append(",")
                sb.append("\n")
            indent -= 1
            sb.append(indent * 2 * " ")
            sb.append("}")
        
        def fmt(self, indent=0):
            sb = []
            self.fmt_into(sb, indent)
            return "".join(sb)
            
        
    return Node

Let = ast_node("Let", Stmt, [
    ("is_mut", bool),
    ("ident", str),
    ("value", Expr),
    ("span", Span),
])

Bin = ast_node("Bin", Expr, [
    ("left", Expr),
    ("op", str),
    ("right", Expr),
    ("span", Span),
])

Num = ast_node("Num", Expr, [
    ("value", int),
    ("span", Span),
])

Un = ast_node("Un", Expr, [
    ("op", str),
    ("expr", Expr),
    ("span", Span),
])

Panic = ast_node("Panic", Expr, [
    ("message", str),
    ("span", Span)
])

ExprStmt = ast_node("ExprStmt", Stmt, [
    ("expr", Expr),
    ("span", Span),
])



# ==============================================================================
#                               Samples
# ==============================================================================
SAMPLES = {
    "stmt": [
        "let mut",
        "let mut a",
        "let mut a =",
        "let mut a = 2 + true? 3: 5",
    ],
    "expr": [
        "2 +",
        "2 + <",
        "2 + <3",
    ]
}

# ==============================================================================
#                               Parser
# ==============================================================================

BINOPS = {
    "+", "-", "*", "/", 
    "==", "!=", "<", "<=", ">", ">=", 
    "||", "&&",
}

def is_binop(token):
    return token.text in BINOPS

PRECEDENCE = {
    "*": 3,
    "/": 3,
    "%": 3,
    "+": 4,
    "-": 4,
    "<": 6,
    "<=": 6,
    ">": 6,
    ">=": 6,
    "==": 7,
    "!=": 7,
    "&&": 11,
    "||": 12,
}

def order_binary_expr(stack):
    expr_stack = []
    op_stack = []
    
    def reduce_two():
        op = op_stack.pop()
        right = expr_stack.pop()
        left = expr_stack.pop()
        span = left.span.until(right.span)
        expr_stack.append(Bin(left, op, right, span))
    
    exprs = deque()
    ops = []
    for i in range(0, len(stack)):
        if i % 2 == 0:
            exprs.append(stack[i])
        else:
            ops.append(stack[i])
    
    expr_stack.append(exprs.popleft())
    for op in ops:
        prec = PRECEDENCE[op]
        while op_stack:
            prev_prec = PRECEDENCE[op_stack[-1]]
            if prev_prec < prec:
                reduce_two()
            else:
                break
        
        expr_stack.append(exprs.popleft())
        op_stack.append(op)
    
    while op_stack:
        reduce_two()
    
    return expr_stack.pop()


def panic_stmt(message, span):
    return ExprStmt(Panic(message, span), span)


class Parser:
    def __init__(self, text):
        self.lexer = Lexer(text)
        self.scopes = []
        self.sync_enders = set()
        self.ref = Ref()
        
    def parse(self):
        pass
    
    def sync(self):
        print("Syncing....")
        token = self.next_token()
        while True:
            if token.kind == TokenKind.Eof:
                break
            elif token.kind in self.sync_enders:
                self.undo()
                break
            token = self.lexer.next_token()
    
    def push_scope(self, *enders):
        self.scopes.append(self.sync_enders)
        self.sync_enders = set(enders)
    
    def pop_scope(self):
        sync_ender = self.scopes.pop()
        self.sync_enders = sync_ender
    
    def add_sync_ender(self, ender):
        self.sync_enders.add(ender)
    
    def parse_single(self):
        token = self.next_token()
        # Read unary ops
        unops = []
        while True:
            if token.text == "-":
                unops.append(token)
                token = self.lexer.next_token()
            else:
                break
            
        # Read the expression
        if token.kind == TokenKind.Num:
            expr = Num(int(token.text), token.span)
        else:
            expr = Panic("Could not parse expr :c", token.span)
            self.undo()
            self.sync()
            
        if unops:
            unops.reverse()
            for token in unops:
                expr = Un(token.text, expr, token.span.until(expr.span))
        return expr
    
    def next_token(self):
        return self.lexer.next_token()
    
    def undo(self):
        self.lexer.undo()
    
    def parse_expr(self):
        self.push_scope(TokenKind.Newline)
        expr = self.parse_single()
        
        token = self.next_token()
        if is_binop(token):
            stack = [expr]
            while is_binop(token):
                stack.append(token.text)
                stack.append(self.parse_single())
                token = self.next_token()
            self.undo()
            return order_binary_expr(stack)
        else:
            self.undo()
            return expr
        
        Sym = "symbol"
        Ident = "identifier"
        Kw = "keyword"
        Num = "number"
        Unknown = "unknown"
        Space = "space"
        Newline = "newline"
        Comment = "comment"
        Eof = "end-of-file"
    
    def eat(self, kind, ref):
        token = self.next_token()
        if token.kind == kind:
            ref.value = token
            return True
        else:
            self.undo()
            return False
    
    def next_span(self):
        token = self.next_token()
        span = token.span
        self.undo()
        return span
    
    def parse_stmt(self):
        token = self.next_token()
        if token.kind == TokenKind.Let:
            start = token.span
            is_mut = False
            if self.eat(TokenKind.Mut, self.ref):
                is_mut = True
            
            if not self.eat(TokenKind.Ident, self.ref):
                stmt = panic_stmt("Expected identifier in 'let'", self.next_span())
                self.sync()
                return stmt
            
            ident = self.ref.value.text
            if not self.eat(TokenKind.Colon, self.ref):
                self.sync()
                expr = Panic("Could not parse let", start.until(self.next_span()))
                return Let(is_mut, ident, expr, start.until(token.span))
                
        else:
            # if can start expr
            self.undo()
            expr = self.parse_expr()
            return ExprStmt(expr, expr.span)

# ==============================================================================
#                               Main
# ==============================================================================





def main():
    
    """lexer = Lexer(text)
    print("Tokens:")
    while True:
        token = lexer.next_token()
        print(f"{token}")
        if token.kind == TokenKind.Eof:
            break"""
    
    print("Attempting to parse samples")
    for kind, samples in sorted(SAMPLES.items()):
        for sample in samples:
            parser = Parser(sample)
            print(f"Parsing {kind} '{sample}'...")
            if kind == "expr":
                res = parser.parse_expr()
            elif kind == "stmt":
                res = parser.parse_stmt()
            else:
                res = None
            print("=> {}".format(res.fmt()))
            print("")
    
    #exp = Parser("2 + 2 * 4 / 3 - 5").parse_expr()
    #print("exp: {}".format(exp.fmt()))
    
    expr = Bin(Num(2, Span(0,0)), "+", Num(2, Span(2,2)), Span(0, 2))
    let = Let(True, "a", expr, Span(0,3))
    print("let: {}".format(let))
    print("fmt: {}".format(let.fmt()))



if __name__ == '__main__':
    main()



