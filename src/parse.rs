
use crate::lex::{Lexer, Token, TokenKind, LexError};
use crate::ast::*;
use crate::util::Span;

#[derive(Debug, Clone)]
pub struct ParseError {
    pub message: String,
    pub pos: usize,
    pub lineno: usize,
    pub col: usize,
    pub line: String,
}

impl ParseError {
    fn new(text: &str, pos: usize, message: String) -> ParseError {
        let mut lineno = 1;
        let mut line_start = 0;
        for (i, ch) in text.char_indices() {
            if i == pos {
                break;
            }
            if ch == '\n' {
                lineno += 1;
                line_start = i+1;
            }
        }
        let col = (&text[line_start..pos]).chars().count() + 1;
        let line = (&text[line_start..]).lines().next().unwrap_or("").to_string();
        ParseError {
            message: message,
            pos: pos,
            lineno: lineno,
            col: col,
            line: line,
        }
    }
}

impl From<LexError> for ParseError {
    fn from(err: LexError) -> ParseError {
        ParseError {
            message: err.message,
            pos: err.pos,
            lineno: err.lineno,
            col: err.col,
            line: err.line,
        }
    }
}


pub type ParseResult<T> = Result<T, ParseError>;

#[derive(Debug)]
struct Parser<'src> {
    lexer: Lexer<'src>,
    text: &'src str,
    cache: Option<Token>,
}

impl<'src> Parser<'src> {
    fn new(text: &'src str) -> Parser<'src> {
        Parser {
            text: text,
            lexer: Lexer::new(text),
            cache: None,
        }
    }
    
    fn peek(&mut self) -> Result<Token, LexError> {
        if let Some(token) = self.cache {
            return Ok(token);
        }
        self.cache = match self.lexer.next() {
            Ok(token) => Some(token),
            Err(err) => return Err(err),
        };
        Ok(self.cache.unwrap())
    }
    
    #[allow(unused)]
    fn peek_is(&mut self, kind: TokenKind) -> Result<bool, LexError> {
        let peek = self.peek()?;
        Ok(peek.kind == kind)
    }
    
    fn try_eat(&mut self, kind: TokenKind) -> Result<Option<Token>, LexError> {
        let peek = self.peek()?;
        if peek.kind == kind {
            Ok(Some(self.advance().unwrap()))
        } else {
            Ok(None)
        }
    }
    
    fn advance(&mut self) -> Result<Token, LexError> {
        self.cache.take().map(|t| Ok(t)).unwrap_or_else(|| self.lexer.next())
    }
    
    fn expect<S: Into<String>>(&mut self, kind: TokenKind, description: S) -> ParseResult<Token> {
        let token = self.advance()?;
        if token.kind == kind {
            Ok(token)
        } else {
            self.error(token.span.start, description)
        }
    }
    
    fn error<T, S: Into<String>>(&self, pos: usize, message: S) -> ParseResult<T> {
        Err(ParseError::new(self.text, pos, message.into()))
    }
    
    fn is_at_end(&mut self) -> ParseResult<bool> {
        Ok(self.peek()?.kind == TokenKind::Eof)
    }
    
    // =================== Parsing Functions =============================
    
    /// Parses a binary expression.
    fn parse_expr(&mut self, first: Option<Expr>) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let mut exprs = Vec::new();
        let mut ops = Vec::new();
        let first = if let Some(expr) = first {
            self.parse_tail(expr)?
        } else {
            self.parse_single()?
        };
        exprs.push(first);
        
        loop {
            let peek = self.peek()?;
            let bin_op = match peek.kind {
                Plus => BinOp::Add,
                Minus => BinOp::Sub,
                Star => BinOp::Mul,
                Slash => BinOp::Div,
                Percent => BinOp::Mod,
                EqEq => BinOp::Eq,
                NotEq => BinOp::NotEq,
                Lt => BinOp::Lt,
                LtEq => BinOp::LtEq,
                Gt => BinOp::Gt,
                GtEq => BinOp::GtEq,
                Or => BinOp::Or,
                And => BinOp::And,
                _ => break,
            };
            let _ = self.advance();
            ops.push(bin_op);
            let expr = self.parse_single()?;
            exprs.push(expr);
        }
        
        /// Shunting yard algorithm to order binary expressions by precedence.
        fn reduce_expr(mut exprs: Vec<Expr>, ops: Vec<BinOp>) -> Expr {
            
            let mut expr_stack = Vec::new();
            let mut op_stack = Vec::new();
            
            fn precedence(op: BinOp) -> usize {
                use crate::ast::BinOp::*;
            
                match op {
                    Mul => 3,
                    Div => 3,
                    Mod => 3,
                    
                    Add => 4,
                    Sub => 4,
                    
                    Lt => 6,
                    LtEq => 6,
                    Gt => 6,
                    GtEq => 6,
                    
                    Eq => 7,
                    NotEq => 7,
                    
                    And => 11,
                    Or => 12,
                }
            }
            
            fn reduce_two(exprs: &mut Vec<Expr>, ops: &mut Vec<BinOp>) {
                //println!("Reducing: exprs: {:?}, ops: {:?}", exprs, ops);
                let op = ops.pop().unwrap();
                let right = exprs.pop().unwrap();
                let left = exprs.pop().unwrap();
                let span = left.span().until(right.span());
                let expr = Expr::Binary(Box::new(left), op, Box::new(right), span);
                exprs.push(expr);
            }
            
            let mut exprs = exprs.drain(..);
            
            expr_stack.push(exprs.next().unwrap());
            for &op in &ops {
                let prec = precedence(op);
                while let Some(&prev_op) = op_stack.last() {
                    if precedence(prev_op) < prec {
                        reduce_two(&mut expr_stack, &mut op_stack);
                    } else {
                        break;
                    }
                }
                
                let expr = exprs.next().unwrap();
                expr_stack.push(expr);
                
                op_stack.push(op);
            }
            
            while ! op_stack.is_empty() {
                reduce_two(&mut expr_stack, &mut op_stack);
            }
            
            expr_stack.pop().unwrap()
        }
        
        Ok(reduce_expr(exprs, ops))
    }
    
    /// Parses the non-atomic parts of an expression
    fn parse_tail(&mut self, expr: Expr) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let mut expr = Some(expr);
        // Try to parse a call... or more
        loop {
            if let Some(token) = self.try_eat(ParOpen)? {
                let (args, end) = self.parse_args()?;
                let span = token.span().until(end);
                expr = Some(Expr::Call(Box::new(expr.unwrap()), args, span));
            
            } else if let Some(token) = self.try_eat(Dot)? {
                let id_tok = self.expect(Ident, "Expected member name after '.'")?;
                let ident = self.ident(id_tok);
                let span = token.span().until(id_tok.span());
                expr = Some(
                    Expr::Access(Loc {
                        kind: LocKind::Field(Box::new(expr.unwrap()), ident),
                        span: span,
                    }, span)
                );
            
            } else if let Some(token) = self.try_eat(BrackOpen)? {
                let index = self.parse_expr(None)?;
                let close = self.expect(BrackClose, "Expected closing ']' in index expression")?;
                let span = token.span().until(close.span());
                expr = Some(
                    Expr::Access(Loc {
                        kind: LocKind::Indexing(Box::new(expr.unwrap()), Box::new(index)),
                        span: span,
                    }, span)
                );
            
            } else {
                break;
            }
        }
        
        Ok(expr.unwrap())
    }
    
    /// Parses a single non-binary expression.
    fn parse_single(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let mut ops = Vec::new();
        loop {
            let token = self.peek()?;
            match token.kind {
                Not => {
                    let _ = self.advance();
                    ops.push((UnOp::Not, token.span()));
                }
                Minus => {
                    let _ = self.advance();
                    ops.push((UnOp::Neg, token.span()));
                }
                _ => break,
            }
        }
        let expr = Some(self.parse_atom()?);
        
        let mut expr = Some(self.parse_tail(expr.unwrap())?);
        
        // Wrap the expression in its unary prefixes
        while let Some((op, start_span)) = ops.pop() {
            let inner = expr.take().unwrap();
            let span = start_span.until(inner.span());
            let new = Expr::Unary(op, Box::new(inner), span);
            expr = Some(new);
        }
        
        Ok(expr.unwrap())
    }
    
    #[inline]
    fn ident(&self, token: Token) -> Ident {
        let text = token.span.slice(&self.text).to_string();
        let span = token.span;
        Ident {
            text: text,
            span: span,
        }
    }
    
    /// Parses an atomic expression.
    fn parse_atom(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let token = self.advance()?;
        match token.kind {
            Integer => {
                let slice = token.slice(self.text);
                let value = slice.parse::<i64>().unwrap();
                Ok(Expr::Integer(value, token.span))
            }
            ParOpen => {
                let expr = self.parse_expr(None)?;
                self.expect(ParClose, "Expected closing ')'")?;
                Ok(expr)
            }
            Ident => {
                let var = self.ident(token);
                let loc = Loc {
                    kind: LocKind::Var(var),
                    span: token.span,
                };
                Ok(Expr::Access(loc, token.span))  
            }
            Panic => {
                self.expect(ParOpen, "Expected '(' after panic")?;
                let msg_token = self.expect(Ident, "Expected var name as panic message")?;
                self.expect(ParClose, "Expected closing ')'")?;
                let msg = msg_token.span.slice(&self.text).to_string();
                let span = token.span().until(msg_token.span());
                Ok(Expr::Panic(msg, span))
            }
            True => {
                Ok(Expr::Bool(true, token.span))
            }
            False => {
                Ok(Expr::Bool(false, token.span))
            }
            If => {
                let start = token.span();
                let cond = self.parse_expr(None)?;
                while let Some(_) = self.try_eat(Newline)? {
                    // pass
                }
                self.expect(Then, "Expected 'then' after 'if'-condition")?;
                //let _ = self.expect(Newline, "Expected newline after condition")?;
                let (ifbody, final_newline) = self.parse_exprs(false)?;
                if ifbody.len() > 1 && !final_newline {
                    self.expect(Newline, "Expected newline after non-compact 'if'-body")?;
                }
                /*if ifbody.is_empty() {
                    return self.error(nl.span.end, "Expected statements in body of if-statement");
                }*/
                let (elsebody, final_newline) = if let Some(_) = self.try_eat(Else)? {
                    //self.expect(Newline, "Expected newline after 'else'")?;
                    self.parse_exprs(false)?
                } else {
                    (Vec::new(), false)
                };
                if elsebody.len() > 1 && !final_newline {
                    self.expect(Newline, "Expected newline after non-compact 'else' body")?;
                }
                let end = self.expect(End, "Expected 'end' to end 'if'-expression")?;
                let span = start.until(end.span());
                Ok(Expr::If(Box::new(cond), ifbody, elsebody, span))
            }
            Do => {
                self.expect(Newline, "Expected newline after 'do'")?;
                let (body, _) = self.parse_exprs(true)?;
                self.expect(End, "Expected 'end' to end 'do' statement")?;
                let span = Span::new(token.span.start, self.peek()?.span.start);
                Ok(Expr::Do(body, span))
            }
            New => {
                let start = token.span();
                let typ = self.parse_type()?;
                if let Some(end) = self.try_eat(End)? {
                    let span = start.until(end.span());
                    return Ok(Expr::Constructor(typ, Vec::new(), span));
                }
                self.expect(Where, "Expected 'where' or 'end' after type name")?;
                self.expect(Newline, "Expected newline after 'where'")?;
                let mut assignments = Vec::new();
                let span;
                loop {
                    if let Some(end) = self.try_eat(End)? {
                        span = start.until(end.span());
                        break;
                    }
                    if let Some(_) = self.try_eat(Newline)? {
                        continue;
                    }
                    let id_tok = self.expect(Ident, "Expected field name")?;
                    let field = self.ident(id_tok);
                    self.expect(Eq, "Expected '=' after field name")?;
                    let value = self.parse_expr(None)?;
                    self.expect(Newline, "Expected newline after field assignment")?;
                    assignments.push((field, value));
                }
                Ok(Expr::Constructor(typ, assignments, span))
            }
            Nil => {
                Ok(Expr::Nil(token.span))
            }
            _ => {
                println!("Could not parse atom :c");
                return self.error(token.span.start, "Expected expression");
            }
        }
    }
    
    fn parse_type(&mut self) -> ParseResult<Type> {
        use crate::lex::TokenKind::*;
        
        let ident_tok = self.expect(Ident, "Expected type name")?;
        let mut span = ident_tok.span();
        let mut text = ident_tok.span().slice(&self.text).to_string();
        
        while let Some(_) = self.try_eat(Dot)? {
            let next_id = self.expect(Ident, "Expected identifier after '.'")?;
            span = span.until(next_id.span());
            text.push('.');
            text.push_str(next_id.span().slice(&self.text));
        }
        let ident = crate::ast::Ident {
            text: text,
            span: span,
        };
        Ok(match ident.text.as_str() {
            "int" => crate::ast::Type::Int,
            "bool" => crate::ast::Type::Bool,
            _ => {
                let mut args = Vec::new();
                let end;
                if let Some(_) = self.try_eat(BrackOpen)? {
                    loop {
                        let typ = self.parse_type()?;
                        args.push(typ);
                        if let Some(token) = self.try_eat(BrackClose)? {
                            end = token.span();
                            break;
                        }
                        self.expect(TokenKind::Comma, "Comma between type arguments")?;
                        if let Some(token) = self.try_eat(BrackClose)? {
                            end = token.span();
                            break;
                        }
                    }
                    let span = ident.span.until(end);
                    crate::ast::Type::Named {
                        name: ident, 
                        generics: args, 
                        span
                    }
                } else {
                    let span = ident.span;
                    crate::ast::Type::Named {
                        name: ident, 
                        generics: args, 
                        span
                    }
                }
            }
        })
    }
    
    fn parse_args(&mut self) -> ParseResult<(Vec<Expr>, Span)> {
        use crate::lex::TokenKind::*;
        //println!("Parsing args...");
        
        let mut args = Vec::new();
        let end;
        loop {
            while let Newline = self.peek()?.kind {
                let _ = self.advance();
            }
            if let Some(token) = self.try_eat(ParClose)? {
                end = token.span;
                break;
            }
            //println!("  Next item: {:?}", self.peek()?.kind);
            let arg = self.parse_expr(None)?;
            args.push(arg);
            while let Newline = self.peek()?.kind {
                let _ = self.advance();
            }
            if let Some(token) = self.try_eat(ParClose)? {
                end = token.span;
                break;
            }
            self.expect(Comma, "Expected ',' between arguments")?;
        }
        //println!("Args parsed!");
        Ok((args, end))
    }
    
    fn parse_ast(&mut self) -> ParseResult<Ast> {
        let (ast, _) = self.parse_exprs(true)?;
        if self.is_at_end()? {
            Ok(ast)
        } else {
            let pos = self.peek()?.span.start;
            self.error(pos, "Expected statement or newline")
        }
    }
    
    fn parse_type_dec(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let start = self.advance()?;
        let name_tok = self.expect(Ident, "Expected type name after 'type'")?;
        let name = self.ident(name_tok);
        if let Some(end) = self.try_eat(End)? {
            let span = start.span().until(end.span());
            return Ok(Expr::TypeDec {
                name: name, 
                fields: Vec::new(), 
                variants: Vec::new(),
                span: span,
            });
            
        } 
        self.expect(Of, "Expected 'of' or 'end' after type name")?;
        self.expect(Newline, "Expected newline after 'of'")?;
        let mut fields = Vec::new();
        let mut variants = Vec::new();
        loop {
            while let Some(_) = self.try_eat(Newline)? {}
            if let Some(end) = self.try_eat(End)? {
                let span = start.span().until(end.span());
                return Ok(Expr::TypeDec {
                    name: name,
                    fields: fields,
                    variants: variants,
                    span: span,
                });
            } else if let Some(field_tok) = self.try_eat(Ident)? {
                let field_id = self.ident(field_tok);
                self.expect(Colon, "Expected ':' after field name")?;
                let typ = self.parse_type()?;
                self.expect(Newline, "Expected newline after field type")?;
                fields.push((field_id, typ));
            
            } else if let Some(start) = self.try_eat(Case)? {
                let var_tok = self.expect(Ident, "Expected variant name after 'case'")?;
                let var_id = self.ident(var_tok);
                if let Some(end) = self.try_eat(End)? {
                    let span = start.span().until(end.span());
                    let variant = (var_id, Vec::new(), span);
                    variants.push(variant);
                    continue;
                }
                self.expect(Of, "Expected 'of' or 'end' after variant name")?;
                self.expect(Newline, "Expected newline after 'of'")?;
                let mut var_fields = Vec::new();
                loop {
                    while let Some(_) = self.try_eat(Newline)? {}
                    if let Some(end) = self.try_eat(End)? {
                        let span = start.span().until(end.span());
                        let variant = (var_id, var_fields, span);
                        variants.push(variant);
                        break;

                    } else if let Some(field_tok) = self.try_eat(Ident)? {
                        let field_id = self.ident(field_tok);
                        self.expect(Colon, "Expected ':' after field name")?;
                        let typ = self.parse_type()?;
                        self.expect(Newline, "Expected newline after field type")?;
                        var_fields.push((field_id, typ));
            
                    } else {
                        let tok = self.advance()?;
                        return self.error(tok.span.start, "Expected field name or 'end'");
                    }
                }
                
            } else {
                let tok = self.advance()?;
                return self.error(tok.span.start, "Expected field name or 'end'");
            }
        }
    }
    
    fn parse_fn(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let start = self.advance().unwrap();
        let name_tok = self.expect(Ident, "Expected function name after 'fn'")?;
        let ident = self.ident(name_tok);
        
        // Parse args
        self.expect(ParOpen, "Expected '(' after function name")?;
        
        let mut args = Vec::new();
        loop {
            if self.try_eat(ParClose)?.is_some() {
                break;
            }
            let argname_tok = self.expect(Ident, "Expected argument name")?;
            let argname = self.ident(argname_tok);
            self.expect(Colon, "Expected ':' after argument name")?;
            let typ = self.parse_type()?;
            
            args.push((argname, typ));
            
            if self.try_eat(ParClose)?.is_some() {
                break;
            }
            self.expect(Comma, "Expected ',' between arguments")?;
        }
        
        let ret_type = if self.try_eat(Arrow)?.is_some() {
            Some(self.parse_type()?)
        } else {
            None
        };
        
        self.expect(Newline, "Expected newline after args/return type")?;
        let (body, _) = self.parse_exprs(true)?;
        let end = self.expect(End, "Expected 'end' to end function declaration")?;
        let span = start.span().until(end.span());
        Ok(Expr::FunDec {
            name: ident, 
            args: args, 
            ret: ret_type, 
            body: body, 
            span: span,
        })
    }
    
    fn parse_assignment_or_expr(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let expr = self.parse_expr(None)?;
        let loc = if let Expr::Access(loc, _) = expr {
            loc
        } else {
            return Ok(expr);
        };
        let next = self.peek()?;
        let op = match next.kind {
            Eq => AssOp::Normal,
            PlusEq => AssOp::Add,
            MinusEq => AssOp::Sub,
            StarEq => AssOp::Mul,
            SlashEq => AssOp::Div,
            PercentEq => AssOp::Mod,
            _ => {
                let span = loc.span;
                return Ok(Expr::Access(loc, span));
            }
        };
        let _ = self.advance();
        let expr = self.parse_expr(None)?;
        let span = loc.span().until(expr.span());
        Ok(Expr::Assign(loc, op, Box::new(expr), span))
    }
    
    fn parse_let(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let start = self.advance()?;
        let mut is_mut = false;
        if let Some(_) = self.try_eat(Mut)? {
            is_mut = true;
        }
        let msg = if is_mut { 
            "Expected variable name after 'mut'" 
        } else {
            "Expected variable name or 'mut' after 'let'"
        };
        let ident = self.expect(Ident, msg)?;
        let var = self.ident(ident);
        self.expect(Eq, "Expected '=' after variable name")?;
        let expr = self.parse_expr(None)?;
        let span = start.span().until(expr.span());
        Ok(Expr::Let { is_mut: is_mut, var: var, value: Box::new(expr), span: span })
    }
    
    fn parse_while(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let start = self.advance()?;
        let cond = self.parse_expr(None)?;
        self.expect(Do, "Expected 'do' after 'while'-condition")?;
        self.expect(Newline, "Expected newline after 'do'")?;
        let (body, _) = self.parse_exprs(true)?;
        let end = self.expect(End, "Expected 'end' to end 'while' statement")?;
        let span = start.span().until(end.span());
        Ok(Expr::While(Box::new(cond), body, span))
    }
    
    fn parse_for(&mut self) -> ParseResult<Expr> {
        use crate::lex::TokenKind::*;
        
        let start = self.advance()?;
        let var = self.expect(Ident, "Expected identifier after 'for'")?;
        let ident = self.ident(var);
        self.expect(In, "Expected 'in' after identifier")?;
        let expr = self.parse_expr(None)?;
        let iterable = if let Some(_) = self.try_eat(DotDot)? {
            let end = self.parse_expr(None)?;
            let span = expr.span().until(end.span());
            Iterable {
                kind: IterableKind::Range(Box::new(expr), Box::new(end)),
                span: span,
            }
        } else {
            let span = expr.span();
            Iterable {
                kind: IterableKind::Expr(Box::new(expr)),
                span: span,
            }
        };
        self.expect(Do, "Expected 'do' after iterable")?;
        self.expect(Newline, "Expected newline after 'do'")?;
        let (body, _) = self.parse_exprs(true)?;
        let end = self.expect(End, "Expected 'end' to end 'for' statement")?;
        let span = start.span().until(end.span());
        Ok(Expr::For(ident, iterable, body, span))
    }
    
    fn parse_exprs(&mut self, expect_final_newline: bool) -> ParseResult<(Vec<Expr>, bool)> {
        use crate::lex::TokenKind::*;
        
        let mut exprs = Vec::new();
        let mut final_newline = false;
        
        loop {
            let token = self.peek()?;
            match token.kind {
                Newline => {
                    let _ = self.advance();
                    final_newline = true;
                    continue;
                }
                Eof => {
                    break;
                }
                End | Else => {
                    break;
                }
                Let => {
                    exprs.push(self.parse_let()?);
                }
                Ident => {
                    exprs.push(self.parse_assignment_or_expr()?);
                }
                While => {
                    exprs.push(self.parse_while()?);
                }
                For => {
                    exprs.push(self.parse_for()?);
                }
                Fn => {
                    exprs.push(self.parse_fn()?);
                }
                Type => {
                    exprs.push(self.parse_type_dec()?);
                }
                _ => {
                    exprs.push(self.parse_expr(None)?);
                }
            }
            final_newline = false;
            if let Some(_) = self.try_eat(Eof)? {
                break;
            }
            match self.peek()?.kind {
                Newline => {
                    final_newline = true;
                }
                _ => {
                    if expect_final_newline {
                        self.expect(Newline, "Expected newline after statement")?;
                    } else {
                        break;
                    }
                }
            }
        }
        
        Ok((exprs, final_newline))
    }
}

pub fn parse(text: &str) -> ParseResult<Ast> {
    let mut parser = Parser::new(text);
    parser.parse_ast()
}


