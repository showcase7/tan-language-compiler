
use std::iter;
use crate::instr::SymInstr;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Span {
    pub start: usize,
    pub end: usize,
}

impl Span {
    pub fn new(start: usize, end: usize) -> Span {
        Span { start, end }
    }
    
    pub fn slice<'a>(&self, text: &'a str) -> &'a str {
        &text[self.start..self.end]
    }
    
    pub fn until(&self, other: Span) -> Span {
        let start = if self.start < other.start {
            self.start
        } else {
            other.start
        };
        let end = if self.end > other.end {
            self.end
        } else {
            other.end
        };
        Span::new(start, end)
    }
}

pub fn line_info(pos: usize, text: &str) -> (usize, usize, String) {
    let mut lineno = 1;
    let mut line_start = 0;
    for (i, ch) in text.char_indices() {
        if i == pos {
            break;
        }
        if ch == '\n' {
            lineno += 1;
            line_start = i+1;
        }
    }
    let col = (&text[line_start..pos]).chars().count() + 1;
    let line = (&text[line_start..]).lines().next().unwrap_or("").to_string();
    (lineno, col, line)
}

pub fn show_source_span(source: &str, span: Span) {
    let (lineno, col, line) = line_info(span.start, &source);
    
    if lineno > 1 {
        let prev = source.lines().skip(lineno - 2).next().unwrap();
        println!("{:03}: {}", lineno - 1, prev);
    }
    
    println!("{:03}: {}", lineno, line);
    let len = span.end - span.start;
    let pad = iter::repeat(' ').take(col-1).collect::<String>();
    let mark = if len > 1 {
        iter::repeat('~').take(len - 1).collect::<String>()
    } else {
        String::new()
    };
    println!("     {}^{}", pad, mark);
    
    if let Some(line) = source.lines().skip(lineno).next() {
        println!("{:03}: {}", lineno+1, line);
    }
}

fn print_zero_padded(num: usize, digits: usize) {
    let n = num.to_string();
    let mut len = n.len();
    while len < digits {
        print!("0");
        len += 1;
    }
    print!("{}", n);
}

fn print_indent(n: usize) {
    for _ in 0..n {
        print!(" ");
    }
}

#[allow(unused)]
fn print_line_part(span: Span, source: &str) {
    let (lineno, col, line) = line_info(span.start, source);
    let part = span.slice(source);
    print!(" | {}: ", lineno);
    for (i, _) in line.char_indices() {
        if i < (col-1) {
            print!(" ");
        } else {
            break;
        }
    }
    print!("{:?}", part);
}

pub fn print_instructions(instrs: &[SymInstr], _source: &str) {
    use crate::instr::SymInstrKind::*;
    let digits = instrs.len().to_string().len();
    let mut index = 0;
    let mut indent = 0;
    let mut was_label = true;
    let mut last_span = Span::new(0, 0);
    for instr in instrs {
        match instr.kind {
            NonSym(ref subinstr) => {
                print_indent(indent);
                print_zero_padded(index, digits);
                
                print!(" {}", subinstr.fmt());
                if instr.span != last_span {
                    //print_line_part(instr.span, source);
                }
                println!("");
                index += 1;
                was_label = false;
            },
            Label(ref label) => {
                if *label.context_name != "" {
                    if ! was_label {
                        println!("");
                    }
                    print_indent(label.context_depth * 2);
                    println!("{}:", label.fmt());
                } else {
                    println!("");
                    //print_indent((label.context_depth + 1) * 2);
                    //println!("...");
                }
                indent = (label.context_depth + 1) * 2;
                was_label = true;
            },
            Bnz(ref label) => {
                print_indent(indent);
                print_zero_padded(index, digits);
                print!(" bnz {}", label.fmt());
                if instr.span != last_span {
                    //print_line_part(instr.span, source);
                }
                println!("");
                index += 1;
                was_label = false;
            },
            Bez(ref label) => {
                print_indent(indent);
                print_zero_padded(index, digits);
                print!(" bez {}", label.fmt());
                if instr.span != last_span {
                    //print_line_part(instr.span, source);
                }
                println!("");
                index += 1;
                was_label = false;
            },
            Jmp(ref label) => {
                print_indent(indent);
                print_zero_padded(index, digits);
                print!(" jmp {}", label.fmt());
                if instr.span != last_span {
                    //print_line_part(instr.span, source);
                }
                println!("");
                index += 1;
                was_label = false;
            },
            Call(ref label, nargs) => {
                print_indent(indent);
                print_zero_padded(index, digits);
                print!(" call {} ({} args)", label.fmt(), nargs);
                if instr.span != last_span {
                    //print_line_part(instr.span, source);
                }
                println!("");
                index += 1;
                was_label = false;
            },
        }
        last_span = instr.span;
    }
    println!("  <END>");
}
