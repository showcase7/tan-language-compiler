
use crate::ast::*;
use crate::util::Span;

use std::collections::{HashMap, HashSet};
use std::ops::Deref;
use std::mem;

#[derive(Debug, Clone, PartialEq)]
pub enum SemError {
    TypeError {
        expected: String,
        actual: String,
        span: Span,
    },
    UnboundVariable {
        name: String,
        span: Span,
    },
    AssignmentToImmutableVar {
        name: String,
        span: Span,
        // dec: Span,
    },
    InvalidAssignmentType {
        loc_typ: String,
        loc_span: Span,
        expr_typ: String,
        expr_span: Span,
        span: Span,
    },
    EmptyWhileLoop {
        span: Span,
    },
    MissingReturnValue {
        fun: Ident,
        span: Span,
        expected: String,
    },
    Message {
        message: String,
        span: Span,
    }
}

impl SemError {
    pub fn description(&self) -> String {
        use self::SemError::*;
        match *self {
            TypeError { ref expected, ref actual, .. } => {
                format!("Expected type {}, got {}", expected, actual)
            }
            UnboundVariable { ref name, .. } => {
                format!("Reference to unbound variable '{}'", name)
            },
            AssignmentToImmutableVar { ref name, ..} => {
                format!("Assignment to immutable variable '{}'", name)
            },
            InvalidAssignmentType { ref loc_typ, ref expr_typ, .. } => {
                format!("Assignment of value of type {} to location of type {}", expr_typ, loc_typ)
            },
            EmptyWhileLoop { .. } => {
                format!("Empty while loop")
            },
            MissingReturnValue { ref fun, ref expected, ..} => {
                format!("Function '{}' expected a return expression of type '{}'", fun.text, expected)
            },
            Message { ref message, .. } => {
                message.clone()
            }
        }
    }
    
    pub fn span(&self) -> Span {
        use self::SemError::*;
        match *self {
            TypeError { span, .. } => span,
            UnboundVariable { span, .. } => span,
            AssignmentToImmutableVar { span, .. } => span,
            InvalidAssignmentType { span, .. } => span,
            EmptyWhileLoop { span } => span,
            MissingReturnValue { span, .. } => span,
            Message { span, .. } => span,
        }
    }
}

enum ScopeChange {
    /// This var should be removed afterwards
    VarAdded(String),
    
    /// This previous var setting should be restored
    VarReplaced(String, VarInfo),
    
    /// This previous return type should be restored
    RetTypeUpdated(Option<TypeId>),
    
    FunAdded(String),
    
    FunReplaced(String, FunSig),
}

#[derive(Debug, Clone)]
struct VarInfo {
    name: String,
    id: IdentId,
    is_mut: bool,
    typ: TypeId,
    store_typ: TypeId,
}

struct Scope {
    changes: Vec<ScopeChange>,
}

impl Scope {
    fn new() -> Scope {
        Scope {
            changes: Vec::new(),
        }
    }
}

enum ScopeKind {
    Function(Option<TypeId>),
    Block,
}

#[derive(Debug, Clone)]
pub struct TypeInfo {
    pub name: Ident,
    pub id: IdentId,
    pub typ: TypeId,
    pub fields: Vec<(Ident, TypeId)>,
    pub variants: Vec<IdentId>,
    pub parent: Option<IdentId>,
}

impl TypeInfo {
    pub fn field(&self, name: &str) -> Option<(usize, TypeId)> {
        for (i, &(ref ident, typ)) in self.fields.iter().enumerate() {
            if ident.text == name {
                return Some((i, typ));
            }
        }
        None
    }

    pub fn has_field(&self, name: &str) -> bool {
        self.fields.iter().any(|&(ref ident, _)| ident.text == name)
    }
}

#[derive(Debug, Clone)]
struct FunSig {
    name: String,
    ident: IdentId,
    params: Vec<TypeId>, 
    ret_type: Option<TypeId>,
}

struct Checker {
    errors: Vec<SemError>,
    scopes: Vec<Scope>,
    vars: HashMap<String, VarInfo>,
    funs: HashMap<String, FunSig>,
    types: HashMap<String, TypeInfo>,
    ret_type: Option<TypeId>,
    next_ident_id: usize,
    type_ids: Vec<Type>,
}


impl Checker {
    fn new() -> Checker {
        Checker {
            errors: Vec::new(),
            scopes: vec![Scope::new()],
            vars: HashMap::new(),
            funs: HashMap::new(),
            types: HashMap::new(),
            ret_type: None,
            next_ident_id: 1,
            type_ids: const_type_order(),
        }
    }
    
    #[inline]
    fn invalid_ident_id(&self) -> IdentId {
        0
    }
    
    #[inline]
    fn add_error(&mut self, error: SemError) {
        self.errors.push(error);
    }
    
    fn add_error_message<S: Into<String>>(&mut self, message: S, span: Span) {
        self.errors.push(SemError::Message {
            message: message.into(),
            span: span,
        });
    }
    
    fn typ(&self, id: TypeId) -> Type {
        let index = (if id < 0 { -id } else { id }) as usize;
        assert!(index < self.type_ids.len());
        self.type_ids[index].clone()
    }
    
    fn get_type_id(&mut self, typ: Type) -> TypeId {
        match typ {
            Type::Nil => return TYPE_NIL,
            Type::Panic => return TYPE_PANIC,
            Type::Int => return TYPE_INT,
            Type::Bool => return TYPE_BOOL,
            _ => {}
        }
        for (i, t) in self.type_ids.iter().enumerate() {
            if &typ == t {
                let mut id = i as TypeId;
                if typ.is_obj() {
                    id = -id;
                }
                return id;
            }
        }
        let mut id = self.type_ids.len() as TypeId;
        if typ.is_obj() {
            id = -id;
        }
        self.type_ids.push(typ);
        id
    }
    
    fn get_var_info(&mut self, loc: &Loc) -> Option<&mut VarInfo> {
        if let LocKind::Var(ref var) = loc.kind {
            self.vars.get_mut(&var.text)
        } else {
            None
        }
    }
    
    fn push_scope(&mut self, kind: ScopeKind) {
        match kind {
            ScopeKind::Function(ret_type) => {
                let mut scope = Scope::new();
                
                // Update the return type
                let ret_change = ScopeChange::RetTypeUpdated(self.ret_type);
                scope.changes.push(ret_change);
                self.ret_type = ret_type;
                
                // Isolate functions from outer vars
                for (var_name, var_info) in self.vars.drain() {
                    let change = ScopeChange::VarReplaced(var_name, var_info);
                    scope.changes.push(change);
                }
                self.scopes.push(scope);
                
            },
            ScopeKind::Block => {
                self.scopes.push(Scope::new());
            }
        }
    }
    
    fn pop_scope(&mut self) {
        use self::ScopeChange::*;
        
        let mut changes = self.scopes.pop().expect("No scope to pop").changes;
        while let Some(change) = changes.pop() {
            match change {
                VarAdded(var_name) => {
                    self.vars.remove(&var_name);
                }
                VarReplaced(var_name, var_info) => {
                    self.vars.insert(var_name, var_info);
                }
                RetTypeUpdated(ret_type) => {
                    self.ret_type = ret_type;
                }
                FunAdded(fun_name) => {
                    self.funs.remove(&fun_name);
                }
                FunReplaced(fun_name, fun_sig) => {
                    self.funs.insert(fun_name, fun_sig);
                }
            }
        }
    }
    
    fn gen_ident_id(&mut self) -> IdentId {
        let id = self.next_ident_id;
        self.next_ident_id += 1;
        id
    }
    
    fn bind_var(&mut self, var_name: &String, is_mut: bool, typ: TypeId) -> IdentId {
        let change = match self.vars.remove(var_name) {
            Some(var_info) => {
                ScopeChange::VarReplaced(var_name.clone(), var_info)
            }
            None => {
                ScopeChange::VarAdded(var_name.clone())
            }
        };
        self.scopes.last_mut().expect("No scope").changes.push(change);
        let ident_id = self.gen_ident_id();
        let info = VarInfo {
            name: var_name.clone(),
            id: ident_id,
            is_mut: is_mut,
            typ: typ,
            store_typ: typ,
        };
        self.vars.insert(var_name.clone(), info);
        ident_id
    }
    
    fn bind_type(&mut self, name: String, info: TypeInfo) {
        if self.types.contains_key(&name) {
            self.add_error_message(format!(
                "Type '{}' was already defined", 
                name
            ), info.name.span);
            return;
        }
        self.types.insert(name, info);
    }
    
    fn bind_fun(&mut self, fun_name: &String, params: Vec<TypeId>, ret_type: Option<TypeId>) -> IdentId {
        let change = match self.funs.remove(fun_name) {
            Some(fun_sig) => {
                ScopeChange::FunReplaced(fun_name.clone(), fun_sig)
            }
            None => {
                ScopeChange::FunAdded(fun_name.clone())
            }
        };
        self.scopes.last_mut().expect("No scope").changes.push(change);
        let fun_id = self.gen_ident_id();

        let sig = FunSig {
            name: fun_name.clone(),
            ident: fun_id,
            params: params, 
            ret_type: ret_type,
        };
        self.funs.insert(fun_name.clone(), sig);
        fun_id
    }
    
    /// Formats a type prettily.
    fn fmt_typ(&self, typ: TypeId) -> String {
        let index = (if typ < 0 { -typ } else { typ }) as usize;
        let t = &self.type_ids[index];
        t.fmt(&mut |t| self.fmt_typ(t))
    }
    
    fn create_unwrap(&mut self, inner: TExpr, expected: TypeId, options: &Union, span: Span) -> TExpr {
        let mut other_cases = Vec::new();
        for &typ in &options.types {
            if typ == expected {
                continue;
            }
            
            // Create this error just to get the standard format.
            let err = SemError::TypeError { 
                expected: self.fmt_typ(expected), 
                actual: self.fmt_typ(typ), 
                span: span,
            };
            let msg = err.description();
            
            other_cases.push((typ, self.fmt_typ(typ), msg));
        }
        
        TExpr::UnwrapUnion {
            value: Box::new(inner), 
            expected: (expected, self.fmt_typ(expected)), 
            other_cases: other_cases,
            typ: expected.clone(),
            span: span,
        }
    }
    
    fn type_is_valid(&self, expected: TypeId, actual: TypeId) -> bool {
        if actual == expected {
            return true;
        }
        match (self.typ(actual), self.typ(expected)) {
            (Type::Panic, _) => true,
            (_, Type::Panic) => true,
            (Type::Bound { id: act_id, parent, .. }, Type::Bound { id: exp_id, .. }) => {
                if act_id == exp_id {
                    true
                } else if let Some(par_id) = parent {
                    exp_id == par_id
                } else {
                    false
                }
            }
            (Type::Union(ref union), _) => {
                // You can use a union as any of the types it contains.
                union.contains(expected)
            }
            /*(Named { .. }, _) => panic!("typecheck got 'Named' (unbound) type"),
            (_, Named { .. }) => panic!("Typecheck got 'Named' (unbound) type"),*/
            /*(Struct(ref act), Struct(ref exp)) => {
                if act == exp {
                    return true;
                }
                if let Some(end) = act.find(".") {
                    let sup = &act[..end];
                    if sup == exp {
                        true
                    } else {
                        false
                    }
                } else if let Some(end) = exp.find(".") {
                    let sup = &exp[..end];
                    if act == sup {
                        true
                    } else {
                        false
                    }
                } else {
                    false
                }
            }
            (_, _) => actual == expected,*/
            _ => false,
        }
    }
    
    /// Validates that the given expression has the expected type, and wraps
    /// it in a type-error panic if it does not.
    fn type_check_expr(&mut self, expected: TypeId, expr: &mut TExpr) {
        // Allow panic types to be anything.
        if expr.typ() == TYPE_PANIC {
            return;
        }
        
        println!("typeck: Checking whether {} can be used as {}...", self.fmt_typ(expr.typ()), self.fmt_typ(expected));
        
        if self.type_is_valid(expected, expr.typ()) {
            if let Type::Union(union) = self.typ(expr.typ()) {
                // Warn about the specialization
                let msg = format!("The value might not be of the expected type, {}", self.fmt_typ(expected));
                self.add_error_message(msg, expr.span());
                
                // Make a copy of the location for reassign purposes
                let loc = if let TExpr::Access { ref loc, .. } = *expr {
                    // And update the type of this variable.
                    if let TLocKind::Var(ref ident) = loc.kind {
                        let info = self.vars.get_mut(&ident.text).unwrap();
                        info.typ = expected;
                    } else {
                        unreachable!();
                    }
                    Some(loc.clone())
                } else {
                    None
                };
                
                // Create an unwrap expression
                let span = expr.span();
                let mut inner = mem::replace(expr, TExpr::empty());
                *inner.typ_mut() = self.get_type_id(Type::Union(union.clone()));
                
                // Prepare error messages for the tag match in the unwrap.
                let unwrap = self.create_unwrap(inner, expected, &union, span);
                
                // Potentially wrap the unwrap in a reassign
                if let Some(loc) = loc {
                    *expr = TExpr::Reassign {
                        loc: loc, 
                        value: Box::new(unwrap), 
                        typ: expected.clone(), 
                        span
                    };
                } else {
                    *expr = unwrap;
                }
            }
            println!("  Yes!");
            return;
        }
        
        println!("  No!");
        let err = SemError::TypeError { 
            expected: self.fmt_typ(expected), 
            actual: self.fmt_typ(expr.typ()), 
            span: expr.span(),
        };
        let msg = err.description();
        self.add_error(err);
        let span = expr.span();
        let mut inner = mem::replace(expr, TExpr::empty());
        inner.wrap_in_panic(msg, span, PanicLoc::After);
        *expr = inner;
    }
    
    fn check_loc(&mut self, loc: &Loc) -> TLoc {
        match loc.kind {
            LocKind::Var(ref var) => {
                if let Some(info) = self.vars.get(&var.text) {
                    let t_var = TIdent {
                        text: var.text.clone(),
                        id: info.id,
                        span: var.span,
                    };
                    TLoc {
                        kind: TLocKind::Var(t_var),
                        typ: info.typ,
                        span: loc.span,
                    }
                } else {
                    let err = SemError::UnboundVariable {
                        name: var.text.clone(),
                        span: loc.span,
                    };
                    let msg = err.description();
                    self.add_error(err);
                    TLoc::panic(TLoc::empty(), msg, loc.span)
                }
            }
            LocKind::Indexing(ref inner, ref index) => {
                let mut ck_expr = self.check_expr(inner, true, false);
                let typ = match self.typ(ck_expr.typ()) {
                    Type::Vec(ref item_typ) => {
                        self.get_type_id((*item_typ.deref()).clone())
                    }
                    Type::Panic => {
                        TYPE_PANIC
                    }
                    _ => {
                        let msg = format!("Expected type Vec[T], got {}", self.fmt_typ(ck_expr.typ()));
                        self.add_error_message(msg.clone(), ck_expr.span());
                        let span = ck_expr.span();
                        ck_expr.wrap_in_panic(msg, span, PanicLoc::After);
                        TYPE_PANIC
                    }
                };
                let mut ck_index = self.check_expr(index, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_index);
                TLoc {
                    kind: TLocKind::Indexing(Box::new(ck_expr), Box::new(ck_index)),
                    typ: typ,
                    span: loc.span,
                }
            }
            LocKind::Field(ref expr, ref field) => {
                let mut ck_expr = self.check_expr(expr, true, false);
                match self.typ(ck_expr.typ()) {
                    Type::Bound { ref name, .. } => {
                        let info = self.types[name].clone();
                        if let Some((index, typ)) = info.field(&field.text) {
                            TLoc {
                                kind: TLocKind::Field(Box::new(ck_expr), index),
                                typ: typ,
                                span: loc.span,
                            }
                        } else {
                            let msg = format!("There is no field named '{}' on type '{}'", field.text, name);
                            self.add_error_message(msg.clone(), field.span);
                            ck_expr.wrap_in_panic(msg, field.span, PanicLoc::After);
                            TLoc {
                                kind: TLocKind::Field(Box::new(ck_expr), 0),
                                typ: TYPE_PANIC,
                                span: loc.span,
                            }
                        }
                    }
                    Type::Panic => {
                        TLoc {
                            kind: TLocKind::Field(Box::new(ck_expr), 0),
                            typ: TYPE_PANIC,
                            span: loc.span,
                        }
                    }
                    _ => {
                        let msg = format!("Type {} does not have any fields", self.fmt_typ(ck_expr.typ()));
                        self.add_error_message(msg.clone(), expr.span());
                        ck_expr.wrap_in_panic(msg, field.span, PanicLoc::After);
                        TLoc {
                            kind: TLocKind::Field(Box::new(ck_expr), 0),
                            typ: TYPE_PANIC,
                            span: loc.span,
                        }
                    }
                }
            }
        }
    }
    
    fn check_builtin_call(&mut self, builtin: Builtin, args: &[Expr], span: Span) -> TExpr {
        // Check the arguments
        let mut ck_args = Vec::new();
        let mut given_arg_info = Vec::new();
        for arg in args {
            let ck_arg = self.check_expr(arg, true, false);
            given_arg_info.push((ck_arg.typ().clone(), ck_arg.span()));
            ck_args.push(ck_arg);
        }
        
        match builtin {
            Builtin::VecPush => {
                // TODO: check mutability
                let mut panic = None;
                if let Err((msg, span)) = self.check_arg_count(2, args.len(), "vec_push", span) {
                    panic = Some((msg, span));
                }
                match self.typ(ck_args[0].typ()) {
                    Type::Vec(ref item_typ) => {
                        let ck_item_typ = match self.check_type(item_typ) {
                            Ok(typ) => typ,
                            Err((typ, _, _)) => typ,
                        };

                        self.type_check_expr(ck_item_typ, &mut ck_args[1]);
                    }
                    Type::Panic => {}
                    _ => {
                        let msg = format!("Expected Vec[T], got {}", self.fmt_typ(ck_args[0].typ()));
                        let span = ck_args[0].span();
                        self.add_error_message(msg.clone(), span);
                        ck_args[0].wrap_in_panic(msg, span, PanicLoc::After);
                    }
                }
                
                let mut expr = TExpr::CallBuiltin {
                    builtin: builtin, 
                    args: ck_args, 
                    typ: TYPE_NIL, 
                    span,
                };
                if let Some((msg, span)) = panic {
                    expr.wrap_in_panic(msg, span, PanicLoc::Before)
                }
                expr
            }
            Builtin::VecPop => {
                // TODO: check mutability
                let mut panic = None;
                if let Err((msg, span)) = self.check_arg_count(1, args.len(), "vec_pop", span) {
                    panic = Some((msg, span));
                }
                let typ = if let Type::Vec(ref item_typ) = self.typ(ck_args[0].typ()) {
                    let ck_item_typ = match self.check_type(item_typ) {
                        Ok(typ) => typ,
                        Err((typ, _, _)) => typ,
                    };
                    // It might not be valid, but it can at least be named
                    ck_item_typ
                } else {
                    let msg = format!("Expected Vec[T], got {}", self.fmt_typ(ck_args[0].typ()));
                    self.add_error_message(msg.clone(), ck_args[0].span());
                    let mut inner = mem::replace(&mut ck_args[0], TExpr::empty());
                    inner.wrap_in_panic(msg, ck_args[0].span(), PanicLoc::After);
                    ck_args[0] = inner;
                    TYPE_PANIC
                };
                
                let mut expr = TExpr::CallBuiltin {
                    builtin: builtin, 
                    args: ck_args, 
                    typ: typ, 
                    span: span
                };
                if let Some((msg, span)) = panic {
                    expr.wrap_in_panic(msg, span, PanicLoc::Before);
                } 
                expr
            }
            Builtin::VecLen => {
                let mut panic = None;
                if let Err((msg, span)) = self.check_arg_count(1, args.len(), "vec_len", span) {
                    panic = Some((msg, span));
                }
                if let Type::Vec(_) = self.typ(ck_args[0].typ()) {
                } else {
                    let msg = format!("Expected Vec[T], got {}", self.fmt_typ(ck_args[0].typ()));
                    self.add_error_message(msg.clone(), ck_args[0].span());
                    let mut inner = mem::replace(&mut ck_args[0], TExpr::empty());
                    inner.wrap_in_panic(msg, ck_args[0].span(), PanicLoc::After);
                    ck_args[0] = inner;
                }
                
                let mut expr = TExpr::CallBuiltin {
                    builtin: builtin, 
                    args: ck_args, 
                    typ: TYPE_INT, 
                    span
                };
                if let Some((msg, span)) = panic {
                    expr.wrap_in_panic(msg, span, PanicLoc::Before);
                } 
                expr
            }
            _ => unimplemented!(),
        }
    }
    
    fn check_arg_count(&mut self, expected: usize, given: usize, name: &str, span: Span) -> Result<(), (String, Span)> {
        if given != expected {
            let s = if given > 1 { "s" } else { "" };
            let msg = format!("Function '{}' was given {} argument{}, expected {}", name, given, s, expected);
            self.add_error_message(msg.clone(), span);
            Err((msg, span))
        } else {
            Ok(())
        }
    }
    
    fn check_call(&mut self, fun_expr: &Expr, args: &[Expr], span: Span) -> TExpr {
        // The resulting type (or panic) of the call expression
        let mut typ = TYPE_NIL;
        let mut panic = None;
        
        // Find out what it refers to
        let mut fun_sig = None;
        let (name, found) = match fun_expr {
            Expr::Access(ref loc, _) => {
                match loc.kind {
                    LocKind::Var(ref ident) => {
                        let name = ident.text.clone();
                        let found = if let Some(sig) = self.funs.get(&ident.text) {
                            fun_sig = Some(sig.clone());
                            true
                        } else {
                            false
                        };
                        (name, found)
                    }
                    _ => unreachable!(),
                }
            }
            _ => {
                // error
                let msg = format!("Call to non-function type");
                self.add_error_message(msg.clone(), fun_expr.span());
                panic = Some((msg, fun_expr.span()));
                (String::new(), false)
            }
        };
        
        // If not found, see if it matches a builtin.
        if ! found {
            match name.as_str() {
                "vec_push" => {
                    return self.check_builtin_call(Builtin::VecPush, args, span);
                }
                "vec_pop" => {
                    return self.check_builtin_call(Builtin::VecPop, args, span);
                }
                "vec_len" => {
                    return self.check_builtin_call(Builtin::VecLen, args, span);
                }
                "" => {}
                _ => {
                    let msg = format!("Call to unknown function '{}'", name);
                    self.add_error_message(msg.clone(), fun_expr.span());
                    if panic.is_none() {
                        panic = Some((msg, fun_expr.span()));
                    }
                }
            }
        }
        
        // Check that the right number of arguments is given
        if let Some(ref fun_sig) = fun_sig {
            if let Err((msg, span)) = self.check_arg_count(fun_sig.params.len(), args.len(), &fun_sig.name, span) {
                if panic.is_none() {
                    panic = Some((msg, span));
                }
            }
        }
        
        // Check the arguments
        let mut ck_args = Vec::new();
        for arg in args {
            let ck_arg = self.check_expr(arg, true, false);
            ck_args.push(ck_arg);
        }
        
        // Check the types of the arguments
        if let Some(ref fun_sig) = fun_sig {
            for i in 0 .. ck_args.len() {
                self.type_check_expr(fun_sig.params[i], &mut ck_args[i]);
            }
        }
        
        // Set the actual return type if everything went well
        if let Some(ref fun_sig) = fun_sig {
            if let Some(ret_type) = fun_sig.ret_type {
                if typ != TYPE_PANIC {
                    typ = ret_type;
                }
            }
        }
        
        let fun_id = fun_sig.map(|sig| sig.ident).unwrap_or_else(|| self.invalid_ident_id());
        let mut expr = TExpr::Call {
            name: fun_id, 
            args: ck_args,
            typ: typ, 
            span: span,
        };
        if let Some((msg, span)) = panic {
            expr.wrap_in_panic(msg, span, PanicLoc::Before);
        } 
        expr
    }
    
    fn check_binary(&mut self, left: &Expr, op: BinOp, right: &Expr, span: Span) -> TExpr {
        match op {
            BinOp::Add | BinOp::Sub | BinOp::Mul | BinOp::Div | BinOp::Mod => {
                let mut ck_left = self.check_expr(left, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_left);
                let mut ck_right = self.check_expr(right, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_right);
                TExpr::Binary {
                    left: Box::new(ck_left), 
                    op: op, 
                    right: Box::new(ck_right), 
                    typ: TYPE_INT, 
                    span: span,
                }
            }
            BinOp::Lt | BinOp::LtEq | BinOp::Gt | BinOp::GtEq => {
                let mut ck_left = self.check_expr(left, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_left);
                let mut ck_right = self.check_expr(right, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_right);
                TExpr::Binary {
                    left: Box::new(ck_left), 
                    op: op, 
                    right: Box::new(ck_right), 
                    typ: TYPE_BOOL, 
                    span: span,
                }
            }
            BinOp::Eq | BinOp::NotEq => {
                let ck_left = self.check_expr(left, true, false);
                let mut ck_right = self.check_expr(right, true, false);
                self.type_check_expr(ck_left.typ(), &mut ck_right);
                TExpr::Binary {
                    left: Box::new(ck_left), 
                    op: op, 
                    right: Box::new(ck_right), 
                    typ: TYPE_BOOL, 
                    span: span,
                }
            }
            BinOp::And | BinOp::Or => {
                let mut ck_left = self.check_expr(left, true, false);
                self.type_check_expr(TYPE_BOOL, &mut ck_left);
                let mut ck_right = self.check_expr(right, true, false);
                self.type_check_expr(TYPE_BOOL, &mut ck_right);
                TExpr::Binary {
                    left: Box::new(ck_left), 
                    op: op, 
                    right: Box::new(ck_right), 
                    typ: TYPE_BOOL, 
                    span: span,
                }
            }
        }
    }
    
    fn check_unary(&mut self, op: UnOp, inner: &Expr, span: Span) -> TExpr {
        match op {
            UnOp::Neg => {
                let mut ck_expr = self.check_expr(inner, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_expr);
                TExpr::Unary {
                    op: op, 
                    value: Box::new(ck_expr), 
                    typ: TYPE_INT, 
                    span: span,
                }
            }
            UnOp::Not => {
                let mut ck_expr = self.check_expr(inner, true, false);
                self.type_check_expr(TYPE_BOOL, &mut ck_expr);
                TExpr::Unary {
                    op: op, 
                    value: Box::new(ck_expr), 
                    typ: TYPE_BOOL, 
                    span: span,
                }
            }
        }
    }
    
    fn check_if(&mut self, cond: &Expr, ifbody: &[Expr], elsebody: &[Expr], span: Span, expect_value: bool) -> TExpr {
        
        let mut ck_cond = self.check_expr(cond, true, false);
        self.type_check_expr(TYPE_BOOL, &mut ck_cond);
        
        self.push_scope(ScopeKind::Block);
        let ck_ifbody = self.check_exprs(ifbody, expect_value, false);
        self.pop_scope();
        
        self.push_scope(ScopeKind::Block);
        let ck_elsebody = self.check_exprs(elsebody, expect_value, false);
        self.pop_scope();
        
        let else_span = if let Some(expr) = ck_elsebody.last() {
            expr.span()
        } else {
            span
        };
        
        // If the 'if' should generate a value to be used:
        
        // If there is no else-clause but there is a return expression,
        // the else clause should panic (because it generates a nil)
        let (typ, if_tag, else_tag) = if expect_value {
            let if_typ = ck_ifbody.last().map(|e| e.typ()).unwrap_or(TYPE_NIL);
            let else_typ = ck_elsebody.last().map(|e| e.typ()).unwrap_or(TYPE_NIL);       
            match (self.typ(if_typ), self.typ(else_typ)) {
                (Type::Panic, other) | 
                (other, Type::Panic) => {
                    (self.get_type_id(other), None, None)
                }
                (Type::Union(mut union), other) => {
                    let other_id = self.get_type_id(other);
                    if other_id != union.first() {
                        self.add_error_message(format!("Expected type {} in 'else'-branch, got {}", self.fmt_typ(union.first()), self.fmt_typ(other_id)), else_span);
                    }
                
                    union.add(other_id);
                    let typ = self.get_type_id(Type::Union(union));
                    (typ, None, Some(other_id))
                }
                (other, Type::Union(mut union)) => {
                    let other_id = self.get_type_id(other);
                    self.add_error_message(format!(
                        "Expected type {} in 'else'-branch, got {}", 
                        self.fmt_typ(other_id), 
                        union.fmt(&mut |ut| self.fmt_typ(ut))), 
                        else_span
                    );
                
                    union.add(other_id);
                    let typ = self.get_type_id(Type::Union(union));
                    (typ, Some(other_id), None)
                }
                (if_typ, else_typ) => {
                    if else_typ != if_typ {
                        let msg = format!(
                            "Expected type {} in 'else'-branch, got {}", if_typ.fmt(&mut |t| self.fmt_typ(t)), 
                            else_typ.fmt(&mut |t| self.fmt_typ(t))
                        );
                        self.add_error_message(msg, else_span);
                        let if_tag = self.get_type_id(if_typ);
                        let else_tag = self.get_type_id(else_typ);
                        let mut union = Union::new();
                        union.add(if_tag);
                        union.add(else_tag);
                        let typ = self.get_type_id(Type::Union(union));
                        (typ, Some(if_tag), Some(else_tag))
                    } else {
                        (self.get_type_id(if_typ), None, None)
                    }
                }
            }
        } else {
            (TYPE_NIL, None, None)
        };
        
        TExpr::If {
            cond: Box::new(ck_cond), 
            if_body: ck_ifbody, 
            if_tag: if_tag, 
            else_body: ck_elsebody, 
            else_tag: else_tag, 
            typ: typ, 
            span: span,
        }
    }
    
    fn check_constructor(&mut self, cons_typ: &Type, assignments: &[(Ident, Expr)], span: Span) -> TExpr {
        
        let (ck_typid, panic) = match self.check_type(cons_typ) {
            Ok(typ) => (typ, None),
            Err((_, msg, span)) => (TYPE_PANIC, Some((msg, span))),
        };
        
        let ck_typ = self.typ(ck_typid);
        let info = if let Type::Bound { ref name, .. } = ck_typ {
            self.types[name].clone()
        
        } else if let Type::Vec(ref item_typ) = ck_typ {
            let is_obj_vec = item_typ.is_obj();
            let mut expr = TExpr::CallBuiltin {
                builtin: Builtin::NewVec(is_obj_vec), 
                args: Vec::new(), 
                typ: ck_typid, 
                span,
            };
            if let Some((msg, span)) = panic {
                expr.wrap_in_panic(msg, span, PanicLoc::Before);
            } 
            return expr;
            
        } else if let Type::Panic = ck_typ {
            let (msg, span) = panic.unwrap();
            return TExpr::panic(msg, span);
        
        } else {
            let cons_typ_id = self.get_type_id(cons_typ.clone());
            let msg = format!("Type '{}' cannot be constructed in this way", self.fmt_typ(cons_typ_id));
            self.add_error_message(msg.clone(), span);
            return TExpr::panic(msg, span);
        };
        
        let mut ck_assignments = Vec::new();
        let mut assigned = HashSet::new();
        for &(ref field, ref value) in assignments {
            // If the field was assigned twice
            let (index, ck_value) = if assigned.contains(&field.text) {
                let msg = format!("Field '{}' was already assigned", field.text);
                self.add_error_message(msg.clone(), field.span);
                let mut ck_value = self.check_expr(value, true, false);
                let (index, typ) = info.field(&field.text).unwrap();
                self.type_check_expr(typ, &mut ck_value);
                ck_value.wrap_in_panic(msg, field.span, PanicLoc::After);
                (index, ck_value)
            
            // If the field is not a member of the struct
            } else if ! info.has_field(&field.text) {
                let msg = format!("Struct '{}' has no field named '{}'", info.name.text, field.text);
                self.add_error_message(msg.clone(), field.span);
                let mut ck_value = self.check_expr(value, true, false);
                ck_value.wrap_in_panic(msg, field.span, PanicLoc::After);
                const MISSING_INDEX: usize = 9999;
                (MISSING_INDEX, ck_value)
            
            // If the field is fine
            } else {
                assigned.insert(field.text.clone());
                let mut ck_value = self.check_expr(value, true, false);
                let (index, typ) = info.field(&field.text).unwrap();
                self.type_check_expr(typ, &mut ck_value);
                (index, ck_value)
            };
            
            ck_assignments.push((index, ck_value));
        }
        
        let mut panic = panic;
        for &(ref field, _) in &info.fields {
            if ! assigned.contains(&field.text) {
                let msg = format!(
                    "Missing assignment to field '{}' of struct '{}'", 
                    field.text, 
                    info.name.text
                );
                panic = panic.or_else(|| {
                    Some((msg.clone(), span))
                });
                self.add_error_message(msg, span);
            }
        }
        
        let mut expr = TExpr::Constructor {
            typ: ck_typid, 
            assignments: ck_assignments, 
            span: span,
        };
        if let Some((msg, span)) = panic {
            expr.wrap_in_panic(msg, span, PanicLoc::Before);
        }
        expr
    }
    
    fn check_do(&mut self, body: &[Expr], span: Span, expect_value: bool) -> TExpr {
        self.push_scope(ScopeKind::Block);
        let ck_body = self.check_exprs(body, expect_value, false);
        self.pop_scope();
        let typ = ck_body.last().map(|e| e.typ().clone()).unwrap_or(TYPE_NIL);
        TExpr::Do {
            body: ck_body, 
            typ: typ, 
            span: span
        }
    }
    
    fn check_let(&mut self, is_mut: bool, var: &Ident, value: &Expr, span: Span) -> TExpr {
        let ck_expr = self.check_expr(value, true, false);
        let id = self.bind_var(&var.text, is_mut, ck_expr.typ());
        let t_var = TIdent {
            text: var.text.clone(),
            id: id,
            span: var.span,
        };
        TExpr::Let { 
            is_mut: is_mut, 
            var: t_var, 
            store_typ: None, 
            value: Box::new(ck_expr),
            typ: TYPE_NIL,
            span: span,
        }
    }
    
    fn check_while(&mut self, cond: &Expr, body: &[Expr], span: Span) -> TExpr {
        let mut ck_cond = self.check_expr(cond, true, false);
        self.type_check_expr(TYPE_BOOL, &mut ck_cond);
        
        self.push_scope(ScopeKind::Block);
        let ck_body = if body.is_empty() {
            let err = SemError::EmptyWhileLoop {
                span: span,
            };
            let msg = err.description();
            self.add_error(err);
            let panic = TExpr::panic(msg, cond.span());
            vec![panic]
        } else {
            self.check_exprs(body, false, false)
        };
        self.pop_scope();
        
        TExpr::While {
            cond: Box::new(ck_cond), 
            body: ck_body, 
            span: span,
            typ: TYPE_NIL,
        }
    }
    
    fn check_for(&mut self, ident: &Ident, iterable: &Iterable, body: &[Expr], span: Span) -> TExpr {
        self.push_scope(ScopeKind::Block);
        
        let ck_iterable = self.check_iterable(iterable);
        
        // Check the iteration variable
        let ident_id = self.bind_var(&ident.text, false, ck_iterable.item_type);
        let ck_ident = TIdent {
            text: ident.text.clone(),
            id: ident_id,
            span: ident.span,
        };
        
        let ck_body = self.check_exprs(body, false, false);
        
        self.pop_scope();
        
        TExpr::For {
            ident: ck_ident, 
            iterable: ck_iterable, 
            body: ck_body, 
            span: span,
            typ: TYPE_NIL,
        }
    }
    
    fn check_fun_dec(&mut self, ident: &Ident, args: &[(Ident, Type)], ret_type: &Option<Type>, body: &[Expr], span: Span) -> TExpr {
        // Validate the arg types
        let mut ck_arg_types = Vec::new();
        for &(ref _arg_ident, ref arg_type) in args {
            
            // Check the type
            let ck_typ = match self.check_type(arg_type) {
                Ok(typ) => typ,
                Err((typ, _, _)) => typ,
            };
            ck_arg_types.push(ck_typ);
        }
        
        // Validate the return type
        
        let ck_ret_type = ret_type.as_ref().map(|typ| {
            match self.check_type(typ) {
                Ok(typ) => typ,
                Err((typ,_, _)) => typ,
            }
        });
                        
        // Recursive functions, yay.
        let ident_id = self.bind_fun(
            &ident.text, 
            ck_arg_types.clone(), 
            ck_ret_type,
        );
        let ck_ident = TIdent {
            text: ident.text.clone(),
            id: ident_id,
            span: ident.span,
        };
        
        // Push a new scope for the function
        self.push_scope(ScopeKind::Function(ck_ret_type));
        
        let mut ck_args = Vec::new();
        for (i, &(ref arg_ident, ref _arg_type)) in args.iter().enumerate() {
            // Bind the arguments
            let is_mut = false;
            let typ = ck_arg_types[i].clone();
            let arg_ident_id = self.bind_var(&arg_ident.text, is_mut, typ);
            let ck_arg_ident = TIdent {
                text: arg_ident.text.clone(),
                id: arg_ident_id,
                span: arg_ident.span,
            };
            
            ck_args.push((ck_arg_ident, typ));
        }
        
        // Check the body.
        let expect_value = ret_type.is_some();
        let mut ck_body = self.check_exprs(body, expect_value, false);
        
        // No more things to check, clean up the function scope
        self.pop_scope();
        
        // Validate that the return type is valid.
        if let Some(typ) = ck_ret_type {
            if let Some(last) = ck_body.last_mut() {
                self.type_check_expr(typ, last);
            
            // If the body has no return type (no statements)
            } else {
                // Mark the error
                let err = SemError::MissingReturnValue {
                    fun: ident.clone(),
                    span: span,
                    expected: self.fmt_typ(typ),
                };
                let msg = err.description();
                self.add_error(err);
                
                // Add a panic
                let panic = TExpr::panic(msg, span);
                ck_body.push(panic);
            }
        }

        // Generate an identifier for the return slot.
        let ck_ret = ck_ret_type.map(|typ| {
            (typ, self.gen_ident_id())
        });
        
        TExpr::FunDec {
            name: ck_ident, 
            args: ck_args, 
            ret: ck_ret, 
            body: ck_body, 
            span: span,
            typ: TYPE_NIL,
        }
    }
    
    fn check_expr(&mut self, expr: &Expr, expect_value: bool, top_level: bool) -> TExpr {
        use crate::ast::Expr::*;
        #[allow(unreachable_patterns)]
        match *expr {
            Integer(value, span) => {
                TExpr::Integer {
                    value, 
                    span,
                    typ: TYPE_INT,
                }
            }
            Binary(ref left, op, ref right, span) => {
                self.check_binary(left, op, right, span)
            }
            Access(ref loc, span) => {
                let ck_loc = self.check_loc(loc);
                let typ = ck_loc.typ;
                TExpr::Access {
                    loc: ck_loc, 
                    typ: typ, 
                    span: span,
                }
            }
            Panic(ref message, span) => {
                let mut expr = TExpr::empty();
                expr.wrap_in_panic(message.clone(), span, PanicLoc::Before);
                expr
            }
            Bool(value, span) => {
                TExpr::Bool {
                    value, 
                    span,
                    typ: TYPE_BOOL,
                }
            }
            Unary(op, ref inner, span) => {
                self.check_unary(op, inner, span)
            }
            If(ref cond, ref ifbody, ref elsebody, span) => {
                self.check_if(cond, ifbody, elsebody, span, expect_value)
            }
            Call(ref fun_expr, ref args, span) => {
                self.check_call(fun_expr, args, span)
            }
            Constructor(ref cons_typ, ref fields, span) => {
                self.check_constructor(cons_typ, fields, span)
            }
            Do(ref body, span) => {
                self.check_do(body, span, expect_value)
            }
            Nil(span) => {
                TExpr::Nil {
                    typ: TYPE_NIL,
                    span,
                }
            }
            Expr::Let{ is_mut, ref var, ref value, span } => {
                self.check_let(is_mut, var, value, span)
            }
            Expr::Assign(ref loc, op, ref expr, span) => {                
                self.check_assign(loc, op, expr, span)
            }
            Expr::While(ref cond, ref body, span) => {
                self.check_while(cond, body, span)
            }
            Expr::For(ref ident, ref iterable, ref body, span) => {
                self.check_for(ident, iterable, body, span)
            }
            Expr::FunDec { ref name, ref args, ref ret, ref body, span } => {
                self.check_fun_dec(name, args, ret, body, span)
            }
            Expr::TypeDec { span, .. } => {
                if ! top_level {
                    self.add_error_message(format!("Type definitions are only allowed at top-level"), span);
                }
                TExpr::Nil {
                    typ: TYPE_NIL,
                    span,
                }
            }
            _ => {
                unimplemented!();
            }
        }
    }
    
    fn check_iterable(&mut self, iterable: &Iterable) -> TIterable {
        match iterable.kind {
            IterableKind::Range(ref start, ref end) => {
                let mut ck_start = self.check_expr(start, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_start);
                let mut ck_end = self.check_expr(end, true, false);
                self.type_check_expr(TYPE_INT, &mut ck_end);
                TIterable {
                    kind: TIterableKind::Range(Box::new(ck_start), Box::new(ck_end)),
                    span: iterable.span,
                    item_type: TYPE_INT,
                }
            }
            IterableKind::Expr(ref expr) => {
                let mut ck_expr = self.check_expr(expr, true, false);
                let item_type = match self.typ(ck_expr.typ()) {
                    Type::Panic => {
                        TYPE_PANIC
                    }
                    Type::Vec(ref item_typ) => {
                        self.get_type_id(item_typ.deref().clone())
                    }
                    _ => {
                        let msg = format!("Type {} is not a valid iterable", self.fmt_typ(ck_expr.typ()));
                        self.add_error_message(msg.clone(), expr.span());
                        ck_expr.wrap_in_panic(msg, expr.span(), PanicLoc::After);
                        TYPE_PANIC
                    }
                };
                
                TIterable {
                    kind: TIterableKind::Expr(Box::new(ck_expr)),
                    span: iterable.span,
                    item_type: item_type,
                }
            }
        }
    }
    
    fn check_assign(&mut self, loc: &Loc, op: AssOp, value: &Expr, span: Span) -> TExpr {
        let mut ck_loc = self.check_loc(loc);
        let mut ck_value = self.check_expr(value, true, false);
        
        // check that all parts are mutable
        match loc.kind {
            LocKind::Var(ref var) => {
                if let Some(ref info) = self.vars.get(&var.text) {
                    if ! info.is_mut {
                        let err = SemError::AssignmentToImmutableVar {
                            name: var.text.clone(),
                            span: var.span,
                        };
                        let msg = err.description();
                        self.add_error(err);
                        let span = ck_loc.span;
                        ck_loc = TLoc::panic(ck_loc, msg, span);
                        // I should probably not 'poison' the loc already, since
                        // it means that I stop type-checking the location.
                    }
                }
            }
            LocKind::Indexing(_, _) => {}
            LocKind::Field(_, _) => {
                //TODO: handle member mutability
            }
        }
        
        // Handle all op-assign variants:
        // Check that the location and expr can be used with the given operator
        match op {
            AssOp::Normal => {},
            AssOp::Add | AssOp::Sub |AssOp::Mul | AssOp::Div | AssOp::Mod => {
                let mut acc = TExpr::access(ck_loc);
                self.type_check_expr(TYPE_INT, &mut acc);
                self.type_check_expr(TYPE_INT, &mut ck_value);
                return TExpr::Assign { 
                    acc: Box::new(acc), 
                    op: op, 
                    value: Box::new(ck_value), 
                    typ: TYPE_NIL,
                    span
                };
            }
        }
        
        // Handle regular assignment
        
        // If this isn't a variable, just type-check it
        match loc.kind {
            LocKind::Var(_) => {}
            LocKind::Indexing(_, _) | LocKind::Field(_, _) => {
                // Only check the value if the location is computable.
                if ck_loc.typ != TYPE_PANIC {
                    self.type_check_expr(ck_loc.typ, &mut ck_value);
                }
                return TExpr::Assign {
                    acc: Box::new(TExpr::access(ck_loc)), 
                    op: op, 
                    value: Box::new(ck_value),
                    typ: TYPE_NIL,
                    span: span
                };
            }
        }
        
        // Now the location is known to be a variable
        
        // See if the assignment is non-unifying
        match (&self.typ(ck_loc.typ()), &self.typ(ck_value.typ())) {
            (&Type::Union(_), _) | (_, &Type::Union(_)) => {},
            (_, &Type::Panic) | (&Type::Panic, _) => {},
            (_, &Type::Nil) | (&Type::Nil, _) => {},
            (loc_typ, value_typ) => {
                if loc_typ == value_typ {
                    return TExpr::Assign {
                        acc: Box::new(TExpr::access(ck_loc)),
                        op: op, 
                        value: Box::new(ck_value),
                        typ: TYPE_NIL,
                        span: span
                    };
                }
            }
        }
        
        // Make it into a union
        let loc_typ = self.typ(ck_loc.typ());
        let was_union = loc_typ.is_union();
        
        let mut union = if let Type::Union(ref union) = loc_typ {
            union.clone()
        } else {
            let mut union = Union::new();
            union.add(ck_loc.typ.clone());
            union
        };
        
        let loc_typ = self.fmt_typ(ck_loc.typ());
        if ! union.contains(ck_value.typ()) {
            // Warn
            self.add_error_message(format!(
                "Assigning value of type {} to location of type {}",
                self.fmt_typ(ck_value.typ()),
                loc_typ,
            ), span);
            
            // Update the type of the location.
            union.add(ck_value.typ().clone());
        }
        
        // If the value isn't a union yet, tag it
        if ! self.typ(ck_value.typ()).is_union() {
            let inner = mem::replace(&mut ck_value, TExpr::empty());
            let tag = inner.typ();
            let span = inner.span();
            let mut union = Union::new();
            union.add(inner.typ().clone());
            let typ = self.get_type_id(Type::Union(union));
            ck_value = TExpr::Union {
                tag: tag, 
                value: Box::new(inner), 
                typ: typ, 
                span,
            };
        }
        
        // If the variable wasn't a union typ before:
        // allocate a new variable to keep the union in and update where
        // the variable points to
        if ! was_union {
            
            let mut info = self.get_var_info(&loc).unwrap().clone();
            let var = TIdent {
                text: info.name.clone(),
                id: info.id,
                span: loc.span
            };
            
            info.typ = self.get_type_id(Type::Union(union.clone()));
            self.vars.insert(info.name.clone(), info.clone());
            
            let tag_var = TExpr::TagVar {
                ident: var.clone(), 
                typ: ck_loc.typ, 
                span: loc.span,
            };
            
            let new_loc = TLoc {
                kind: TLocKind::Var(var.clone()),
                typ: self.get_type_id(Type::Union(union.clone())),
                span: ck_loc.span,
            };
            
            let assignment = TExpr::Assign {
                acc: Box::new(TExpr::access(new_loc)), 
                op: op, 
                value: Box::new(ck_value),
                span: span,
                typ: TYPE_NIL,
            };
            
            TExpr::new_do(vec![tag_var, assignment]).into()
            
        } else {
            // Update the type of the variable
            let new_typ = self.get_type_id(Type::Union(union.clone()));
            if let Some(ref mut info) = self.get_var_info(&loc) {
                info.typ = new_typ;
            }
            
            TExpr::Assign {
                acc: Box::new(TExpr::access(ck_loc)), 
                op: op, 
                value: Box::new(ck_value),
                typ: TYPE_NIL,
                span: span,
            }
        }
    }
    
    fn check_type(&mut self, typ: &Type) -> Result<TypeId, (TypeId, String, Span)> {
        match *typ {
            Type::Named { ref name, ref generics, span } => {
                match name.text.as_str() {
                    "Vec" => {
                        if generics.len() != 1 {
                            let msg = format!("Type Vec[T] expected 1 type argument, got {}", generics.len());
                            self.add_error_message(msg.clone(), span);
                            let name = typ.fmt(&mut |t| self.fmt_typ(t));
                            let typ = Type::Invalid { name: name };
                            Err((self.get_type_id(typ), msg, span))
                        } else {
                            let ck_typ = self.check_type(&generics[0])?;
                            Ok(self.get_type_id(Type::Vec(Box::new(self.typ(ck_typ)))))
                        }
                    }
                    _ => {
                        match self.types.get(name.text.as_str()) {
                            Some(ref info) => {
                                Ok(info.typ)
                            }
                            None => {
                                let msg = format!("Unknown type '{}'", name.text);
                                self.add_error_message(msg.clone(), span);
                                let name = typ.fmt(&mut |t| self.fmt_typ(t));
                                let typ = Type::Invalid { name: name };
                                Err((self.get_type_id(typ), msg, span))
                            }
                        }
                        
                    }
                }
            }
            _ => Ok(self.get_type_id(typ.clone())),
        }
    }
    
    fn check_exprs(&mut self, exprs: &[Expr], expect_value: bool, top_level: bool) -> Vec<TExpr> {
        let mut ck_exprs = Vec::new();
        let last = if exprs.is_empty() { 0 } else { exprs.len() - 1 };
        for (i, expr) in exprs.iter().enumerate() {
            let exp_val = if i != last {
                false
            } else {
                expect_value
            };
            let ck_expr = self.check_expr(expr, exp_val, top_level);
            ck_exprs.push(ck_expr);
        }
        ck_exprs
    }
    
    /// A big hack to fix let statements whose storage types have later been
    /// promoted to union size.
    fn fix_let_sizes_in_exprs(&mut self, exprs: &mut Vec<TExpr>) {
        for expr in exprs.iter_mut() {
            self.fix_let_sizes_in_expr(expr);
        }
    }
    
    fn fix_let_sizes_in_expr(&mut self, expr: &mut TExpr) {
        use crate::ast::TExpr::*;
        match *expr {
            Integer { .. } => {},
            Bool { .. } => {},
            Binary { ref mut left, ref mut right, .. } => {
                self.fix_let_sizes_in_expr(left);
                self.fix_let_sizes_in_expr(right);
            },
            Access { ref mut loc, .. } => self.fix_let_sizes_in_loc(loc),
            Unary { ref mut value, .. } => self.fix_let_sizes_in_expr(value),
            If { ref mut cond, ref mut if_body, ref mut else_body, .. } => {
                self.fix_let_sizes_in_expr(cond);
                self.fix_let_sizes_in_exprs(if_body);
                self.fix_let_sizes_in_exprs(else_body);
            },
            // fun ident, ret_type, args
            Call { ref mut args, .. } => {
                for arg in args.iter_mut() {
                    self.fix_let_sizes_in_expr(arg);
                }
            },
            CallBuiltin { ref mut args, .. } => {
                for arg in args.iter_mut() {
                    self.fix_let_sizes_in_expr(arg);
                }
            },
            Constructor { ref mut assignments, .. } => {
                for &mut (_, ref mut value) in assignments.iter_mut() {
                    self.fix_let_sizes_in_expr(value);
                }
            },
            Do { ref mut body, .. } => {
                self.fix_let_sizes_in_exprs(body);
            },
            Panic { ref mut inner, .. } => {
                self.fix_let_sizes_in_expr(inner);
            },
            // Update the location with the given value
            Reassign { ref mut loc, ref mut value, .. } => {
                self.fix_let_sizes_in_loc(loc);
                self.fix_let_sizes_in_expr(value);
            },
            // Tag the given expression.
            Union { ref mut value, .. } => {
                self.fix_let_sizes_in_expr(value);
            },
            UnwrapUnion { ref mut value, .. } => {
                self.fix_let_sizes_in_expr(value);
            },
            Nil { .. } => {},
            Let { ref var, ref mut store_typ, ref mut value, .. } => {
                if let Some(info) = self.vars.get(&var.text).clone() {
                    if info.store_typ != value.typ() {
                        // FUCK. Specialization in-between :|
                        // let mut a = true // a: bool
                        // a = 2 // a: <bool or int>
                        // use_int(a) // a: int
                        println!(
                            "Updating 'let' storage type of {} from {} to {}", 
                            info.name, 
                            self.fmt_typ(value.typ()), 
                            self.fmt_typ(info.typ)
                        );
                        *store_typ = Some(info.store_typ);
                    }
                }
                self.fix_let_sizes_in_expr(value);
            },
            Assign { ref mut acc, ref mut value, .. } => {
                self.fix_let_sizes_in_expr(acc);
                self.fix_let_sizes_in_expr(value);
            },
            While { ref mut cond, ref mut body, .. } => {
                self.fix_let_sizes_in_expr(cond);
                self.fix_let_sizes_in_exprs(body);
            },
            FunDec { ref mut body, .. } => {
                self.fix_let_sizes_in_exprs(body);
            },
            For { ref mut iterable, ref mut body, .. } => {
                match iterable.kind {
                    TIterableKind::Expr(ref mut expr) => {
                        self.fix_let_sizes_in_expr(expr);
                    }
                    TIterableKind::Range(ref mut start, ref mut end) => {
                        self.fix_let_sizes_in_expr(start);
                        self.fix_let_sizes_in_expr(end);
                    }
                }
                self.fix_let_sizes_in_exprs(body);
            },
            TagVar { .. } => {},
        }
    }
    
    fn fix_let_sizes_in_loc(&mut self, loc: &mut TLoc) {
        use crate::ast::TLocKind::*;
        match loc.kind {
            Var(_) => {},
            Indexing(ref mut target, ref mut index) => {
                self.fix_let_sizes_in_expr(target);
                self.fix_let_sizes_in_expr(index);
            },
            Field(ref mut target, _) => self.fix_let_sizes_in_expr(target),
            Missing => {},
            Panic(ref mut inner, _, _) => self.fix_let_sizes_in_loc(inner),
        }
    }
    
    fn check_field_decs<F: FnMut(&Type, &mut Self) -> TypeId>(&mut self, fields: &[(Ident, Type)], mut check_type: F) -> Vec<(Ident, TypeId)> {
        let mut ck_fields = Vec::new();
        let mut used_names = HashSet::new();
        for &(ref field_name, ref field_typ) in fields {
            if used_names.contains(&field_name.text) {
                self.add_error_message(format!("Field '{}' was already defined!", field_name.text), field_name.span);
            }
            used_names.insert(field_name.text.clone());
            let typ = check_type(field_typ, self);
            ck_fields.push((field_name.clone(), typ));
        }
        ck_fields
    }
    
    fn check_ast(&mut self, ast: &Ast) -> Result<TAst, (TAst, Vec<SemError>)> {
        // Bind types first

        // Collect the type names and definitions
        let mut types = HashMap::new();
        let mut type_order = Vec::new();
        for expr in ast {
            // TODO: Allow type declarations again
            if let &Expr::TypeDec { ref name, ref fields, ref variants, span, } = expr {
                if types.contains_key(&name.text) {
                    self.add_error_message(format!("Redeclaration of type {}", name.text), expr.span());
                } else {
                    let ident_id = self.gen_ident_id();
                    types.insert(name.text.clone(), (name, ident_id, fields, variants, span));
                    type_order.push(name.text.clone());
                }
            }
        }

        let check_type = |typ: &Type, checker: &mut Checker| {
            match *typ {
                Type::Named { ref name, span, .. } => {
                    if let Some(&(_, ident_id, _, _, _)) = types.get(&name.text) {
                        // TODO: Check generics
                        let typ = Type::Bound {
                            id: ident_id,
                            name: name.text.clone(),
                            parent: None,
                        };
                        checker.get_type_id(typ)
                    } else {
                        let name = typ.fmt(&mut |t| checker.fmt_typ(t));
                        checker.add_error_message(format!(
                            "Unknown type '{}'", 
                            name
                        ), span);
                        checker.get_type_id(Type::Invalid { 
                            name: name,
                        })
                    }
                }
                _ => {
                    match checker.check_type(typ) {
                        Ok(typ) => typ,
                        Err((typ, _, _)) => typ,
                    }
                }
            }
        };

        // Type-check the types
        for type_name in type_order {
            let (name, ident_id, fields, variants, _span) = types[&type_name];

            // Check that fields have valid names and types.
            let ck_fields = self.check_field_decs(fields, check_type);
            let field_names = fields.iter().map(|&(ref name, _)| name.text.clone()).collect::<HashSet<_>>();

            // Do the same for the members of the variants.
            let mut variant_names = HashSet::new();
            let mut variant_ids = Vec::new();
            for &(ref var_name, ref var_fields, _span) in variants {
                if variant_names.contains(&var_name.text) {
                    self.add_error_message(format!(
                        "Variant '{}' was already declared", 
                        var_name.text
                    ), var_name.span);
                    continue;
                }
                for &(ref name, _) in var_fields {
                    if field_names.contains(&name.text) {
                        self.add_error_message(format!(
                            "Field '{}' was already declared on the supertype", name.text
                        ), name.span);
                    }
                }
                let mut ck_var_fields = self.check_field_decs(var_fields, check_type);

                // Bind the variant type
                let var_id = self.gen_ident_id();
                let mut cmb_fields = ck_fields.clone();
                let cmb_text = format!("{}.{}", type_name, var_name.text);
                let mut cmb_name = var_name.clone();
                cmb_name.text = cmb_text.clone();
                cmb_fields.extend(ck_var_fields.drain(..));
                let typ = self.get_type_id(Type::Bound {
                    id: var_id,
                    name: cmb_text.clone(),
                    parent: Some(ident_id),
                });
                let info = TypeInfo {
                    name: cmb_name,
                    id: var_id,
                    typ: typ,
                    fields: cmb_fields,
                    variants: Vec::new(),
                    parent: Some(ident_id),
                };
                eprintln!("Binding type {}", cmb_text);
                self.bind_type(cmb_text, info);

                variant_names.insert(var_name.text.clone());
                variant_ids.push(var_id);
            }

            // Bind the full type
            let typ = self.get_type_id(Type::Bound {
                id: ident_id,
                name: name.text.clone(),
                parent: None,
            });
            let info = TypeInfo {
                name: name.clone(),
                id: ident_id,
                typ: typ,
                fields: ck_fields,
                variants: variant_ids,
                parent: None,
            };
            eprintln!("Binding type {}", name.text.clone());
            self.bind_type(name.text.clone(), info);
        }

        let ck_exprs = self.check_exprs(&ast, true, true);
        let mut ck_exprs = ck_exprs;
        self.fix_let_sizes_in_exprs(&mut ck_exprs);
        let tast = TAst {
            exprs: ck_exprs,
            types: self.type_ids.clone(),
        };
        if ! self.errors.is_empty() {
            Err((tast, self.errors.clone()))
        } else {
            Ok(tast)
        }
    }
}


/// Looks for semantic errors in the program represented by the given AST, and
/// converts it to a typed and bound representation.
pub fn check_ast(ast: &Ast) -> Result<TAst, (TAst, Vec<SemError>)> {
    let mut checker = Checker::new();
    checker.check_ast(ast)
}


