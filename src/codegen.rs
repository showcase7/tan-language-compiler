
use crate::instr::*;
use crate::ast::*;
use crate::util::Span;

use std::collections::HashMap;
use std::rc::Rc;
use std::borrow::Cow;


#[derive(Debug, Clone)]
pub struct StructDef {
    pub field_is_obj: Vec<bool>, 
}

#[derive(Debug, Clone)]
pub struct Program {
    pub instructions: Vec<Instr>,
    pub source_spans: Vec<Span>,
    pub struct_defs: HashMap<TypeId, StructDef>,
    pub top_level_stack_size: usize,
}

fn resolve_jumps(instrs: &[SymInstr]) -> (Vec<Instr>, Vec<Span>) {
    use crate::instr::SymInstrKind::*;
    
    // Find out where the labels are located
    let mut label_indices = HashMap::new();
    let mut i: i64 = 0;
    for sym_instr in instrs {
        match sym_instr.kind {
            Label(ref label) => {
                label_indices.insert(label.id, i);
            }
            _ => {
                i += 1;
            }
        }
    }
    
    // Resolve all jumps to be relative to the current position and to the
    // referenced label.
    let mut instructions = Vec::new();
    let mut source_spans = Vec::new();
    i = 0;
    for sym_instr in instrs {
        match sym_instr.kind {
            NonSym(ref instr) => {
                instructions.push(instr.clone());
                source_spans.push(sym_instr.span);
                i += 1;
            }
            Bnz(ref label) => {
                let offset = label_indices[&label.id] - i;
                instructions.push(Instr::Bnz(offset));
                source_spans.push(sym_instr.span);
                i += 1;
            }
            Bez(ref label) => {
                let offset = label_indices[&label.id] - i;
                instructions.push(Instr::Bez(offset));
                source_spans.push(sym_instr.span);
                i += 1;
            }
            Jmp(ref label) => {
                let offset = label_indices[&label.id] - i;
                instructions.push(Instr::Jmp(offset));
                source_spans.push(sym_instr.span);
                i += 1;
            }
            Call(ref label, arg_size) => {
                let offset = label_indices[&label.id] - i;
                instructions.push(Instr::Call(offset, arg_size));
                source_spans.push(sym_instr.span);
                i += 1;
            }
            Label(_) => {}
        }
    }
    
    (instructions, source_spans)
}

#[derive(Debug)]
enum EnvKind {
    Block(usize),
    Fun(Option<IdentId>),
}

#[derive(Debug)]
struct Env {
    /// The stack slots used by local variables
    /// (excluding return value and return addr).
    local_stack_size: usize,
    outer_stack_size: usize,
    local_stack: Vec<(TypeId, usize)>,
    kind: EnvKind,
}

impl Env {
    fn new(kind: EnvKind) -> Env {
        let outer_stack_size = match kind {
            EnvKind::Block(outer_stack_size) => outer_stack_size,
            EnvKind::Fun(_) => 0,
        };
        Env {
            local_stack_size: 0,
            outer_stack_size: outer_stack_size,
            local_stack: Vec::new(),
            kind: kind,
        }
    }
    
    fn allocate(&mut self, typ: TypeId, size: usize) -> usize {
        //println!("Allocating {} stack space(s) for type {:?}", size, typ);
        self.local_stack.push((typ, size));
        let mut index = self.outer_stack_size + self.local_stack_size;
        // The return value is implicitly at the beginning of the local stack.
        match self.kind {
            EnvKind::Fun(Some(_)) => index += 1,
            _ => {}
        }
        self.local_stack_size += size;
        index
    }
    
    /// Pops a value from the stack in this environment.
    fn pop(&mut self) -> TypeId {
        let (typ, size) = self.local_stack.pop().expect("No values to pop in Env");
        self.local_stack_size -= size;
        typ
    }
    
    /*fn deallocate(&mut self, size: usize) {
        println!("Deallocating {} values from stack of size {}", size, self.local_stack_size);
        assert!(self.local_stack_size >= size);
        self.local_stack_size -= size;
    }*/
}

pub struct CodeGen {
    instructions: Vec<SymInstr>,
    env: Vec<Env>,
    var_indices: HashMap<IdentId, usize>,
    struct_defs: HashMap<TypeId, StructDef>,
    next_label_id: usize,
    functions: HashMap<IdentId, Label>,
    context_name: Rc<Cow<'static, str>>,
    context_instance: usize,
    context_instance_count: HashMap<Rc<Cow<'static, str>>, usize>,
    context_stack: Vec<(Rc<Cow<'static, str>>, usize)>,
    interner: HashMap<Cow<'static, str>, Rc<Cow<'static, str>>>,
    types: Vec<Type>,
}

impl CodeGen {
    pub fn new() -> CodeGen {
        CodeGen {
            instructions: Vec::new(),
            env: vec![Env::new(EnvKind::Block(0))],
            var_indices: HashMap::new(),
            struct_defs: HashMap::new(),
            next_label_id: 0,
            functions: HashMap::new(),
            context_name: Rc::new(Cow::Borrowed("")),
            context_instance: 0,
            context_instance_count: HashMap::new(),
            context_stack: Vec::new(),
            interner: HashMap::new(),
            types: Vec::new(),
        }
    }
    
    fn push(&mut self, instr: Instr, span: Span) {
        self.instructions.push(SymInstr {
            kind: SymInstrKind::NonSym(instr),
            span: span,
        });
    }
    
    fn push_bnz(&mut self, label: Label, span: Span) {
        self.instructions.push(SymInstr {
            kind: SymInstrKind::Bnz(label),
            span: span,
        });
    }
    
    fn push_bez(&mut self, label: Label, span: Span) {
        self.instructions.push(SymInstr {
            kind: SymInstrKind::Bez(label),
            span: span,
        });
    }
    
    fn push_jmp(&mut self, label: Label, span: Span) {
        self.instructions.push(SymInstr {
            kind: SymInstrKind::Jmp(label),
            span: span,
        });
    }
    
    fn push_call(&mut self, label: Label, arg_size: usize, span: Span) {
        self.instructions.push(SymInstr {
            kind: SymInstrKind::Call(label, arg_size),
            span: span,
        });
    }
    
    fn push_label(&mut self, label: Label, span: Span) {
        self.instructions.push(SymInstr {
            kind: SymInstrKind::Label(label),
            span: span,
        });
    }
    
    fn intern<S: Into<Cow<'static, str>>>(&mut self, s: S) -> Rc<Cow<'static, str>> {
        let s = s.into();
        let contained = self.interner.contains_key(&s);
        if ! contained {
            let interned = Rc::new(s.clone());
            self.interner.insert(s, interned.clone());
            interned
        } else {
            self.interner.get(&s).unwrap().clone()
        }
    }
    
    fn push_label_context<S: Into<Cow<'static, str>>>(&mut self, name: S, span: Span) {
        let name = self.intern(name);
        //println!("Push context({}): (new depth: {})", *name, self.context_stack.len() + 1);
        
        let instance = *self.context_instance_count.entry(name.clone()).or_insert(0);
        *self.context_instance_count.get_mut(&name).unwrap() += 1;
        
        self.context_stack.push((self.context_name.clone(), self.context_instance));
        
        self.context_name =  name;
        self.context_instance = instance;
        let lab = self.gen_label("");
        self.push_label(lab, span);
    }
    
    fn pop_label_context(&mut self, span: Span) {
        //println!("Pop context({}): (new depth: {})", *self.context_name, self.context_stack.len() - 1);
        let (context_name, context_instance) = self.context_stack.pop().expect("No context to pop!");
        self.context_name = context_name;
        self.context_instance = context_instance;
        let id = self.next_label_id;
        self.next_label_id += 1;
        let lab = Label {
            context_name: Rc::new("".into()),
            context_instance: 0,
            name: Rc::new("".into()),
            id: id,
            context_depth: self.context_stack.len(),
        };
        self.push_label(lab, span);
    }
    
    fn gen_label<S: Into<Cow<'static, str>>>(&mut self, name: S) -> Label {
        let name = self.intern(name);
        let id = self.next_label_id;
        self.next_label_id += 1;
        Label {
            context_name: self.context_name.clone(),
            context_instance: self.context_instance,
            name: name,
            id: id,
            context_depth: self.context_stack.len(),
        }
    }

    fn fmt_typ(&self, typ: TypeId) -> String {
        let index = (if typ < 0 { -typ } else { typ }) as usize;
        let t = &self.types[index];
        t.fmt(&mut |t| self.fmt_typ(t))
    }

    /// Returns whether a given type is an object type, and thus heap-allocated.
    fn is_obj(&self, typ: TypeId) -> bool {
        let index = (if typ < 0 { -typ } else { typ }) as usize;
        self.types[index].is_obj()
    }

    /// Returns whether a given type is a union type, and thus larger than 
    /// other types.
    fn is_union(&self, typ: TypeId) -> bool {
        let index = (if typ < 0 { -typ } else { typ }) as usize;
        self.types[index].is_union()
    }
    
    /// Binds a variable to the given local stack index.
    fn bind_var(&mut self, var: IdentId, index: usize) {
        self.var_indices.insert(var, index);
    }
    
    #[allow(unused)]
    fn print_stack_slots(&self) {
        println!("Stack slots:");
        let mut indent = 2;
        for subenv in &self.env {
            for _ in 0..indent {
                print!(" ");
            }
            for &(ref typ, typsize) in &subenv.local_stack {
                //print!("{}: {}, ", typ.fmt(|ti| self.fmt_typ), typsize);
            }
            println!("");
            indent += 2;
        }
    }
    
    /// Marks the top of the stack as having a variable of the given type and
    /// increments the tracked stack with the size so that the next free index
    /// is after this value.
    fn push_stack_slot(&mut self, typ: TypeId) -> usize {
        let size = self.size_of(typ);
        let index = self.env.last_mut().expect("No Env").allocate(typ.clone(), size);
        //self.print_stack_slots();
        index
    }
    
    /// Marks the top value of the local stack as having been deallocated.
    fn pop_stack_slot(&mut self) -> TypeId {
        self.env.last_mut().expect("No Env").pop()
    }
    
    /// Generates empty space on the stack for a value of the given type.
    fn gen_alloc(&mut self, typ: TypeId, span: Span) {
        let size = self.size_of(typ);
        self.push(Instr::Alloc(size), span);
    }
    
    /// Generates code to deallocate a union value potentially containing
    /// object pointers.
    fn gen_union_ref_decr(&mut self, span: Span) {
        self.push_label_context("union refdecr", span);
        /*
                [.., tag, value]
        swap    [.., value, tag]
        0       [.., value, tag, 0]
        lt      [.., value, tag<0]
        bez pop [.., value]
    decr:
        refdecr [..]
        jmp end
    pop:
        pop [..]
    end:
        */
        let lab_pop = self.gen_label("pop");
        let lab_end = self.gen_label("end");
        
        self.push(Instr::Swap, span);
        self.push(Instr::PushI(0), span);
        self.push(Instr::Lt, span);
        self.push_bez(lab_pop.clone(), span);
        
        self.push(Instr::RefDecr, span);
        self.push_jmp(lab_end.clone(), span);
        
        self.push_label(lab_pop.clone(), span);
        self.push(Instr::Pop, span);
        
        self.push_label(lab_end.clone(), span);
        
        self.pop_label_context(span);
    }
    
    fn gen_union_ref_incr(&mut self, span: Span) {
        self.push_label_context("union refincr", span);
        // The same as above, aside from incrementing the reference instead
        /*
                 [.., tag, value]
        swap     [.., value, tag]
        0        [.., value, tag, 0]
        lt       [.., value, tag<0]
        bez pop  [.., value]
    incr:
        refincr [..]
        jmp end
    pop:
        pop [..]
    end:
        */
        let lab_pop = self.gen_label("pop");
        let lab_end = self.gen_label("end");
        
        self.push(Instr::Swap, span);
        self.push(Instr::PushI(0), span);
        self.push(Instr::Lt, span);
        self.push_bez(lab_pop.clone(), span);
        
        self.push(Instr::RefIncr, span);
        self.push_jmp(lab_end.clone(), span);
        
        self.push_label(lab_pop.clone(), span);
        self.push(Instr::Pop, span);
        
        self.push_label(lab_end.clone(), span);
        
        self.pop_label_context(span);
    }
    
    fn gen_dealloc(&mut self, typ: TypeId, span: Span) {
        /*println!("Instructions so far:");
        for instr in &self.instructions {
            println!("  {:?}", instr);
        }
        println!("");*/
        match (self.is_obj(typ), self.is_union(typ)) {
            (true, true) => {
                self.gen_union_ref_decr(span);
            }
            (true, false) => {
                self.push(Instr::RefDecr, span);
            }
            _ => {
                let size = self.size_of(typ);
                self.push(Instr::Dealloc(size), span);
            }
        }
    }
    
    fn push_block_env(&mut self) {
        let outer_stack_size = self.env.last().map(|env| {
            env.outer_stack_size + env.local_stack_size
        }).unwrap_or(0);
        self.env.push(Env::new(EnvKind::Block(outer_stack_size)));
    }
    
    fn push_fun_env(&mut self, ret_ident: Option<IdentId>) {
        if let Some(ret_ident) = ret_ident {
            // The return value is always at index 0 relative to the base
            // pointer.
            self.var_indices.insert(ret_ident, 0);
        }
        self.env.push(Env::new(EnvKind::Fun(ret_ident)));
    }
    
    /// Pops the environment and deallocates its local stack.
    fn pop_env(&mut self, span: Span) {
        let mut env = self.env.pop().expect("No Env");
        while let Some((typ, _)) = env.local_stack.pop() {
            //println!("Generating dealloc for type {}", typ.fmt());
            self.gen_dealloc(typ, span);
        }
    }
    
    fn size_of(&self, typ: TypeId) -> usize {
        let index = (if typ < 0 { -typ } else { typ }) as usize;
        match self.types[index] {
            Type::Union(_) => 2,
            _ => 1,
        }
    }
    
    pub fn instructions(&self) -> &[SymInstr] {
        &self.instructions
    }
    
    // ================== Greneations functions ========================
    
    /// Generates an isolated block of statements with an optional index to
    /// store a returned value at.
    /// The return slot is necessary in order to allow local variables on the
    /// stack (so that the block can clean its env of local vars at the end).
    fn gen_block(&mut self, 
        exprs: &[TExpr], ret: Option<(usize, TypeId)>, 
        type_id: Option<TypeId>, span: Span
    ) {
        let has_value = ret.is_some();
        
        self.push_block_env();
        
        self.gen_exprs(exprs, span, has_value);
        
        if let Some(tag) = type_id {
            self.push(Instr::PushI(tag), span);
            self.push(Instr::Swap, span);
        }
        
        // Assign the return value
        if let Some((ret_index, ret_typ)) = ret {
            self.push(Instr::GetBasePtr, span);
            self.push(Instr::PushI(ret_index as i64), span);
            self.push(Instr::Add, span);
            
            let size = self.size_of(ret_typ);
            
            self.push(Instr::MoveDown(size), span);
            self.push(Instr::Sti(size), span);
        }
        
        self.pop_env(span);
    }
    
    fn gen_expr(&mut self, expr: &TExpr, expect_value: bool) {
        use crate::ast::TExpr::*;

        //eprintln!("Generating expr...");
        //eprintln!("expr: {:#?}", expr);
        
        #[allow(unreachable_patterns)]
        match *expr {
            Integer { value, span, .. } => {
                self.push(Instr::PushI(value), span);
            }
            Binary { ref left, op: BinOp::And, ref right, span, .. } => {
                self.gen_and(left, right, span);
            }
            Binary { ref left, op: BinOp::Or, ref right, span, .. } => {
                self.gen_or(left, right, span);
            }
            Binary { ref left, op, ref right, span, .. } => {
                self.gen_bin_expr(left, op, right, span);
            }
            Access { ref loc, span, .. } => {
                self.gen_acc(loc, span);
            }
            Bool { value, span, .. } => {
                let value = if value { 1 } else { 0 };
                self.push(Instr::PushI(value), span);
            }
            Unary { op, ref value, span, .. } => {
                self.gen_expr(value, true);
                match op {
                    UnOp::Not => self.push(Instr::Not, span),
                    UnOp::Neg => self.push(Instr::Neg, span),
                }
            }
            If {
                ref cond, ref if_body, if_tag, ref else_body, else_tag, 
                typ, span,
            } => {
                self.gen_if(cond, if_body, if_tag, else_body, else_tag, span, typ, expect_value);
            }
            Do { ref body, typ, span, } => {
                self.gen_do(body, span, typ, expect_value);
            }
            Call { name, typ, ref args, span } => {
                self.gen_call(name, typ, args, span);
            }
            CallBuiltin { ref builtin, ref args, span, .. } => {
                self.push_label_context(format!("builtin_{}", builtin.fmt()), span);
                
                // Don't allocate a return type since CallBuiltin is different
                for arg in args {
                    self.gen_expr(arg, true);
                }
                self.push(Instr::CallBuiltin(builtin.clone()), span);
                
                self.pop_label_context(span);
            }
            Constructor { typ, ref assignments, span } => {
                self.gen_constructor(typ, assignments, span);
            }
            Nil { .. } => {
                // Do nothing. 
                // The value is pushed after this match statement if necessary
            }
            Panic { ref inner, ref message, loc, span, .. } => {
                match loc {
                    PanicLoc::Before => {
                        self.push(Instr::Panic(message.clone()), span);
                        self.gen_expr(inner, expect_value);
                    }
                    PanicLoc::After => {
                        self.gen_expr(inner, expect_value);
                        self.push(Instr::Panic(message.clone()), span);
                    }
                }
            }
            Reassign { ref loc, ref value, span, .. } => {
                self.gen_reassign(loc, value, span);
            }
            UnwrapUnion { ref value, ref expected, ref other_cases, span, .. } => {
                self.gen_unwrap_union(value, expected, other_cases, span);
            }
            Union { tag, ref value, span, .. } => {
                self.push_label_context("tag_union", span);
                // TODO: How does this play into 'expect-value' ???
                self.push(Instr::PushI(tag), value.span());
                self.gen_expr(value, true);
                self.pop_label_context(span);
            }
            Let { is_mut: _is_mut, ref var, store_typ, ref value, span, .. } => {
                self.push_label_context("let", span);
                // Put the value on the stack
                self.gen_expr(value, true);
                
                // Mark that this value is now the variable location
                let index = if let Some(store_typ) = store_typ {
                    let size_diff = self.size_of(store_typ) - self.size_of(value.typ());
                    if size_diff != 0 {
                        self.push(Instr::Alloc(size_diff), span);
                    }
                    self.push_stack_slot(store_typ)
                } else {
                    self.push_stack_slot(value.typ())
                };
                
                //println!("  Let '{}' got index {}", var.name, index);
                self.bind_var(var.id, index);
                self.pop_label_context(span);
            }
            Assign { ref acc, op, ref value, span, .. } => {
                self.gen_assign(acc, op, value, span);
            }
            While { ref cond, ref body, span, .. } => {
                self.gen_while(cond, body, span);
            }
            For { ref ident, ref iterable, ref body, span, .. } => {
                self.gen_for(ident, iterable, body, span);
            }
            FunDec { ref name, ref args, ref ret, ref body, span, .. } => {
                self.gen_fun_dec(name, args, ret, body, span);
            }
            TagVar { ref ident, typ, span, .. } => {
                self.gen_tag_var(ident, typ, span);
            }
            _ => unimplemented!("gen_expr: {:?}", expr),
        }
        
        match (expect_value, expr.typ() == TYPE_NIL) {
            // If the expression is a nil expression but needs a value
            (true, true) => {
                self.gen_nil(expr.span());
            }
            // If a value is not desired and the expression generates one:
            (false, false) => {
                self.gen_dealloc(expr.typ(), expr.span());
            }
            _ => {}
        }
    }
    
    fn gen_nil(&mut self, span: Span) {
        self.push(Instr::PushI(999), span);
    }
    
    fn gen_addr(&mut self, loc: &TLoc, load: bool) {
        use crate::ast::TLocKind::*;
        
        #[allow(unreachable_patterns)]
        match loc.kind {
            Var(ref var) => {
                if let Some(&index) = self.var_indices.get(&var.id) {
                    self.push(Instr::GetBasePtr, loc.span);
                    self.push(Instr::PushI(index as i64), loc.span);
                    self.push(Instr::Add, loc.span);
                    if load {
                        let size = self.size_of(loc.typ);
                        self.push(Instr::Ldi(size), loc.span);
                    }
                } else {
                    let msg = format!("Reference to unbound variable '{}'", var.text);
                    self.push(Instr::Panic(msg), loc.span);
                }
            }
            Indexing(ref arr, ref index) => {
                self.gen_expr(arr, true);
                self.gen_expr(index, true);
                if load {
                    self.push(Instr::CallBuiltin(Builtin::GetIndex), loc.span);
                }
            }
            Field(ref expr, index) => {
                self.gen_expr(expr, true);
                if load {
                    self.push(Instr::FieldGet(index), loc.span);
                }
            }
            Panic(ref inner, ref msg, span) => {
                self.push(Instr::Panic(msg.clone()), span);
                self.gen_addr(inner, load);
            }
            Missing => {}
            _ => {
                unimplemented!("gen_addr for {:?}", loc.kind);
            }
        }
    }
    
    fn gen_acc(&mut self, loc: &TLoc, span: Span) {
        self.gen_addr(loc, true);
        match (self.is_obj(loc.typ), self.is_union(loc.typ)) {
            (true, false) => {
                self.push(Instr::Dup(1), span);
                self.push(Instr::RefIncr, span);
            }
            (true, true) => {
                // Unions can only be in variables.
                self.push(Instr::Dup(2), span);
                self.gen_union_ref_incr(span);
            }
            _ => {}
        }
    }
    
    fn gen_and(&mut self, left: &TExpr, right: &TExpr, span: Span) {
        /*
        <left>
        bnz right
        push 0
        jmp end
    right:
        <right>
        bnz true
        push 0
        jmp end
    true:
        push 1
    end:
        */
        self.push_label_context("and", span);
        
        let right_lab = self.gen_label("right");
        let true_lab = self.gen_label("true");
        let end_lab = self.gen_label("end");
        
        self.gen_expr(left, true);
        self.push_bnz(right_lab.clone(), span);
        self.push(Instr::PushI(0), span);
        self.push_jmp(end_lab.clone(), span);
        
        self.push_label(right_lab.clone(), span);
        self.gen_expr(right, true);
        self.push_bnz(true_lab.clone(), span);
        self.push(Instr::PushI(0), span);
        self.push_jmp(end_lab.clone(), span);
        
        self.push_label(true_lab.clone(), span);
        self.push(Instr::PushI(1), span);
        
        self.push_label(end_lab.clone(), span);
        
        self.pop_label_context(span);
    }
    
    fn gen_or(&mut self, left: &TExpr, right: &TExpr, span: Span) {
        /*
        <left>
        bnz true
        <right>
        bnz true
        push 0
        jmp end
    true:
        push 1
    end:
        */
        self.push_label_context("or", span);
        
        let true_lab = self.gen_label("true");
        let end_lab = self.gen_label("end");
        
        self.gen_expr(left, true);
        self.push_bnz(true_lab.clone(), span);
        self.gen_expr(right, true);
        self.push_bnz(true_lab.clone(), span);
        self.push(Instr::PushI(0), span);
        self.push_jmp(end_lab.clone(), span);
        
        self.push_label(true_lab.clone(), span);
        self.push(Instr::PushI(1), span);
        
        self.push_label(end_lab.clone(), span);
        
        self.pop_label_context(span);
    }
    
    fn gen_bin_expr(&mut self, left: &TExpr, op: BinOp, right: &TExpr, span: Span) {
        use crate::ast::BinOp::*;
        
        self.gen_expr(left, true);
        self.gen_expr(right, true);
        match op {
            Add => self.push(Instr::Add, span),
            Sub => self.push(Instr::Sub, span),
            Mul => self.push(Instr::Mul, span),
            Div => self.push(Instr::Div, span),
            Mod => self.push(Instr::Mod, span),
            
            Eq => self.push(Instr::Eq, span),
            NotEq => {
                self.push(Instr::Eq, span);
                self.push(Instr::Not, span);
            }
            
            Lt => self.push(Instr::Lt, span),
            LtEq => {
                self.push(Instr::Gt, span);
                self.push(Instr::Not, span);
            }
            Gt => self.push(Instr::Gt, span),
            GtEq => {
                self.push(Instr::Lt, span);
                self.push(Instr::Not, span);
            }
            _ => unreachable!(),
        }
    }
    
    fn gen_call(&mut self, fun_ident: IdentId, ret_type: TypeId, args: &[TExpr], span: Span) {
        let fun_name = if let Some(label) = self.functions.get(&fun_ident) {
            label.context_name.clone()
        } else {
            Rc::new("UNKNOWN".into())
        };
        self.push_label_context(format!("call_{}", fun_name), span);
        // Allocate a return type
        if ret_type != TYPE_NIL {
            self.gen_alloc(ret_type, span);
        }
        
        // Prepare the args
        for arg in args {
            self.gen_expr(arg, true);
        }
        
        // Call the function
        if let Some(label) = self.functions.get(&fun_ident).map(|lab| lab.clone()) {
            let arg_size = args.len() + if ret_type != TYPE_NIL { 1 } else { 0 };
            self.push_call(label, arg_size, span);
        } else {
            let msg = format!("unreachable: Call to unresolved function");
            self.push(Instr::Panic(msg), span);
        }
        
        self.pop_label_context(span);
    }
    
    fn gen_constructor(&mut self, typ: TypeId, assignments: &[(usize, TExpr)], span: Span) {
        self.push_label_context(format!("new {}", self.fmt_typ(typ)), span);
        
        self.push(Instr::AllocStruct(typ, assignments.len()), span);
        for &(index, ref value) in assignments {
            self.push(Instr::Dup(1), span);
            self.gen_expr(value, true);
            self.push(Instr::FieldSet(index), span);
            self.push(Instr::Pop, span);
        }
        
        self.pop_label_context(span);
    }
    
    fn gen_if(&mut self, 
        cond: &TExpr, ifbody: &[TExpr], if_tag: Option<TypeId>, elsebody: &[TExpr], 
        else_tag: Option<TypeId>, span: Span, typ: TypeId, expect_value: bool,
    ) {
        /*
        <alloc ret slot>
        <gen cond>
        bez else
    if:
        <ifbody>
        jmp end
    else:
        <elsebody>
    end:
        <tag union>
        */
        self.push_label_context("if", span);
        
        let if_lab = self.gen_label("if");
        let else_lab = self.gen_label("else");
        let end_lab = self.gen_label("end");
        //let expect_value = has_ret_type;
        
        // Generate the return value slot
        let mut ret = None;
        if expect_value {
            let index = self.push_stack_slot(typ);
            //println!("  Ret slot got index {}", index);
            ret = Some((index, typ));
            self.gen_alloc(typ, span);
        }
        
        self.gen_expr(cond, true);
        self.push_bez(else_lab.clone(), span);
        
        // ======== If body =================
        self.push_label(if_lab, span);
        self.gen_block(ifbody, ret, if_tag, span);
        self.push_jmp(end_lab.clone(), span);
        
        // ========= Else body ===============
        self.push_label(else_lab.clone(), span);
        self.gen_block(elsebody, ret, else_tag, span);
        self.push_label(end_lab.clone(), span);
        
        // 'unmark' the return value (so that it's just a value on the)
        // stack instead of a 'local'
        if expect_value {
            self.pop_stack_slot();
        }
        
        self.pop_label_context(span);
    }
    
    fn gen_do(&mut self, body: &[TExpr], span: Span, typ: TypeId, expect_value: bool) {
        self.push_label_context("do", span);
        
        // Generate the return value slot
        let mut ret = None;
        if expect_value {
            let index = self.push_stack_slot(typ);
            //println!("  Ret slot got index {}", index);
            ret = Some((index, typ));
            self.gen_alloc(typ, span);
        }
        
        self.gen_block(body, ret, None, span);
        
        
        // 'unmark' the return value (so that it's just a value on the)
        // stack instead of a 'local'
        if expect_value {
            self.pop_stack_slot();
        }
        
        self.pop_label_context(span);
    }
    
    /// Reassigns a value to a variable before leaving the value on the stack
    fn gen_reassign(&mut self, loc: &TLoc, value: &TExpr, span: Span) {
        self.gen_expr(value, true);
        assert_eq!(self.size_of(value.typ()), 1);
        self.push(Instr::Dup(1), span);
        self.gen_addr(loc, false);
        self.push(Instr::Swap, span);
        self.push(Instr::Sti(1), span);
    }
    
    fn gen_unwrap_union(&mut self, 
        value: &TExpr, expected: &(TypeId, String), 
        other_cases: &[(TypeId, String, String)], span: Span
    ) {
        /*
        <value> [.., tag, val]
        swap [.., val, tag]
        
        dup [.., val, tag, tag]
        <exptag> [.., val, tag, tag, exptag]
        eq [.., val, tag, tag==exptag]
        bnz exptype
        
        dup
        <tag0>
        eq
        bnz type0
        
        dup
        <tag1>
        eq
        bnz type1
        
        ...
        
        panic("unreachable: unhandled union tag")
        
    exptype: [.., val, tag]
        pop [.., val]
        jmp end
    type0:
        panic(msg0)
    type1: 
        ...
    ...
    end:
        */
        
        self.gen_expr(value, true);
        
        self.push_label_context("unwrap_union", span);
        
        let (exp_tag, exp_name) = expected.clone();
        let exp_lab = self.gen_label(exp_name);
        let end_lab = self.gen_label("end");
        
        let mut other_labels = Vec::new();
        for &(_, ref type_name, _) in other_cases {
            let case_lab = self.gen_label(type_name.clone());
            other_labels.push(case_lab);
        }
        
        self.push(Instr::Swap, span);
        self.push(Instr::Dup(1), span);
        self.push(Instr::PushI(exp_tag), span);
        self.push(Instr::Eq, span);
        self.push_bnz(exp_lab.clone(), span);
        
        for (i, &(tag, _, _)) in other_cases.iter().enumerate() {
            self.push(Instr::Dup(1), span);
            self.push(Instr::PushI(tag), span);
            self.push(Instr::Eq, span);
            self.push_bnz(other_labels[i].clone(), span);
        }
        
        self.push(Instr::Panic("Unreachable".to_string()), span);
        
        self.push_label(exp_lab, span);
        self.push(Instr::Pop, span);
        self.push_jmp(end_lab.clone(), span);
        
        for (i, &(_, _, ref msg)) in other_cases.iter().enumerate() {
            self.push_label(other_labels[i].clone(), span);
            self.push(Instr::Panic(msg.clone()), span);
        }
        
        self.push_label(end_lab, span);
        
        self.pop_label_context(span);
    }
    
    /*fn gen_recast_union(&mut self, 
        value: &TExpr, expected_cases: &[(TypeId, String)], 
        other_cases: &[(TypeId, String, String)], span: Span
    ) {
        // Basically the same as an unwrap, but with multiple valid cases
        // and no tag removal.
        self.gen_expr(value, true);
        
        self.push_label_context("recast_union", span);
        
        let mut exp_labels = Vec::new();
        for &(_, ref name) in expected_cases {
            let label = self.gen_label(name.clone());
            exp_labels.push(label);
        }
        
        let end_lab = self.gen_label("end");
        
        let mut other_labels = Vec::new();
        for &(_, ref type_name, _) in other_cases {
            let case_lab = self.gen_label(type_name.clone());
            other_labels.push(case_lab);
        }
        
        self.push(Instr::Swap, span);
        
        for (i, &(exp_tag, _)) in expected_cases.iter().enumerate() {
            let exp_lab = exp_labels[i].clone();
            self.push(Instr::Dup(1), span);
            self.push(Instr::PushI(exp_tag), span);
            self.push(Instr::Eq, span);
            self.push_bnz(exp_lab, span);
        }
        
        for (i, &(tag, _, _)) in other_cases.iter().enumerate() {
            self.push(Instr::Dup(1), span);
            self.push(Instr::PushI(tag), span);
            self.push(Instr::Eq, span);
            self.push_bnz(other_labels[i].clone(), span);
        }
        
        self.push(Instr::Panic("Unreachable".to_string()), span);
        
        for exp_lab in exp_labels {
            self.push_label(exp_lab, span);
            // Move the tag back :)
            self.push(Instr::Swap, span);
            self.push_jmp(end_lab.clone(), span);
        }
        
        for (i, &(_, _, ref msg)) in other_cases.iter().enumerate() {
            self.push_label(other_labels[i].clone(), span);
            self.push(Instr::Panic(msg.clone()), span);
        }
        
        self.push_label(end_lab, span);
        
        self.pop_label_context(span);
    }*/
    
    
    /// Tags a variable with its union tag (converting it to a union storage).
    fn gen_tag_var(&mut self, var: &TIdent, type_id: TypeId, span: Span) {
        self.push_label_context("tag_var", span);
        
        /*
                    [..]
        <gen_addr>  [.., addr]
        dup 1       [.., addr, addr]
        ldi 1       [.., addr, val]
        swap        [.., val, addr]
        dup 1       [.., val, addr, addr]
        push <tag>  [.., val, addr, addr, tag]
        sti 1       [.., val, addr]
        push 1      [.., val, addr, 1]
        add         [.., val, addr+1]
        swap        [.., addr+1, val]
        sti 1       [..]
        */
        
        if let Some(&index) = self.var_indices.get(&var.id) {
            self.push(Instr::GetBasePtr, span);
            self.push(Instr::PushI(index as i64), span);
            self.push(Instr::Add, span);
        } else {
            let msg = format!("Reference to unbound variable '{}'", var.text);
            self.push(Instr::Panic(msg), span);
        }
        self.push(Instr::Dup(1), span);
        self.push(Instr::Ldi(1), span);
        self.push(Instr::Swap, span);
        self.push(Instr::Dup(1), span);
        self.push(Instr::PushI(type_id), span);
        self.push(Instr::Sti(1), span);
        self.push(Instr::PushI(1), span);
        self.push(Instr::Add, span);
        self.push(Instr::Swap, span);
        self.push(Instr::Sti(1), span);
        
        self.pop_label_context(span);
    }
    
    fn gen_assign(&mut self, acc: &TExpr, op: AssOp, value: &TExpr, span: Span) {
        use crate::ast::TLocKind::*;
        
        self.push_label_context(format!("assign({})", op.fmt()), span);
        if op == AssOp::Normal {
            // If the access doesn't panic
            if let Some(loc) = acc.loc() {
                
                self.gen_addr(loc, false);
                
                // If we're overwriting an object reference
                if self.is_obj(loc.typ()) {
                    // Load the ptr and decrease the refcount
                    #[allow(unreachable_patterns)]
                    match loc.kind {
                        Var(_) => {
                            // Duplicate the var stack index
                            self.push(Instr::Dup(1), span);
                            
                            // Handle unions in which not all members are 
                            // necessarily object pointers.
                            if self.is_union(loc.typ()) {
                                self.push(Instr::Ldi(2), span);
                                self.gen_union_ref_decr(span);
                            } else {
                                self.push(Instr::Ldi(1), span);
                                self.push(Instr::RefDecr, span);
                            }
                        }
                        Indexing(_, _) => {
                            self.push(Instr::Dup(2), span);
                            self.push(Instr::CallBuiltin(Builtin::GetIndex), span);
                            self.push(Instr::RefDecr, span);
                        }
                        Panic(_, _, _) => {}
                        Missing => {}
                        Field(_, index) => {
                            self.push(Instr::Dup(1), span);
                            self.push(Instr::FieldGet(index), span);
                            self.push(Instr::RefDecr, span);
                        }
                        _ => unimplemented!("gen_assign for {:?}", loc.kind),
                    }
                }
                
                // TODO: Handle union types containing objects :|
                
                self.gen_expr(value, true);
                
                // Store the value
                #[allow(unreachable_patterns)]
                match loc.kind {
                    Var(_) => {
                        //println!("Generating assignment of value: {:#?}", value);
                        let size = self.size_of(value.typ());
                        //println!("Storage size: {}", size);
                        self.push(Instr::Sti(size), span);
                    }
                    Indexing(_, _) => {
                        self.push(Instr::CallBuiltin(Builtin::SetIndex), span);
                    }
                    Panic(_, _, _) => {}
                    Missing => {}
                    Field(_, index) => {
                        self.push(Instr::FieldSet(index), span);
                        self.push(Instr::Pop, span);
                    }
                    _ => unimplemented!("gen_assign for {:?}", loc.kind),
                }
            
            // Panic
            } else {
                self.gen_expr(acc, false);
            }
        
        } else {
            // Generate the address
            if let Some(loc) = acc.loc() {
                self.gen_addr(loc, false);
            }
            
            // If this is a different operator, we need to use the access to gen
            // the value before reassigning it (potentially reassigning a var)
            self.gen_expr(acc, true);
            
            // Apply the operation
            self.gen_expr(value, true);
            match op {
                AssOp::Normal => {},
                AssOp::Add => self.push(Instr::Add, span),
                AssOp::Sub => self.push(Instr::Sub, span),
                AssOp::Mul => self.push(Instr::Mul, span),
                AssOp::Div => self.push(Instr::Div, span),
                AssOp::Mod => self.push(Instr::Mod, span),
            };
            
            // Store the new value
            if let Some(loc) = acc.loc() {
                #[allow(unreachable_patterns)]
                match loc.kind {
                    Var(_) => {
                        //println!("Generating assignment of value: {:#?}", value);
                        let size = self.size_of(value.typ());
                        //println!("Storage size: {}", size);
                        self.push(Instr::Sti(size), span);
                    }
                    Indexing(_, _) => {
                        self.push(Instr::CallBuiltin(Builtin::SetIndex), span);
                    }
                    Field(_, index) => {
                        self.push(Instr::FieldSet(index), span);
                        self.push(Instr::Pop, span);
                    }
                    Panic(_, _, _) => {}
                    Missing => {}
                    _ => unimplemented!("gen_assign for {:?}", loc.kind),
                }
            }
        }
        
        self.pop_label_context(span);
    }
    
    fn gen_while(&mut self, cond: &TExpr, body: &[TExpr], span: Span) {
        /*
    loop:
        <cond>
        not
        bnz end
        <body>
        jmp loop
    end:
        */
        self.push_label_context("while", span);
        let loop_lab = self.gen_label("loop");
        let end_lab = self.gen_label("end");
        
        self.push_label(loop_lab.clone(), span);
        
        self.gen_expr(cond, true);
        self.push(Instr::Not, span);
        self.push_bnz(end_lab.clone(), span);
        
        self.gen_block(body, None, None, span);
        
        self.push_jmp(loop_lab.clone(), span);
        
        self.push_label(end_lab.clone(), span);
    }
    
    fn gen_for(&mut self, ident: &TIdent, iterable: &TIterable, body: &[TExpr], span: Span) {
        /*
        [index, max_index, iterable?, var? | for...]
        <start>
        <end>
        #if not range:
            <var>
            <iterable>
            
    loop:
        &index
        ldi
        &max_index
        ldi
        lt
        not
        bnz end
        // Load the value at this index into var
        #if not range:
            &var
            &iterable
            &index
            ldi
            sti

        <body>
        
        &index
        dup
        ldi
        1
        add
        sti
        
        jmp loop
    cleanup:
        #if not range:
            pop <iterable>
            pop <var>
        pop <max_index>
        pop <index>
    end:
        */
        self.push_block_env();
        
        self.push_label_context("for", span);
        let loop_lab = self.gen_label("loop");
        let cleanup_lab = self.gen_label("cleanup");
        let end_lab = self.gen_label("end");
        
        // Setup the environment and variables
        let mut var_index = None;
        let mut iterable_index = None;
        let index_index;
        let max_index;
        match iterable.kind {
            TIterableKind::Range(ref start, ref end) => {
                index_index = self.push_stack_slot(TYPE_INT);
                self.gen_expr(start, true);
                
                max_index = self.push_stack_slot(TYPE_INT);
                self.gen_expr(end, true);
                
                self.bind_var(ident.id, index_index);
            }
            TIterableKind::Expr(ref expr) => {
                index_index = self.push_stack_slot(TYPE_INT);
                self.gen_alloc(TYPE_INT, ident.span);
                
                // Get len of iterable
                // Gen the iterable (or panic here)
                self.gen_expr(expr, true);
                
                // Update ref count
                self.push(Instr::Dup(1), span);
                self.push(Instr::RefIncr, span);
                
                // Dup it to reuse it for later
                self.push(Instr::Dup(1), span);
                self.push(Instr::CallBuiltin(Builtin::VecLen), span);
                
                // Swap it
                self.push(Instr::Swap, span);
                max_index = self.push_stack_slot(TYPE_INT);
                iterable_index = Some(self.push_stack_slot(expr.typ()));
                
                // Prepare the variable
                self.gen_alloc(iterable.item_type, ident.span);
                var_index = Some(self.push_stack_slot(iterable.item_type));
                self.bind_var(ident.id, var_index.unwrap());
            }
        }
        //println!("Env: {:?}", self.env);
        //println!("Indices: index: {}, max: {}, var: {:?}, iter: {:?}", index_index, max_index, var_index, iterable_index);
        
        self.push_label(loop_lab.clone(), span);
        
        // Check the loop condition
        
        self.push(Instr::GetBasePtr, span);
        self.push(Instr::PushI(index_index as i64), span);
        self.push(Instr::Add, span);
        self.push(Instr::Ldi(self.size_of(TYPE_INT)), span);
        
        self.push(Instr::GetBasePtr, span);
        self.push(Instr::PushI(max_index as i64), span);
        self.push(Instr::Add, span);
        self.push(Instr::Ldi(self.size_of(TYPE_INT)), span);
        
        self.push(Instr::Lt, span);
        self.push_bez(cleanup_lab.clone(), span);
        
        // Load the item into the var if necessary
        if let TIterableKind::Expr(_) = iterable.kind {
            // Load the var addr
            self.push(Instr::GetBasePtr, span);
            self.push(Instr::PushI(var_index.unwrap() as i64), span);
            self.push(Instr::Add, span);
            
            // Load the iterable
            self.push(Instr::GetBasePtr, span);
            self.push(Instr::PushI(iterable_index.unwrap() as i64), span);
            self.push(Instr::Add, span);
            self.push(Instr::Ldi(self.size_of(TYPE_INT)), span);
            
            // Load the index
            self.push(Instr::GetBasePtr, span);
            self.push(Instr::PushI(index_index as i64), span);
            self.push(Instr::Add, span);
            self.push(Instr::Ldi(self.size_of(TYPE_INT)), span);
            
            // Read the iterator value at the given index
            self.push(Instr::CallBuiltin(Builtin::GetIndex), span);
            
            // Load it into the 'for' variable
            self.push(Instr::Sti(self.size_of(iterable.item_type)), span);
        }
        
        // Execute the body
        self.push_block_env();
        self.gen_exprs(body, span, false);
        self.pop_env(span);
        
        // Increment the index (counter)
        self.push(Instr::GetBasePtr, span);
        self.push(Instr::PushI(index_index as i64), span);
        self.push(Instr::Add, span);
        self.push(Instr::Dup(1), span);
        
        self.push(Instr::Ldi(self.size_of(TYPE_INT)), span);
        self.push(Instr::PushI(1), span);
        self.push(Instr::Add, span);
        
        self.push(Instr::Sti(self.size_of(TYPE_INT)), span);
        
        // Restart the loop
        self.push_jmp(loop_lab.clone(), span);
        
        self.push_label(cleanup_lab.clone(), span);
        
        // Clean up
        self.pop_env(span);
        
        // Mark end
        self.push_label(end_lab.clone(), span);
    }
    
    fn gen_fun_dec(&mut self, 
        ident: &TIdent, args: &[(TIdent, TypeId)], ret: &Option<(TypeId, IdentId)>, 
        body: &[TExpr], span: Span
    ) {
        self.push_label_context(format!("fn_{}", ident.text.clone()), span);
        
        let fun_lab = self.gen_label("start");
        let end_lab = self.gen_label("end");
        
        // Don't execute the function when it is reached.
        self.push_jmp(end_lab.clone(), span);
        
        // Mark the start of the function
        self.functions.insert(ident.id, fun_lab.clone());
        self.push_label(fun_lab.clone(), span);
        
        // Update the scope and allocate the return variable
        let ret_ident = ret.as_ref().map(|&(_, ident)| ident);
        self.push_fun_env(ret_ident);
        
        // Bind and allocate the arguments.
        for &(ref ident, typ) in args {
            let index = self.push_stack_slot(typ);
            self.bind_var(ident.id, index);
        }
        
        // Generate the body
        self.gen_exprs(body, span, ret.is_some());
        
        // If the body has an implicit return
        if let &Some((ret_typ, _)) = ret {
            // Assume an expr is on top of the stack 
            // (or there is a panic from the semchecker)
            self.push(Instr::GetBasePtr, span);
            self.push(Instr::Swap, span);
            let size = self.size_of(ret_typ);
            self.push(Instr::Sti(size), span);
        }
        
        // Clean up and return
        self.pop_env(span);
        let size = if let &Some((ret_typ, _)) = ret {
            self.size_of(ret_typ)
        } else {
            0
        };
        self.push(Instr::Ret(size), span);
        
        self.push_label(end_lab.clone(), span);
        
        self.pop_label_context(span);
    }
    
    fn gen_exprs(&mut self, exprs: &[TExpr], span: Span, expect_value: bool) {
        //eprintln!("Generating exprs...");
        let last = if ! exprs.is_empty() { 
            exprs.len() - 1 
        } else { 
            0 
        };
        for (i, expr) in exprs.iter().enumerate() {
            let expect_val = expect_value && (i == last);
            self.gen_expr(expr, expect_val);
        }
        if exprs.is_empty() && expect_value {
            self.gen_nil(span);
        }
    }
    
    pub fn gen(&mut self, ast: &TAst) -> Program {
        let span = ast.exprs.last().map(|e| e.span()).unwrap_or(Span::new(0, 0));
        self.types = ast.types.clone();
        self.gen_exprs(&ast.exprs, span, true);
        let (instrs, spans) = resolve_jumps(&self.instructions);
        Program {
            instructions: instrs,
            source_spans: spans,
            struct_defs: self.struct_defs.clone(),
            top_level_stack_size: self.env[0].local_stack_size,
        }
    }
}

/// Generates the bytecode for the program represented by the given AST.
pub fn generate_code(ast: &TAst) -> Program {
    let mut cg = CodeGen::new();
    cg.gen(ast)
}







