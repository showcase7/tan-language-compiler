
use std::fs::File;
use std::io::Read;
use std::error::Error;
use std::iter;

use crate::vm::RtError;
use crate::check::SemError;

pub fn load_and_run(file: &str, 
    expected_errors: Vec<SemError>, 
    expected_result: Result<Option<i64>, RtError>,
    expected_rc: Vec<usize>,
) {
    let mut f = match File::open(file) {
        Ok(f) => f,
        Err(err) => {
            panic!(format!("Could not open file '{}': {}", file, err.description()));
        }
    };
    let mut text = String::new();
    match f.read_to_string(&mut text) {
        Ok(_) => {}
        Err(err) => {
            panic!(format!("Could not read file '{}': {}", file, err.description()));
        }
    };
    let ast = match crate::parse::parse(&text) {
        Ok(ast) => {
            ast
        }
        Err(err) => {
            println!("Parse error:");
            println!("{}:{}: {}", err.lineno, err.col, err.message);
            println!("{}", err.line);
            let line = std::iter::repeat('~').take(err.col - 1).collect::<String>();
            println!("{}^", line);
            panic!("Parsing error");
        }
    };
    let (ck_ast, errors) = match crate::check::check(&ast) {
        Ok(ast) => {
            println!("  The AST is valid!");
            (ast, Vec::new())
        }
        Err((ast, errors)) => {
            println!("Semantic errors:");
            for error in &errors {
                println!("  {:?}", error);
            }
            (ast, errors)
        }
    };
    
    let program = crate::codegen::gen(&ck_ast);
    
    println!("Program:");
    for (i, instr) in program.instructions.iter().enumerate() {
        println!("{:03} {:?}", i, instr);
    }
    
    let (res, heap_rc) = crate::vm::run_tracing(&program);
    
    match res {
        Ok(Some(ref val)) => {
            println!("");
            println!("=> {:?}", val);
        }
        Ok(None) => {
            println!("");
            println!("=> ()");
        }
        Err(ref error) => {
            println!("Runtime error:");
            let (lineno, col, line) = crate::util::line_info(error.span.start, &text);
            
            if lineno > 1 {
                let prev = text.lines().skip(lineno - 2).next().unwrap();
                println!("{:03}: {}", lineno - 1, prev);
            }
            
            println!("{:03}: {}", lineno, line);
            let len = error.span.end - error.span.start;
            let pad = iter::repeat(' ').take(col-1).collect::<String>();
            let mark = if len > 1 {
                iter::repeat('~').take(len - 1).collect::<String>()
            } else {
                String::new()
            };
            println!("     {}^{}", pad, mark);
            
            if let Some(line) = text.lines().skip(lineno).next() {
                println!("{:03}: {}", lineno+1, line);
            }
            
            println!("");
            println!("{}:{}: {}", lineno, col, error.message);
        }
    }
    
    assert_eq!(expected_errors, errors);
    assert_eq!(expected_result, res);
    assert_eq!(expected_rc, heap_rc);
}

mod arith {
    use crate::test::load_and_run;
    
    #[test]
    fn minus_2() {
        load_and_run("test/arith/t_minus_2.tan", vec![], Ok(Some(-2)), vec![]);
    }
    
    #[test]
    fn arith() {
        load_and_run("test/arith/t_arith.tan", vec![], Ok(Some(-1)), vec![]);
    }
}

mod bool_ {
    use crate::test::load_and_run;
    use crate::check::SemError;
    use crate::util::Span;
    use crate::vm::RtError;
    
    #[test]
    fn false_or_2() {
        load_and_run("test/bool/f_false_or_2.tan", vec![
            SemError::TypeError { 
                expected: "bool".to_string(), 
                actual: "int".to_string(), 
                span: Span { start: 9, end: 10 } 
            }], 
            Err(RtError { 
                message: "Panic: Expected type bool, got int".to_string(), 
                 span: Span { start: 9, end: 10 } 
            }), 
            vec![]
        );
    }
    
    #[test]
    fn eq() {
        load_and_run("test/bool/t_eq.tan", vec![], 
            Ok(Some(1)), 
            vec![]
        );
    }
    
    #[test]
    fn false_() {
        load_and_run("test/bool/t_false.tan", vec![], 
            Ok(Some(0)), 
            vec![]
        );
    }
    
    #[test]
    fn not_not_true() {
        load_and_run("test/bool/t_not_not_true.tan", vec![], 
            Ok(Some(1)), 
            vec![]
        );
    }
    
    #[test]
    fn not_true() {
        load_and_run("test/bool/t_not_true.tan", vec![], 
            Ok(Some(0)), 
            vec![]
        );
    }
    
    #[test]
    fn true_and_false() {
        load_and_run("test/bool/t_true_and_false.tan", vec![], 
            Ok(Some(20)), 
            vec![]
        );
    }
    
    #[test]
    fn true_or_2() {
        load_and_run("test/bool/t_true_or_2.tan", vec![
            SemError::TypeError { 
                expected: "bool".to_string(), 
                actual: "int".to_string(), 
                span: Span { start: 8, end: 9 } 
            }
        ], 
            Ok(Some(1)), 
            vec![]
        );
    }
    
    #[test]
    fn true_or_false() {
        load_and_run("test/bool/t_true_or_false.tan", vec![], 
            Ok(Some(10)), 
            vec![]
        );
    }
    
    #[test]
    fn true_() {
        load_and_run("test/bool/t_true.tan", vec![], 
            Ok(Some(1)), 
            vec![]
        );
    }
}

mod check {
    use crate::test::load_and_run;
    use crate::check::SemError;
    use crate::util::Span;
    use crate::vm::RtError;
    
    #[test]
    fn unbound_var() {
        load_and_run("test/check/f_unbound_var.tan", vec![
            SemError::UnboundVariable { name: "x".to_string(), span: Span { start: 0, end: 1 } }
        ], 
            Err(RtError { message: "Panic: Reference to unbound variable \"x\"".to_string(), span: Span { start: 0, end: 1 } }), 
            vec![]
        );
    }
    
    #[test]
    fn unbound_x_y() {
        load_and_run("test/check/f_unbound_x_y.tan", vec![
            SemError::UnboundVariable { name: "x".to_string(), span: Span { start: 0, end: 1 } },
            SemError::UnboundVariable { name: "y".to_string(), span: Span { start: 4, end: 5 } },
        ], 
            Err(RtError { message: "Panic: Reference to unbound variable \"x\"".to_string(),  span: Span { start: 0, end: 1 } }), 
            vec![]
        );
    }
}

mod do_ {
    use crate::test::load_and_run;
    use crate::check::SemError;
    use crate::util::Span;
    use crate::vm::RtError;
    
    #[test]
    fn multiple() {
        load_and_run("test/do/t_multiple.tan", vec![], 
            Ok(Some(6)),
            vec![],
        );
    }
    
    #[test]
    fn simple() {
        load_and_run("test/do/t_simple.tan", vec![], 
            Ok(Some(3)), 
            vec![]
        );
    }
}

mod err {
    use crate::test::load_and_run;
    use crate::check::SemError;
    use crate::util::Span;
    use crate::vm::RtError;
    
    #[test]
    #[should_panic]
    fn bad_end() {
        load_and_run("test/err/e_bad_end.tan", vec![], 
            Ok(None),
            vec![],
        );
    }
    
    #[test]
    #[should_panic]
    fn end() {
        load_and_run("test/err/e_end.tan", vec![], 
            Ok(None), 
            vec![]
        );
    }
}

mod for_ {
    use crate::test::load_and_run;
    use crate::check::SemError;
    use crate::util::Span;
    use crate::vm::RtError;
    
    #[test]
    fn bad_range() {
        load_and_run("test/for/f_bad_range.tan", vec![
            SemError::TypeError { expected: "int".to_string(), actual: "bool".to_string(), span: Span { start: 12, end: 16 } }
        ], 
            Err(RtError { message: "Panic: Expected type int, got bool".to_string(),  span: Span { start: 12, end: 16 } }),
            vec![],
        );
    }
    
    #[test]
    fn wrong_var_type() {
        load_and_run("test/for/f_wrong_var_type.tan", vec![
            SemError::TypeError { expected: "int".to_string(), actual: "bool".to_string(), span: Span { start: 21, end: 25 } }
        ], 
            Err(RtError { message: "Panic: Expected type int, got bool".to_string(),  span: Span { start: 21, end: 25 } }), 
            vec![]
        );
    }
    
    #[test]
    fn fac10() {
        load_and_run("test/for/t_fac10.tan", vec![], 
            Ok(Some(3628800)), 
            vec![]
        );
    }
    
    #[test]
    fn for_int() {
        load_and_run("test/for/f_for_int.tan", vec![
            SemError::Message { message: "Type int is not a valid iterable".to_string(), span: Span { start: 9, end: 10 } }
        ], 
            Err(RtError { message: "Panic: Type int is not a valid iterable".to_string(),  span: Span { start: 9, end: 10 } }), 
            vec![]
        );
    }
}

mod fun {
    use crate::test::load_and_run;
    use crate::check::SemError;
    use crate::util::Span;
    use crate::vm::RtError;
    use crate::ast::*;
    
    #[test]
    fn bad_ret() {
        load_and_run("test/fun/f_bad_ret.tan", vec![
            SemError::TypeError { expected: "int".to_string(), actual: "bool".to_string(), span: Span { start: 18, end: 22 } }
        ], 
            Ok(None), 
            vec![]
        );
    }
    
    #[test]
    fn call_few_args() {
        load_and_run("test/fun/f_call_few_args.tan", vec![
            SemError::Message { message: "Function \'add\' was given 1 argument, expected 2".to_string(), span: Span { start: 42, end: 45 } }
        ], 
            Err(RtError { message: "Panic: Function \'add\' was given 1 argument, expected 2".to_string(), span: Span { start: 42, end: 45 } }), 
            vec![]
        );
    }
    
    #[test]
    fn call_later() {
        load_and_run("test/fun/f_call_later.tan", vec![
            SemError::Message { message: "Call to unknown function \'foo\'".to_string(), span: Span { start: 0, end: 3 } }
        ], 
            Err(RtError { message: "Panic: Call to unknown function \'foo\'".to_string(), span: Span { start: 0, end: 3 } }), 
            vec![]
        );
    }
    
    #[test]
    fn call_unknown() {
        load_and_run("test/fun/f_call_unknown.tan", vec![
            SemError::Message { message: "Call to unknown function \'hello_world\'".to_string(), span: Span { start: 0, end: 11 } }
        ], 
            Err(RtError { message: "Panic: Call to unknown function \'hello_world\'".to_string(),  span: Span { start: 0, end: 11 } }), 
            vec![]
        );
    }
    
    #[test]
    fn call_wrong_arg() {
        load_and_run("test/fun/f_call_wrong_arg.tan", vec![
            SemError::TypeError { expected: "int".to_string(), actual: "bool".to_string(), span: Span { start: 23, end: 27 } }
        ], 
            Err(RtError { message: "Panic: Expected type int, got bool".to_string(), span: Span { start: 23, end: 27 } }), 
            vec![]
        );
    }
    
    #[test]
    fn call_add() {
        load_and_run("test/fun/t_call_add.tan", vec![
        
        ], 
            Ok(Some(5)), 
            vec![]
        );
    }
    
    #[test]
    fn fn_add() {
        load_and_run("test/fun/t_fn_add.tan", vec![
        
        ], 
            Ok(None), 
            vec![]
        );
    }
    
    #[test]
    fn nested() {
        load_and_run("test/fun/t_nested.tan", vec![
        
        ], 
            Ok(Some(98)), 
            vec![]
        );
    }
    
    #[test]
    fn no_ret() {
        load_and_run("test/fun/f_no_ret.tan", vec![
            SemError::MissingReturnValue { fun: Ident { name: "foo".to_string(), span: Span { start: 3, end: 6 } }, span: Span { start: 0, end: 2 }, expected: Type::Int }
        ], 
            Ok(None), 
            vec![]
        );
    }
    
    #[test]
    fn outer_ref() {
        load_and_run("test/fun/f_outer_ref.tan", vec![
            SemError::UnboundVariable { name: "x".to_string(), span: Span { start: 28, end: 29 } }
        ], 
            Ok(None), 
            vec![]
        );
    }
    
    #[test]
    fn rec() {
        load_and_run("test/fun/t_rec.tan", vec![
        
        ], 
            Ok(Some(3628800)), 
            vec![]
        );
    }
    
    #[test]
    fn set_arg() {
        load_and_run("test/fun/f_set_arg.tan", vec![
            SemError::AssignmentToImmutableVar { name: "x".to_string(), span: Span { start: 17, end: 18 } }
        ], 
            Ok(None), 
            vec![]
        );
    }
    
    #[test]
    fn stmt_ret() {
        load_and_run("test/fun/f_stmt_ret.tan", vec![
            SemError::MissingReturnValue { fun: Ident { name: "foo".to_string(), span: Span { start: 3, end: 6 } }, span: Span { start: 0, end: 2 }, expected: Type::Int }
        ], 
            Ok(None), 
            vec![]
        );
    }
}

mod if_ {
    use crate::test::load_and_run;
    use crate::check::SemError;
    use crate::util::Span;
    use crate::vm::RtError;
    use crate::ast::*;
    
    #[test]
    fn if_2_3() {
        load_and_run("test/if/if_2_3.tan", vec![
        
        ], 
            Ok(Some(2)), 
            vec![]
        );
    }
    
    #[test]
    fn if_2_true() {
        load_and_run("test/if/if_2_true.tan", vec![
            SemError::TypeError { expected: "bool".to_string(), actual: "int".to_string(), span: Span { start: 22, end: 23 } }
        ], 
            Err(RtError { message: "Panic: Expected type bool, got int".to_string(), span: Span { start: 22, end: 23 } }), 
            vec![]
        );
    }
    
    #[test]
    fn if_else() {
        load_and_run("test/if/if_else.tan", vec![
        
        ], 
            Ok(Some(1)), 
            vec![]
        );
    }
    
    #[test]
    fn if_() {
        load_and_run("test/if/if.tan", vec![
        
        ], 
            Ok(None), 
            vec![]
        );
    }
    
    #[test]
    fn let_if_bad() {
        load_and_run("test/if/let_if_bad.tan", vec![
            SemError::TypeError { expected: "int".to_string(), actual: "nil".to_string(), span: Span { start: 24, end: 25 } }
        ], 
            Err(RtError { message: "Panic: Expected type int, got nil".to_string(), span: Span { start: 24, end: 25 } }), 
            vec![]
        );
    }
    
    #[test]
    fn let_if() {
        load_and_run("test/if/let_if.tan", vec![
        
        ], 
            Ok(Some(4)), 
            vec![]
        );
    }
    
    #[test]
    fn nested() {
        load_and_run("test/if/nested.tan", vec![
        
        ], 
            Ok(Some(5)), 
            vec![]
        );
    }
    
    
}
