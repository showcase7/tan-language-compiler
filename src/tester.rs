extern crate tan;

use std::fs::File;
use std::io::Read;
use std::collections::{HashMap, HashSet};
use std::path::Path;


#[allow(unused)]
use tan::{Span, RtError, SemError, Program, ParseError, TokenKind};
use tan::util::{show_source_span, line_info, print_instructions};

//const TEST_DIR: &'static str = "test";



#[derive(Debug)]
enum ExpRes<'a> {
    Panic(&'a str),
    None,
    Some(i64),
}

fn parse_strings(text: &str) -> Vec<&str> {
    let mut strings = Vec::new();
    let mut in_string = false;
    let mut start = 0;
    for (i, ch) in text.char_indices() {
        match (in_string, ch) {
            (true, '"') => {
                in_string = false;
                strings.push(&text[start..i]);
            }
            (false, '"') => {
                in_string = true;
                start = i + 1;
            }
            _ => continue,
        }
    }
    if in_string {
        panic!("Unclosed '\"' in comment string (in text: '{}')", text);
    }
    strings
}

fn get_lineno(source: &str, span: Span) -> usize {
    line_info(span.start, source).0
}

fn parse_test_info<'a>(source: &'a str) -> Result<(Option<ExpRes<'a>>, Vec<(usize, &'a str)>), String> {
    let mut raw_lexer = tan::lex::RawLexer::new(&source);
    let mut expected_result = None;
    let mut lineno = 1;
    let mut expected_errors = Vec::new();
    loop {
        let token = match raw_lexer.next() {
            Ok(token) => token,
            Err(parse_err) => {
                let span = Span::new(parse_err.pos, parse_err.pos);
                show_source_span(&source, span);
                let msg = format!("{}: Parse error: {}", 
                    get_lineno(&source, span), 
                    parse_err.message
                );
                println!("{}", msg);
                println!("");
                return Err(msg);
            }
        };
        match token.kind {
            TokenKind::Comment => {
                let text = (&token.span.slice(&source)[1..]).trim();
                //println!("Got a comment: {:?}", text);
                if text.starts_with("panic: ") {
                    //println!("  Matched: panic");
                    if expected_result.is_some() {
                        show_source_span(&source, token.span);
                        let msg = format!("{}: More than one result specifier comment", 
                            get_lineno(&source, token.span)
                        );
                        println!("{}", msg);
                        println!("");
                        return Err(msg);
                    }
                    let panic_text = parse_strings(text).pop()
                        .expect("No string in panic specifier comment");
                    expected_result = Some(ExpRes::Panic(panic_text));
                
                } else if text.starts_with("error: ") {
                    //println!("  Matched: error");
                    let strings = parse_strings(text);
                    if strings.is_empty() {
                        let msg = format!("{}: No strings found in 'error:' comment!", 
                            get_lineno(&source, token.span)
                        );
                        println!("{}", msg);
                        println!("");
                        return Err(msg);
                    }
                    for error_message in strings {
                        expected_errors.push((lineno, error_message));
                    }
                } else if let Some(text) = text.splitn(2, "result: ").skip(1).next() {
                    //println!("  Matched: result");
                    if expected_result.is_some() {
                        show_source_span(&source, token.span);
                        let msg = format!("{}: More than one result specifier comment", 
                            get_lineno(&source, token.span)
                        );
                        println!("{}", msg);
                        println!("");
                        return Err(msg);
                    }
                    match text {
                        "none" | "None" => {
                            expected_result = Some(ExpRes::None);
                        }
                        _ => {
                            if let Ok(value) = text.parse::<i64>() {
                                expected_result = Some(ExpRes::Some(value));
                            } else {
                                show_source_span(&source, token.span);
                                let msg = format!("{}: Invalid result type '{}'", 
                                    get_lineno(&source, token.span),
                                    text
                                );
                                println!("{}", msg);
                                println!("");
                                return Err(msg);
                            }
                        }
                    }
                }
            }
            TokenKind::Newline => {
                //println!("Got a newline!");
                lineno += 1;
            }
            TokenKind::Eof => break,
            _ => {}
        }
    }
    Ok((expected_result, expected_errors))
}

fn error_line_numbers(errors: &[SemError], source: &str) -> Vec<usize> {
    if errors.is_empty() {
        return Vec::new();
    }
    let mut lineno = 1;
    let mut linenos = vec![0; errors.len()];
    let mut starts = HashMap::new();
    for (i, err) in errors.iter().enumerate() {
        starts.entry(err.span().start).or_insert_with(Vec::new).push(i);
    }
    for (i, ch) in source.char_indices() {
        if let Some(indices) = starts.get(&i) {
            for &index in indices {
                linenos[index] = lineno;
            } 
        }
        if ch == '\n' {
            lineno += 1;
        }
    }
    
    linenos
}

fn load_and_run_test(filepath: &str) -> Result<bool, String> {
    println!("");
    println!("============ Running test: '{}'... ================", filepath);
    println!("");
    
    let mut f = match File::open(filepath) {
        Ok(f) => f,
        Err(_) => {
            let msg = format!("Could not open file");
            return Err(msg);
        }
    };
    let mut source = String::new();
    match f.read_to_string(&mut source) {
        Ok(_) => {}
        Err(_) => {
            let msg = format!("Could not read file");
            return Err(msg);
        }
    }
    
    let (expected_result, expected_errors) = parse_test_info(&source)?;
    
    /*println!("Test info:");
    println!("  Expected result:");
    println!("    {:?}", expected_result);
    println!("");
    println!("  Expected errors:");
    for &(lineno, msg) in &expected_errors {
        println!("    {}: {:?}", lineno, msg);
    }*/

    match std::panic::catch_unwind(|| {
        let ast = match tan::parse(&source) {
            Ok(ast) => ast,
            Err(parse_err) => {
                let span = Span::new(parse_err.pos, parse_err.pos);
                show_source_span(&source, span);
                let msg = format!("Parse error: {}", parse_err.message);
                println!("{}", msg);
                println!("");
                return Err(msg);
            }
        };
        
        let (tast, errors) = match tan::check_ast(&ast) {
            Ok(tast) => (tast, Vec::new()),
            Err((tast, errors)) => (tast, errors),
        };
        
        let mut cg = tan::codegen::CodeGen::new();
        let program = cg.gen(&tast);
        let res = tan::run(program.clone());
        
        let mut passed = true;
        
        // - Wrong result
        let exp_res = expected_result.unwrap_or(ExpRes::None);
        let mut had_wrong_result = false;
        match (exp_res, res) {
            (ExpRes::Panic(exp_msg), Err(rt_err)) => {
                let exp_msg = format!("Panic: {}", exp_msg);
                if rt_err.message.as_str() != exp_msg.as_str() {
                    passed = false;
                    had_wrong_result = true;
                    println!("OUTPUT: Expected panic '{}', got '{}'", exp_msg, rt_err.message);
                    println!("");
                }
            }
            // 999 is the Nil constant.
            (ExpRes::None, Ok(Some(999))) => {}
            (ExpRes::None, Ok(None)) => {}
            (ExpRes::Some(exp_value), Ok(Some(value))) => {
                if value != exp_value {
                    passed = false;
                    had_wrong_result = true;
                    println!("OUTPUT: Expected value '{}' got '{}'", exp_value, value);
                    println!("");
                }
            }
            (exp_res, res) => {
                passed = false;
                had_wrong_result = true;
                let exp_fmt = match exp_res {
                    ExpRes::Panic(exp_msg) => format!("Panic: {}", exp_msg),
                    ExpRes::None => "None".to_string(),
                    ExpRes::Some(value) => value.to_string(),
                };
                let res_fmt = match res {
                    Ok(None) => "None".to_string(),
                    Ok(Some(999)) => "nil (or 999)".to_string(),
                    Ok(Some(value)) => value.to_string(),
                    Err(rt_err) => rt_err.message.clone(),
                };
                println!("OUTPUT: Expected '{}', got '{}'", exp_fmt, res_fmt);
                println!("");
            }
        }
        
        // Check for unexpected semcheck errors
        
        let mut exp_errs = HashMap::new();
        for (i, &(lineno, msg)) in expected_errors.iter().enumerate() {
            exp_errs.entry(lineno).or_insert_with(Vec::new).push((i, msg));
        }
        
        //println!("Exp errs: {:?}", exp_errs);
        
        let mut found_errs = HashSet::new();
        let err_linenos = error_line_numbers(&errors, &source);
        //println!("Errors:       {:#?}", errors);
        //println!("Err_linenos:  {:#?}", err_linenos);
        for (i, err) in errors.iter().enumerate() {
            let lineno = err_linenos[i];
            //println!("Checking found test: Line {}, msg: '{}'", lineno, err.description());
            let mut found = false;
            if let Some(errs) = exp_errs.get(&lineno) {
                for &(err_id, exp_err) in errs {
                    //println!("Comparing '{}' with '{}'", err.description(), exp_err);
                    if err.description().as_str() == exp_err {
                        found_errs.insert(err_id);
                        found = true;
                        break;
                    }
                }
            }
            if ! found {
                passed = false;
                show_source_span(&source, err.span());
                println!("Unexpected error: {}", err.description());
                println!("");
            }
        }
        
        // Check for missing expected semcheck errors
        if found_errs.len() != expected_errors.len() {
            passed = false;
            println!("Missing expected errors:");
            for i in 0..expected_errors.len() {
                if ! found_errs.contains(&i) {
                    let (lineno, msg) = expected_errors[i];
                    println!("  Line {}: {}", lineno, msg);
                }
            }
            println!("");
        }
        
        // Print extra information
        if had_wrong_result {
            println!("Program:");
            print_instructions(cg.instructions(), &source);
            println!("");
            println!("Program trace:");
            let _ = tan::run_tracing(program);
        }
        
        Ok(passed)
    }) {
        Ok(res) => {
            res
        }
        Err(_) => {
            Err(format!("<TEST PANICKED>"))
        }
    }
    
    
}

#[derive(Debug)]
enum TestTree {
    Dir { 
        path: String, 
        name: String, 
        items: Vec<TestTree>,
        test_count: usize,
    },
    Leaf {
        path: String,
        name: String,
    }
}

impl TestTree {
    fn name(&self) -> &String {
        match *self {
            TestTree::Dir { ref name, ..} => name,
            TestTree::Leaf { ref name, ..} => name,
        }
    }
    
    fn test_count(&self) -> usize {
        match *self {
            TestTree::Dir { test_count, .. } => test_count,
            TestTree::Leaf { .. } => 1,
        }
    }
}

fn read_tests(filepath: &str) -> TestTree {
    let path = Path::new(filepath);
    if path.is_dir() {
        let mut items = Vec::new();
        let mut test_count = 0;
        for entry in path.read_dir().expect("Could not read dir") {
            let entry = entry.expect("Could not read dir entry");
            let entry_path = entry.path();
            if let Some(ext) = entry_path.extension() {
                if ext != "tan" {
                    continue;
                }
            } else if entry_path.is_dir() {
                // Don't skip.
            } else {
                continue;
            }
            let item = read_tests(entry_path.to_str().unwrap());
            test_count += item.test_count();
            items.push(item);
        }
        
        items.sort_by(|left, right| {
            left.name().cmp(&right.name())
        });
        let name = path.file_stem().unwrap().to_str().unwrap().to_string();
        TestTree::Dir {
            path: filepath.to_string(),
            name: name,
            items: items,
            test_count: test_count,
        }
        
    } else {
        if ! filepath.ends_with(".tan") {
            panic!("The given filepath is not a .tan file!");
        }
        let name = path.file_stem().unwrap().to_str().unwrap().to_string();
        TestTree::Leaf {
            path: filepath.to_string(),
            name: name,
        }
    }
}

#[derive(Debug)]
enum TestResultTree {
    Dir { 
        path: String, 
        name: String, 
        items: Vec<TestResultTree>,
        pass_count: usize,
        test_count: usize,
    },
    Leaf {
        path: String,
        name: String,
        result: Result<bool, String>,
    }
}

impl TestResultTree {
    fn passed(&self) -> bool {
        match *self {
            TestResultTree::Dir { pass_count, test_count, .. } => {
                pass_count == test_count
            },
            TestResultTree::Leaf { ref result, .. } => {
                match *result {
                    Ok(true) => true,
                    _ => false,
                }
            },
        }
    }
    
    fn pass_count(&self) -> usize {
        match *self {
            TestResultTree::Dir { pass_count, .. } => pass_count,
            TestResultTree::Leaf { ref result, .. } => {
                match *result {
                    Ok(true) => 1,
                    _ => 0,
                }
            }
        }
    }
    
    fn test_count(&self) -> usize {
        match *self {
            TestResultTree::Dir { test_count, .. } => test_count,
            TestResultTree::Leaf { .. } => 1,
        }
    }
}

fn run_tests(tree: &TestTree) -> TestResultTree {
    match *tree {
        TestTree::Dir { ref items, ref path, ref name, test_count } => {
            let mut pass_count = 0;
            let mut run_items = Vec::new();
            for item in items {
                let run_item = run_tests(item);
                pass_count += run_item.pass_count();
                run_items.push(run_item);
            }
            TestResultTree::Dir {
                path: path.clone(),
                name: name.clone(),
                items: run_items,
                pass_count: pass_count,
                test_count: test_count,
            }
        }
        TestTree::Leaf { ref path, ref name } => {
            let res = load_and_run_test(path);
            TestResultTree::Leaf {
                path: path.clone(),
                name: name.clone(),
                result: res,
            }
        }
    }
}

#[allow(unused)]
fn print_passed_tests_inner(tree: &TestResultTree, indent: &mut String) {
    match *tree {
        TestResultTree::Dir { ref items, ref name, pass_count, test_count, .. } => {
            if pass_count == test_count {
                println!("{}{}.*", indent, name);
            
            } else if pass_count > 0 {
                println!("{}{}", indent, name);
                indent.push_str("  ");
                for item in items {
                    print_passed_tests_inner(item, indent);
                }
                indent.pop();
                indent.pop();
            }
        }
        TestResultTree::Leaf { ref name, ref result, .. } => {
            if let &Ok(true) = result {
                println!("{}{}", indent, name);
            }
        }
    }
}

#[allow(unused)]
fn print_passed_tests(tree: &TestResultTree) {
    let mut s = String::new();
    print_passed_tests_inner(tree, &mut s);
}

fn print_failed_tests_inner(tree: &TestResultTree, indent: &mut String) {
    match *tree {
        TestResultTree::Dir { ref items, ref name, pass_count, test_count, .. } => {
            if pass_count != test_count {
                println!("{}{}", indent, name);
                indent.push_str("  ");
                for item in items {
                    print_failed_tests_inner(item, indent);
                }
                indent.pop();
                indent.pop();
            }
        }
        TestResultTree::Leaf { ref name, ref result, .. } => {
            match result {
                &Ok(false) => {
                    println!("{}{}", indent, name);
                }
                &Err(ref msg) => {
                    println!("{}{}: Error: '{}'", indent, name, msg);
                }
                _ => {}
            }
        }
    }
}

fn print_failed_tests(tree: &TestResultTree) {
    let mut s = String::new();
    print_failed_tests_inner(tree, &mut s);
}

fn run_and_print_tests(filepath: &str) {
    let test_tree = read_tests(filepath);
    let result_tree = run_tests(&test_tree);
    
    println!("");
    println!("================== Results ===================");
    println!("");
    
    //println!("Passed tests:");
    //print_passed_tests(&result_tree);
    //println!("");
    
    let all_passed = result_tree.pass_count() == result_tree.test_count();
    if ! all_passed {
        println!("Failed tests:");
        print_failed_tests(&result_tree);
        println!("");
    }
    
    println!("{} / {} tests passed", result_tree.pass_count(), result_tree.test_count());
    if result_tree.passed() {
        println!("TEST SUCCESFUL");
    } else {
        println!("TEST FAILED");
    }
}

fn main() {
    //let args = env::args().skip(1).collect::<Vec<_>>();
    
    run_and_print_tests("test");
}
