
use crate::util::Span;
use std::str::CharIndices;
use std::iter::Peekable;

#[derive(Debug, Clone, Copy)]
pub struct Token {
    pub kind: TokenKind,
    pub span: Span,
}

impl Token {
    fn new(kind: TokenKind, start: usize, end: usize) -> Token {
        Token { 
            kind: kind, 
            span: Span::new(start, end),
        }
    }
    
    pub fn slice<'src>(&self, text: &'src str) -> &'src str {
        &text[self.span.start..self.span.end]
    }
    
    pub fn span(&self) -> Span {
        self.span
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TokenKind {
    Integer,
    Ident,
    
    // Keywords
    Type,
    Of,
    Impl,
    Let,
    Mut,
    Fn,
    If,
    Then,
    Else,
    For,
    In,
    Match,
    Case,
    End,
    Not,
    Return,
    Panic,
    True,
    False,
    While,
    New,
    Where,
    Do,
    Nil,
    Variant,
    
    // Operators/symbols
    Plus,       // +
    Minus,      // -
    Star,       // *
    Slash,      // /
    Percent,    // %
    
    EqEq,       // ==
    NotEq,      // !=
    Lt,         // <
    LtEq,       // <=
    Gt,         // >
    GtEq,       // >=
    
    And,        // && , and
    Or,         // || , or
    
    PlusEq,     // +=
    MinusEq,    // -=
    StarEq,     // *=
    SlashEq,    // /=
    PercentEq,  // %=
    
    ParOpen,    // (
    ParClose,   // )
    BrackOpen,  // [
    BrackClose, // ]
    Colon,      // :
    Ref,        // &
    Arrow,      // ->
    FatArrow,   // =>
    Line,       // |
    Dot,        // .
    DotDot,     // ..
    Comma,      // ,
    Eq,         // =
    
    Newline,
    
    // Filtered
    Comment,
    Whitespace,
    
    // Meta
    Eof,
}

#[derive(Debug, Clone)]
pub struct LexError {
    pub message: String,
    pub pos: usize,
    pub lineno: usize,
    pub col: usize,
    pub line: String,
}

impl LexError {
    fn new(text: &str, pos: usize, message: String) -> LexError {
        let mut lineno = 1;
        let mut line_start = 0;
        for (i, ch) in text.char_indices() {
            if i == pos {
                break;
            }
            if ch == '\n' {
                lineno += 1;
                line_start = i+1;
            }
        }
        let col = (&text[line_start..pos]).chars().count() + 1;
        let line = (&text[line_start..]).lines().next().unwrap().to_string();
        LexError {
            message: message,
            pos: pos,
            lineno: lineno,
            col: col,
            line: line,
        }
    }
}

#[derive(Debug)]
pub struct Lexer<'src> {
    raw: RawLexer<'src>,
}

impl<'src> Lexer<'src> {
    pub fn new(text: &'src str) -> Lexer<'src> {
        let raw = RawLexer::new(text);
        Lexer {
            raw: raw,
        }
    }
    
    pub fn next(&mut self) -> Result<Token, LexError> {
        loop {
            match self.raw.next() {
                Ok(token) => {
                    match token.kind {
                        TokenKind::Whitespace | TokenKind::Comment => {
                            continue;
                        }
                        _ => {
                            return Ok(token);
                        }
                    }
                }
                Err(err) => {
                    return Err(err);
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct RawLexer<'src> {
    text: &'src str,
    chars: Peekable<CharIndices<'src>>,
}

impl<'src> RawLexer<'src> {
    pub fn new(text: &'src str) -> RawLexer<'src> {
        RawLexer {
            text: text,
            chars: text.char_indices().peekable(),
        }
    }
    
    fn next_pos(&mut self) -> usize {
        self.chars.peek().map(|&(i, _)| i).unwrap_or_else(|| self.text.len())
    }
    
    #[inline(always)]
    fn send_1(&mut self, kind: TokenKind, start: usize) -> Token {
        Token::new(kind, start, start+1)
    }
    
    #[inline(always)]
    fn send_2(&mut self, kind: TokenKind, start: usize) -> Token {
        self.chars.next();
        Token::new(kind, start, start+2)
    }
    
    pub fn next(&mut self) -> Result<Token, LexError> {
        use self::TokenKind::*;
        
        let (i, ch) = if let Some((i, ch)) = self.chars.next() {
            (i, ch)
        } else {
            let end = self.text.len();
            return Ok(Token::new(Eof, end, end));
        };
        
        let nch = self.chars.peek().map(|&(_, ch)| ch).unwrap_or(' ');
        let token = match (ch, nch) {
            ('+', '=') => self.send_2(PlusEq, i),
            ('+', _) => self.send_1(Plus, i),
            ('-', '>') => self.send_2(Arrow, i),
            ('-', '=') => self.send_2(MinusEq, i),
            ('-', _) => self.send_1(Minus, i),
            ('*', '=') => self.send_2(StarEq, i),
            ('*', _) => self.send_1(Star, i),
            ('/', '=') => self.send_2(SlashEq, i),
            ('/', _) => self.send_1(Slash, i),
            ('%', '=') => self.send_2(PercentEq, i),
            ('%', _) => self.send_1(Percent, i),
            ('.', '.') => self.send_2(DotDot, i),
            ('.', _) => self.send_1(Dot, i),
            (',', _) => self.send_1(Comma, i),
            ('(', _) => self.send_1(ParOpen, i),
            (')', _) => self.send_1(ParClose, i),
            ('[', _) => self.send_1(BrackOpen, i),
            (']', _) => self.send_1(BrackClose, i),
            (':', _) => self.send_1(Colon, i),
            ('&', '&') => self.send_2(And, i),
            ('&', _) => self.send_1(Ref, i),
            ('|', '|') => self.send_2(Or, i),
            ('|', _) => self.send_1(Line, i),
            ('=', '=') => self.send_2(EqEq, i),
            ('=', '>') => self.send_2(FatArrow, i),
            ('!', '=') => self.send_2(NotEq, i),
            ('=', _) => self.send_2(Eq, i),
            ('<', '=') => self.send_2(LtEq, i),
            ('<', _) => self.send_1(Lt, i),
            ('>', '=') => self.send_2(GtEq, i),
            ('>', _) => self.send_1(Gt, i),
            ('\r', '\n') => self.send_2(Newline, i),
            ('\n', _) => self.send_1(Newline, i),
            ('0'..='9', _) => {
                while let Some(&(_, ch)) = self.chars.peek() {
                    match ch {
                        '0'..='9' => self.chars.next(),
                        _ => break,
                    };
                }
                Token::new(Integer, i, self.next_pos())
            }
            ('#', _) => {
                while let Some(&(_, ch)) = self.chars.peek() {
                    if ch == '\n' {
                        break;
                    } else {
                        let _ = self.chars.next();
                    }
                }
                Token::new(Comment, i, self.next_pos())
            }
            _ => {
                match ch {
                    ch if ch.is_whitespace() => {
                        while let Some(&(_, ch)) = self.chars.peek() {
                            if ch == '\n' {
                                break;
                            } else if ch.is_whitespace() {
                                self.chars.next();
                            } else {
                                break;
                            }
                        }
                        Token::new(Whitespace, i, self.next_pos())
                    }
                    '_' | 'a'..='z' | 'A'..='Z' => {
                        while let Some(&(_, ch)) = self.chars.peek() {
                            match ch {
                                '0'..='9' | '_' | 'a'..='z' | 'A'..='Z' => {
                                    self.chars.next();
                                }
                                _ => break,
                            }
                        }
                        let end = self.next_pos();
                        let text = &self.text[i..end];
                        let kind = match text {
                            "type" => Type,
                            "of" => Of,
                            "variant" => Variant,
                            "impl" => Impl,
                            "let" => Let,
                            "mut" => Mut,
                            "fn" => Fn,
                            "if" => If,
                            "then" => Then,
                            "else" => Else,
                            "for" => For,
                            "in" => In,
                            "match" => Match,
                            "case" => Case,
                            "end" => End,
                            "not" => Not,
                            "return" => Return,
                            "panic" => Panic,
                            "true" => True,
                            "false" => False,
                            "and" => And,
                            "or" => Or,
                            "while" => While,
                            "new" => New,
                            "where" => Where,
                            "do" => Do,
                            "nil" => Nil,
                            _ => Ident,
                        };
                        Token::new(kind, i, end)
                    }
                    _ => {
                        return Err(LexError::new(
                            self.text, 
                            i, 
                            format!("Invalid character: {:?}", ch)
                        ));
                    }
                }
            }
        };
        
        Ok(token)
    }
}


