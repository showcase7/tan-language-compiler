
use crate::instr::Instr;
use crate::codegen::{Program, StructDef};
use crate::util::Span;
use crate::ast::{Builtin, TypeId};

use std::collections::HashMap;
use std::mem;

#[derive(Debug, Clone, PartialEq)]
pub struct RtError {
    pub message: String,
    pub span: Span,
}

#[derive(Debug, Clone)]
enum ObjKind {
    Vec {
        items: Vec<i64>, 
        is_obj_vec: bool,
    },
    Struct {
        fields: Vec<i64>,
        type_id: TypeId,
    },
    Empty,
}

#[derive(Debug, Clone)]
struct HeapObj {
    kind: ObjKind,
    ref_count: usize,
}


#[derive(Debug, Clone)]
pub struct Machine{
    instructions: Vec<Instr>,
    source_spans: Vec<Span>,
    struct_defs: HashMap<TypeId, StructDef>,
    pc: usize,
    stack: Vec<i64>,
    top_level_stack_size: usize,
    bp: i64,
    heap: Vec<HeapObj>,
}

impl Machine{
    pub fn new() -> Machine {
        Machine {
            instructions: Vec::new(),
            source_spans: Vec::new(),
            struct_defs: HashMap::new(),
            pc: 0,
            stack: Vec::new(),
            top_level_stack_size: 0,
            bp: 0,
            heap: Vec::new(),
        }
    }
    
    pub fn load(&mut self, program: Program) {
        self.instructions = program.instructions;
        self.source_spans = program.source_spans;
        self.top_level_stack_size = program.top_level_stack_size;
        self.struct_defs = program.struct_defs;
        self.pc = 0;
    }
    
    #[inline]
    fn advance(&mut self) {
        self.pc += 1;
    }
    
    #[inline]
    fn pop(&mut self) -> Result<i64, RtError> {
        if let Some(value) = self.stack.pop() {
            Ok(value)
        } else {
            self.error("No value to pop")
        }
    }
    
    #[inline]
    fn push(&mut self, value: i64) {
        self.stack.push(value)
    }
    
    #[inline]
    fn jump_by(&mut self, offset: i64) -> Result<(), RtError> {
        let dst = self.pc as i64 + offset;
        self.jump_to(dst)
    }
    
    #[inline]
    fn jump_to(&mut self, dst: i64) -> Result<(), RtError> {
        if dst < 0 || dst > self.instructions.len() as i64 {
            return self.error(format!("Invalid jump destination: {}", dst));
        }
        self.pc = dst as usize;
        Ok(())
    }
    
    fn error<T, S: Into<String>>(&self, message: S) -> Result<T, RtError> {
        Err(RtError {
            message: message.into(),
            span: self.source_spans[self.pc],
        })
    }
    
    fn alloc(&mut self, kind: ObjKind) -> i64 {
        let obj = HeapObj {
            kind: kind,
            ref_count: 1,
        };
        let ptr = self.heap.len() as i64;
        self.heap.push(obj);
        ptr
    }
    
    fn validate_obj(&mut self, ptr: i64) -> Result<usize, RtError> {
        if ptr < 0 {
            self.error(format!("Invalid pointer (value < 0: {})", ptr))
        } else {
            let index = ptr as usize;
            if index >= self.heap.len() {
                self.error(format!("Invalid heap index: {} >= {}", index, self.heap.len()))
            } else {
                Ok(index)
            }
        }
    }
    
    fn increase_ref_count(&mut self, index: usize) {
        self.heap[index].ref_count += 1;
    }
    
    fn decrease_ref_count(&mut self, index: usize) {
        self.heap[index].ref_count -= 1;
        if self.heap[index].ref_count == 0 {
            self.dealloc(index);
        }
    }
    
    fn dealloc(&mut self, index: usize) {
        let mut obj = HeapObj { kind: ObjKind::Empty, ref_count: 0 };
        mem::swap(&mut obj, &mut self.heap[index]);
        match obj.kind {
            ObjKind::Vec { items, is_obj_vec } => {
                if is_obj_vec {
                    for item in items {
                        self.decrease_ref_count(item as usize);
                    }
                }
            }
            ObjKind::Empty => unreachable!(),
            ObjKind::Struct { type_id, .. } => {
                let should_dealloc = self.struct_defs[&type_id]
                    .field_is_obj
                    .iter()
                    .map(|&o| o)
                    .enumerate()
                    .collect::<Vec<_>>();
                for (i, is_obj) in should_dealloc {
                    if is_obj {
                        self.decrease_ref_count(i);
                    }
                }
            }
        }
    }
    
    
    fn builtin_vec_push(&mut self) -> Result<(), RtError> {
        let item = self.pop()?;
        let vec_ptr = self.pop()?;
        let vec_index = self.validate_obj(vec_ptr)?;
        if let ObjKind::Vec { ref mut items, .. } = self.heap[vec_index].kind {
            items.push(item);
        } else {
            return self.error(format!("No vec found at heap index {}!", vec_index));
        }
        // Decrease the ref count for the vector
        self.decrease_ref_count(vec_index);
        
        // Push a nil value to be a valid expr
        self.push(0);
        Ok(())
    }
    
    fn builtin_vec_pop(&mut self) -> Result<(), RtError> {
        let vec_ptr = self.pop()?;
        let vec_index = self.validate_obj(vec_ptr)?;
        if let ObjKind::Vec { ref mut items, is_obj_vec } = self.heap[vec_index].kind {
            if let Some(value) = items.pop() {
                // Reduce the ref count for the item if necessary
                if is_obj_vec {
                    let item_index = self.validate_obj(value)?;
                    self.decrease_ref_count(item_index);
                }
                self.push(value);
            } else {
                return self.error("Pop called on empty vector");
            }
        } else {
            return self.error(format!("No vec found at heap index {}!", vec_index));
        }
        
        // Decrease the ref count for the vector
        self.decrease_ref_count(vec_index);
        Ok(())
    }
    
    fn builtin_vec_len(&mut self) -> Result<(), RtError> {
        let vec_ptr = self.pop()?;
        let vec_index = self.validate_obj(vec_ptr)?;
        if let ObjKind::Vec { ref items, .. } = self.heap[vec_index].kind {
            let value = items.len() as i64;
            self.push(value);
        } else {
            return self.error(format!("No vec found at heap index {}!", vec_index));
        }
        
        // Decrease the ref count for the vector
        self.decrease_ref_count(vec_index);
        Ok(())
    }
    
    fn builtin_get_index(&mut self) -> Result<(), RtError> {
        let index = self.pop()? as usize;
        let vec_ptr = self.pop()?;
        let vec_index = self.validate_obj(vec_ptr)?;
        if let ObjKind::Vec { ref items, .. } = self.heap[vec_index].kind {
            if index >= items.len() {
                return self.error(format!("Attempted to get index {} of Vec with len {}", index, items.len()));
            }
            let value = items[index];
            self.push(value);
        } else {
            return self.error(format!("No vec found at heap index {}!", vec_index));
        }

        Ok(())
    }
    
    fn builtin_set_index(&mut self) -> Result<(), RtError> {
        let value = self.pop()?;
        let index = self.pop()? as usize;
        let vec_ptr = self.pop()?;
        let vec_index = self.validate_obj(vec_ptr)?;
        if let ObjKind::Vec { ref mut items, .. } = self.heap[vec_index].kind {
            if index >= items.len() {
                let msg = format!("Attempted to set index {} of Vec with len {}", index, items.len());
                return self.error(msg);
            }
            items[index] = value;
        } else {
            return self.error(format!("No vec found at heap index {}!", vec_index));
        }

        Ok(())
    }
    
    fn builtin_new_vec(&mut self, is_obj_vec: bool) -> Result<(), RtError> {
        let ptr = self.alloc(ObjKind::Vec {
            items: Vec::new(),
            is_obj_vec: is_obj_vec,
        });
        self.push(ptr);
        Ok(())
    }
    
    fn execute(&mut self, instr: Instr) -> Result<(), RtError> {
        use crate::instr::Instr::*;
        
        #[allow(unreachable_patterns)]
        match instr {
            PushI(val) => {
                self.push(val);
            },
            Alloc(count) => {
                for _ in 0..count {
                    self.push(0);
                }
            }
            AllocStruct(type_id, field_count) => {
                let ptr = self.alloc(ObjKind::Struct {
                    fields: vec![0; field_count],
                    type_id: type_id
                });
                self.push(ptr);
            }
            RefIncr => {
                let ptr = self.pop()?;
                let index = self.validate_obj(ptr)?;
                self.increase_ref_count(index);
            }
            RefDecr => {
                let ptr = self.pop()?;
                let index = self.validate_obj(ptr)?;
                self.decrease_ref_count(index);
            }
            Dealloc(count) => {
                for _ in 0..count {
                    self.pop()?;
                }
            }
            Eq => {
                let right = self.pop()?;
                let left = self.pop()?;
                let res = if left == right { 1 } else { 0 };
                self.push(res);
            }
            Add | Sub | Mul | Div | Mod | Lt | Gt => {
                let right = self.pop()?;
                let left = self.pop()?;
                
                match instr {
                    Add => self.push(left + right),
                    Sub => self.push(left - right),
                    Mul => self.push(left * right),
                    Div => self.push(left / right),
                    Mod => self.push(left % right),
                    Lt => self.push(if left < right { 1 } else { 0 }),
                    Gt => self.push(if left > right { 1 } else { 0 }),
                    _ => unimplemented!(),
                }
            },
            Pop => {
                self.pop()?;
            }
            Swap => {
                let top = self.pop()?;
                let next = self.pop()?;
                self.push(top);
                self.push(next);
            }
            MoveDown(n) => {
                let mut i = self.stack.len() - 1;
                for _ in 0..n {
                    self.stack.swap(i, i-1);
                    i -= 1;
                }
            }
            Dup(n) => {
                if self.stack.len() < n {
                    return self.error(format!("Could not dup {} items on stack", n));
                }
                let start = self.stack.len() - n;
                let end = self.stack.len();
                for i in start..end {
                    let value = self.stack[i];
                    self.stack.push(value);
                }
            }
            Panic(ref msg) => {
                return self.error(format!("Panic: {}", msg));
            }
            Sti(n) => {
                if n == 1 {
                    let value = self.pop()?;
                    let index = self.pop()? as usize;
                    if index < self.stack.len() {
                        self.stack[index] = value;
                    } else {
                        return self.error(format!("Invalid Sti index: {}", index));
                    }
                } else if n == 2 {
                    //println!("Running STI(2)");
                    let value2 = self.pop()?;
                    let value1 = self.pop()?;
                    let index = self.pop()? as usize;
                    let start = index;
                    let end = start + 1;
                    if start < self.stack.len() && end < self.stack.len() {
                        self.stack[index] = value1;
                        self.stack[index + 1] = value2;
                    } else {
                        return self.error(format!("Invalid Sti({}) index: {} (stack size: {})", n, index, self.stack.len()));
                    }
                } else {
                    let mut values = Vec::new();
                    for _ in 0..n {
                        values.push(self.pop()?);
                    }
                    let index = self.pop()? as usize;
                    if index >= self.stack.len() {
                        return self.error(format!("Invalid Sti index: {}", index));
                    }
                    let mut i = index;
                    while let Some(value) = values.pop() {
                        self.stack[i] = value;
                        i += 1;
                    }
                }
                
            }
            Ldi(n) => {
                let index = self.pop()? as usize;
                if n == 1 {
                    if index < self.stack.len() {
                        let value = self.stack[index].clone();
                        self.stack.push(value);
                    } else {
                        return self.error(format!("Invalid Ldi index: {}", index));
                    }
                } else if n == 2 {
                    let end = index + 1;
                    if end < self.stack.len() {
                        let value0 = self.stack[index].clone();
                        let value1 = self.stack[index+1].clone();
                        self.stack.push(value0);
                        self.stack.push(value1);
                    } else {
                        return self.error(format!("Invalid Ldi index: {}", index));
                    }
                } else {
                    unimplemented!();
                }
            }
            FieldSet(index) => {
                let mut value = self.pop()?;
                let ptr = self.pop()? as usize;
                if let ObjKind::Struct { ref mut fields, .. } = self.heap[ptr].kind {
                    mem::swap(&mut value, &mut fields[index]);
                    self.push(value);
                } else {
                    return self.error(format!("The value at heap {} was not a struct: {:?}", ptr, self.heap[ptr]));
                }
            }
            FieldGet(index) => {
                let ptr = self.pop()? as usize;
                if let ObjKind::Struct { ref fields, .. } = self.heap[ptr].kind {
                    let value = fields[index];
                    self.push(value);
                } else {
                    return self.error(format!("The value at heap {} was not a struct: {:?}", ptr, self.heap[ptr]));
                }
            }
            Not => {
               let value = self.pop()?;
               let res = if value == 0 { 1 } else { 0 };
               self.push(res);
            }
            Neg => {
                let value = self.pop()?;
                self.push(-value);
            }
            Bnz(offset) => {
                let value = self.pop()?;
                if value != 0 {
                    return self.jump_by(offset);
                }
            }
            Bez(offset) => {
                let value = self.pop()?;
                if value == 0 {
                    return self.jump_by(offset);
                }
            }
            Jmp(offset) => {
                return self.jump_by(offset);
            }
            Call(offset, arg_size) => {
                let start = self.stack.len() - arg_size;
                self.push(0);
                self.push(0);
                //println!("Stack: {:?}", self.stack);
                let end = self.stack.len() - 1;
                //println!("End: {}", end);
                for i in 0..arg_size {
                    let dst = end - i;
                    let src = dst - 2;
                    //println!("Moving arg from {} to {}", src, dst);
                    self.stack[dst] = self.stack[src];
                }
                self.stack[start] = (self.pc + 1) as i64;
                self.stack[start+1] = self.bp;
                self.bp = start as i64 + 2;
                return self.jump_by(offset);
            }
            CallBuiltin(builtin) => {
                match builtin {
                    Builtin::NewVec(is_obj_vec) => self.builtin_new_vec(is_obj_vec)?,
                    Builtin::VecPush => self.builtin_vec_push()?,
                    Builtin::VecPop => self.builtin_vec_pop()?,
                    Builtin::VecLen => self.builtin_vec_len()?,
                    Builtin::GetIndex => self.builtin_get_index()?,
                    Builtin::SetIndex => self.builtin_set_index()?,
                    _ => unimplemented!(),
                }
            }
            Ret(n) => {
                let mut ret_vals = Vec::new();
                for _ in 0..n {
                    ret_vals.push(self.pop()?);
                }
                let bp = self.pop()?;
                let pc = self.pop()?;
                for _ in 0..n {
                    self.push(ret_vals.pop().unwrap());
                }
                self.bp = bp;
                //println!("Stack after ret: {:?}", self.stack);
                return self.jump_to(pc);
            }
            GetBasePtr => {
                self.push(self.bp);
            }
            _ => {
                unimplemented!("Instr: {:?}", instr);
            }
        }
        
        self.advance();
        
        Ok(())
    }
    
    pub fn run(&mut self) -> Result<Option<i64>, RtError> {
        while self.pc < self.instructions.len() {
            let instr = self.instructions[self.pc].clone();
            self.execute(instr)?;
        }
        
        if self.stack.len() > self.top_level_stack_size {
            Ok(self.stack.pop())
        } else {
            Ok(None)
        }
    }
    
    fn fmt_heap(&self) -> String {
        let mut s = String::new();
        s.push_str("(");
        let last = if ! self.heap.is_empty() { self.heap.len() - 1 } else { 0 };
        for (i, obj) in self.heap.iter().enumerate() {
            s.push_str(&obj.ref_count.to_string());
            if i != last {
                s.push_str(", ");
            }
        }
        s.push_str(")");
        s
    }
    
    #[allow(unused)]
    pub fn heap_rc_count(&self) -> Vec<usize> {
        self.heap.iter().map(|obj| obj.ref_count).collect()
    }
    
    pub fn run_tracing(&mut self) -> Result<Option<i64>, RtError> {
        println!("Tracing...");
        while self.pc < self.instructions.len() {
            println!("  {:03}: {:?} - {}", self.pc, self.stack, self.fmt_heap());
            println!("    {:?}", self.instructions[self.pc]);
            self.execute(self.instructions[self.pc].clone())?;
        }
        println!("  {:03}: {:?} - {}", self.pc, self.stack, self.fmt_heap());
        println!("");
        println!("Final Heap:");
        for (i, obj) in self.heap.iter().enumerate() {
            println!("{:3}: {:?}", i, obj);
        }
        
        //println!("Top level stack size: {}", self.top_level_stack_size);
        if self.stack.len() > self.top_level_stack_size {
            Ok(self.stack.pop())
        } else {
            Ok(None)
        }
    }
}

/// Runs the given program.
pub fn run(program: Program) -> Result<Option<i64>, RtError> {
    let mut machine = Machine::new();
    machine.load(program);
    machine.run()
}

/// Runs the given program, writing a lot of intermediate information in order
/// to closely monitor the execution of the program.
pub fn run_tracing(program: Program) -> (Result<Option<i64>, RtError>, Vec<usize>) {
    let mut machine = Machine::new();
    machine.load(program);
    let res = machine.run_tracing();
    let heap_rc = machine.heap_rc_count();
    (res, heap_rc)
}


