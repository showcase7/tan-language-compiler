pub mod util;
pub mod ast;
pub mod lex;
pub mod parse;
pub mod check;
pub mod codegen;
pub mod instr;
pub mod vm;

pub use crate::lex::{Lexer, Token, TokenKind, LexError};
pub use crate::util::Span;
pub use crate::parse::{ParseError, parse};
pub use crate::check::{SemError, check_ast};
pub use crate::instr::Instr;
pub use crate::codegen::{Program, generate_code};
pub use crate::vm::{RtError, run, run_tracing};

