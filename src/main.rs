extern crate tan;

use std::env;
use std::fs::File;
use std::io::Read;
use std::error::Error;
use std::iter;

use tan::{Lexer, TokenKind};
use tan::util::{show_source_span, line_info, print_instructions};

fn print_usage() {
    eprintln!("Usage: rutan <debug | --help> [args...]");
}

fn try_read_file(filename: &str) -> Result<String, String> {
    let mut f = match File::open(filename) {
        Ok(f) => f,
        Err(err) => {
            return Err(format!("Could not open file '{}': {}", filename, err.description()));
        }
    };
    let mut s = String::new();
    match f.read_to_string(&mut s) {
        Ok(_) => {}
        Err(err) => {
            return Err(format!("Could not read file '{}': {}", filename, err.description()));
        }
    };
    Ok(s)
}

fn print_tokens(source_text: &str) {
    let mut lexer = Lexer::new(source_text);
    
    println!("Tokens:");
    loop {
        let token = lexer.next();
        match token {
            Ok(token) => {
                let text = token.span.slice(source_text);
                println!("  {:?} {:?}", text, token.kind);
                if let TokenKind::Eof = token.kind {
                    break;
                }
            }
            Err(err) => {
                println!("Lexing error:");
                println!("{}:{}: {}", err.lineno, err.col, err.message);
                println!("{}", err.line);
                let line = std::iter::repeat('~').take(err.col - 1).collect::<String>();
                println!("{}^", line);
            }
        }
    }
}

fn main() -> Result<(), String> {
    
    let args = env::args().skip(1).collect::<Vec<String>>();
    if args.len() == 0 {
        print_usage();
        return Err("Invalid usage".to_string());
    }
    match args[0].as_str() {
        "lex" => {
            if args.len() != 2 {
                eprintln!("Usage: rutan lex <file>");
                return Err("Invalid usage".to_string());
            }
            let filename = &args[1];
            
            let text = try_read_file(&filename)?;
            print_tokens(&text);
            Ok(())
        }
        "debug" => {
            if args.len() != 2 {
                eprintln!("Usage: rutan debug <file>");
                return Err("Invalid usage".to_string());
            }
            let filename = &args[1];
            
            let text = try_read_file(&filename)?;
            
            print_tokens(&text);
    
            println!("");
            println!("Parsing...");
            let ast = match tan::parse(&text) {
                Ok(ast) => {
                    println!("Parsed AST:");
                    println!("{:#?}", ast);
                    ast
                }
                Err(err) => {
                    println!("Parse error:");
                    println!("{}:{}: {}", err.lineno, err.col, err.message);
                    println!("{}", err.line);
                    let line = std::iter::repeat('~').take(err.col - 1).collect::<String>();
                    println!("{}^", line);
                    return Err("Invalid program".to_string());
                }
            };
    
            println!("");
            println!("Checking semantics, binding...");
            let ck_ast = match tan::check_ast(&ast) {
                Ok(ast) => {
                    println!("  The AST is valid!");
                    ast
                }
                Err((ast, errors)) => {
                    println!("Semantic errors:");
                    println!("");
                    for err in errors {
                        show_source_span(&text, err.span());
                        println!("");
                        let (lineno, col, _) = line_info(err.span().start, &text);
                        println!("{}:{}: {}", lineno, col, err.description());
                        println!("");
                    }
                    ast
                }
            };
            
            println!("  AST: {:#?}", ck_ast);
            
            println!("");
            println!("Generating program...");
            let mut cg = tan::codegen::CodeGen::new();
            let program = cg.gen(&ck_ast);
    
            println!("Program:");
            print_instructions(cg.instructions(), &text);
            
            println!("");
            println!("Running program...");
            match tan::run_tracing(program) {
                (Ok(Some(val)), _) => {
                    println!("");
                    println!("=> {:?}", val);
                }
                (Ok(None), _) => {
                    println!("");
                    println!("=> ()");
                }
                (Err(error), _) => {
                    println!("Runtime error:");
                    let (lineno, col, line) = tan::util::line_info(error.span.start, &text);
                    
                    if lineno > 1 {
                        let prev = text.lines().skip(lineno - 2).next().unwrap();
                        println!("{:03}: {}", lineno - 1, prev);
                    }
                    
                    println!("{:03}: {}", lineno, line);
                    let len = error.span.end - error.span.start;
                    let pad = iter::repeat(' ').take(col-1).collect::<String>();
                    let mark = if len > 1 {
                        iter::repeat('~').take(len - 1).collect::<String>()
                    } else {
                        String::new()
                    };
                    println!("     {}^{}", pad, mark);
                    
                    if let Some(line) = text.lines().skip(lineno).next() {
                        println!("{:03}: {}", lineno+1, line);
                    }
                    
                    println!("");
                    println!("{}:{}: {}", lineno, col, error.message);
                }
            }
            Ok(())
        }
        "--help" => {
            print_usage();
            Ok(())
        }
        _ => {
            print_usage();
            return Err("Invalid usage".to_string());
        }
    }
}
