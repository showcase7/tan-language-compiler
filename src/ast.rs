

use crate::util::Span;
use std::mem;
use std::borrow::Cow;

// ============================== Untyped AST ===================================

pub type Ast = Vec<Expr>;

#[derive(Debug, Clone)]
pub enum Expr {
    Integer(i64, Span),
    Bool(bool, Span),
    Binary(Box<Expr>, BinOp, Box<Expr>, Span),
    Access(Loc, Span),
    Panic(String, Span),
    Unary(UnOp, Box<Expr>, Span),
    If(Box<Expr>, Vec<Expr>, Vec<Expr>, Span),
    Call(Box<Expr>, Vec<Expr>, Span),
    Constructor(Type, Vec<(Ident, Expr)>, Span),
    Do(Vec<Expr>, Span),
    Nil(Span),
    Let { is_mut: bool, var: Ident, value: Box<Expr>, span: Span },
    Assign(Loc, AssOp, Box<Expr>, Span),
    While(Box<Expr>, Vec<Expr>, Span),
    FunDec {
        name: Ident, 
        args: Vec<(Ident, Type)>, 
        ret: Option<Type>, 
        body: Vec<Expr>, 
        span: Span,
    },
    For(Ident, Iterable, Vec<Expr>, Span),
    TypeDec {
        name: Ident, 
        fields: Vec<(Ident, Type)>, 
        variants: Vec<(Ident, Vec<(Ident, Type)>, Span)>, 
        span: Span,
    },
}

impl Expr {
    pub fn span(&self) -> Span {
        use self::Expr::*;
        match *self {
            Integer(_, span) => span,
            Bool(_, span) => span,
            Binary(_, _, _, span) => span,
            Access(_, span) => span,
            Panic(_, span) => span,
            Unary(_, _, span) => span,
            If(_, _, _, span) => span,
            Call(_, _, span) => span,
            Constructor(_, _, span) => span,
            Do(_, span) => span,
            Nil(span) => span,
            Let { span, .. } => span,
            Assign(_, _, _, span) => span,
            While(_, _, span) => span,
            FunDec { span, .. } => span,
            For(_, _, _, span) => span,
            TypeDec { span, .. } => span,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum BinOp {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    
    Eq,
    NotEq,
    Lt,
    LtEq,
    Gt,
    GtEq,
    
    And,
    Or,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum AssOp {
    Normal,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
}

impl AssOp {
    pub fn fmt(&self) -> &'static str {
        use self::AssOp::*;
        match *self {
            Normal => "=",
            Add => "+=",
            Sub => "-=",
            Mul => "*=",
            Div => "/=",
            Mod => "%=",
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum UnOp {
    Neg,
    Not,
}

#[derive(Debug, Clone)]
pub struct Iterable {
    pub kind: IterableKind,
    pub span: Span,
}

#[derive(Debug, Clone)]
pub enum IterableKind {
    Range(Box<Expr>, Box<Expr>),
    Expr(Box<Expr>),
}

#[derive(Debug, Clone)]
pub struct Loc {
    pub kind: LocKind,
    pub span: Span,
}

impl Loc {
    pub fn span(&self) -> Span {
        self.span
    }
}

#[derive(Debug, Clone)]
pub enum LocKind {
    Var(Ident),
    Indexing(Box<Expr>, Box<Expr>),
    Field(Box<Expr>, Ident),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Ident {
    pub text: String,
    pub span: Span,
}

// ============================== Typed AST ===================================

#[derive(Debug)]
pub struct TAst {
    pub exprs: Vec<TExpr>,
    pub types: Vec<Type>,
}

pub type IdentId = usize;

/// Identifies what union member is actually present
pub type TypeId = i64;

pub const TYPE_NIL: TypeId = 0;
pub const TYPE_INT: TypeId = 1;
pub const TYPE_BOOL: TypeId = 2;
pub const TYPE_PANIC: TypeId = 3;

pub fn const_type_order() -> Vec<Type> {
    vec![
        Type::Nil,
        Type::Int,
        Type::Bool,
        Type::Panic,
    ]
}

#[derive(Debug, Clone, PartialEq)]
pub struct Union {
    pub types: Vec<TypeId>,
}

impl Union {
    pub fn new() -> Union {
        Union {
            types: Vec::new(),
        }
    }
    
    pub fn first(&self) -> TypeId {
        self.types[0]
    }
    
    pub fn contains(&self, typ: TypeId) -> bool {
        self.types.contains(&typ)
    }
    
    pub fn contains_union(&self, union: &Union) -> bool {
        for typ in &union.types {
            if ! self.types.contains(typ) {
                return false;
            }
        }
        true
    }
    
    pub fn add(&mut self, typ: TypeId) {
        if ! self.types.contains(&typ) {
            self.types.push(typ);
        }
    }
    
    pub fn add_union(&mut self, union: Union) {
        for utyp in union.types {
            self.add(utyp);
        }
    }
    
    pub fn fmt<F: FnMut(TypeId) -> String>(&self, fmt_typ: &mut F) -> String {

        let mut s = String::new();
        let last = if self.types.len() == 0 {
            0
        } else {
            self.types.len() - 1
        };
        s.push('<');
        for (i, &typ) in self.types.iter().enumerate() {
            //s.push_str(&typ.fmt());
            s.push_str(&fmt_typ(typ));
            if i != last {
                s.push_str(" or ");
            }
        }
        s.push('>');
        s
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Type {
    Nil,
    Int,
    Bool,
    /// A 'lower' part of this expression always panics.
    Panic,
    Named {
        name: Ident, 
        generics: Vec<Type>, 
        span: Span,
    },
    // An invalid type, known by that name, ie: 'Foo' if Foo isn't defined, 
    // Vec[Bar] if 'Bar' isn't defined, or Vec[A, B] (wrong generics).
    Invalid {
        name: String,
    },
    Bound {
        id: IdentId,
        name: String,
        parent: Option<IdentId>,
    },
    //Fun(Ident, Vec<Type>, Option<Box<Type>>),
    Vec(Box<Type>),
    Union(Union),
}

impl Type {
    pub fn is_panic(&self) -> bool {
        match *self {
            Type::Panic => true,
            _ => false,
        }
    }
    
    pub fn is_obj(&self) -> bool {
        use self::Type::*;
        match *self {
            Int => false,
            Bool => false,
            Panic => false,
            Nil => false,
            Named { .. } => panic!("is_obj called on Type::Named"),
            Bound { .. } => true,
            Invalid { .. } => false, // It cannot be instantiated
            Vec(_) => true,
            Union(ref union) => union.types.iter().any(|&id| id < 0),
        }
    }
    
    pub fn is_nil(&self) -> bool {
        if let Type::Nil = *self {
            true
        } else {
            false
        }
    }
    
    pub fn is_union(&self) -> bool {
        match *self {
            Type::Union(_) => true,
            _ => false,
        }
    }
    
    pub fn fmt<F: FnMut(TypeId) -> String>(&self, fmt_typ: &mut F) -> String {
        //eprintln!("fmt()");
        use self::Type::*;
        match *self {
            Int => "int".to_string(),
            Bool => "bool".to_string(),
            Panic => "panic".to_string(),
            Nil => "nil".to_string(),
            Named { ref name, ref generics, .. } => {
                let mut s = name.text.clone();
                if ! generics.is_empty() {
                    s.push('[');
                    let last = generics.len() - 1;
                    for (i, gen) in generics.iter().enumerate() {
                        s.push_str(&gen.fmt(fmt_typ));
                        if i != last {
                            s.push(',');
                        }
                    }
                    s.push(']');
                }
                s
            }
            Vec(ref item) => {
                let mut s = String::new();
                s.push_str("Vec[");
                s.push_str(&item.fmt(fmt_typ));
                s.push_str("]");
                s
            }
            Union(ref union) => {
                union.fmt(fmt_typ)
            }
            Bound { ref name, .. } => {
                name.clone()
            }
            Invalid { ref name } => {
                name.clone()
            }
            /*Fun(_, ref args, ref ret) => {
                let mut s = String::new();
                s.push_str("function(");
                let last = if args.is_empty() { 0 } else { args.len() - 1 };
                for (i, arg) in args.iter().enumerate() {
                    s.push_str(&arg.fmt());
                    if i != last {
                        s.push_str(", ");
                    }
                }
                s.push(')');
                if let &Some(ref ret_type) = ret {
                    s.push_str(" -> ");
                    s.push_str(&ret_type.fmt());
                }
                s
            }*/
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum PanicLoc {
    // The expression is unsound and cannot be computed.
    Before,
    // The expression is fine, but invalid in this context.
    After,
}


#[derive(Debug, Clone)]
pub enum TExpr {
    Integer {
        value: i64, 
        span: Span, 
        typ: TypeId,
    },
    Bool {
        value: bool, 
        span: Span, 
        typ: TypeId,
    },
    Binary {
        left: Box<TExpr>, 
        op: BinOp, 
        right: Box<TExpr>, 
        typ: TypeId, 
        span: Span,
    },
    Access {
        loc: TLoc, 
        typ: TypeId, 
        span: Span,
    },
    Unary {
        op: UnOp, 
        value: Box<TExpr>, 
        typ: TypeId, 
        span: Span,
    },
    // The TypeIds are for 'if' and 'else', marking whether the results of the
    // branches should be union-tagged with that type before being returned.
    If {
        cond: Box<TExpr>, 
        if_body: Vec<TExpr>, 
        if_tag: Option<TypeId>, 
        else_body: Vec<TExpr>, 
        else_tag: Option<TypeId>, 
        typ: TypeId, 
        span: Span,
    },
    // fun ident, ret_type, args
    Call {
        name: IdentId, 
        args: Vec<TExpr>, 
        typ: TypeId, 
        span: Span
    },
    CallBuiltin {
        builtin: Builtin, 
        args: Vec<TExpr>, 
        typ: TypeId, 
        span: Span,
    },
    Constructor {
        assignments: Vec<(usize, TExpr)>, 
        typ: TypeId,
        span: Span,
    },
    Do {
        body: Vec<TExpr>, 
        typ: TypeId, 
        span: Span
    },
    Panic {
        inner: Box<TExpr>, 
        message: String, 
        loc: PanicLoc, 
        typ: TypeId, 
        span: Span
    },
    // Update the location with the given value
    Reassign {
        loc: TLoc, 
        value: Box<TExpr>, 
        typ: TypeId, 
        span: Span
    },
    // Tag the given expression.
    Union {
        tag: TypeId, 
        value: Box<TExpr>, 
        typ: TypeId, 
        span: Span
    },
    UnwrapUnion {
        value: Box<TExpr>, 
        expected: (TypeId, String), 
        // The other cases contain (tag, type-name, panic-msg)
        other_cases: Vec<(TypeId, String, String)>,
        typ: TypeId,
        span: Span,
    },
    Nil {
        typ: TypeId, 
        span: Span,
    },
    Let { 
        is_mut: bool, 
        var: TIdent, 
        store_typ: Option<TypeId>, 
        value: Box<TExpr>,
        typ: TypeId,
        span: Span 
    },
    Assign { 
        acc: Box<TExpr>, 
        op: AssOp, 
        value: Box<TExpr>,
        typ: TypeId,
        span: Span
    },
    While {
        cond: Box<TExpr>, 
        body: Vec<TExpr>, 
        typ: TypeId,
        span: Span,
    },
    FunDec {
        name: TIdent, 
        args: Vec<(TIdent, TypeId)>, 
        ret: Option<(TypeId, IdentId)>, 
        body: Vec<TExpr>, 
        typ: TypeId,
        span: Span,
    },
    For {
        ident: TIdent, 
        iterable: TIterable, 
        body: Vec<TExpr>, 
        typ: TypeId,
        span: Span,
    },
    /*TypeDec {
        name: Ident, 
        type_id: TypeId, 
        fields: Vec<(Ident, Type)>, 
        variants: Vec<(Ident, TypeId, Vec<(Ident, Type)>, Span)>, 
        typ: TypeId,
        span: Span,
    },*/
    TagVar {
        ident: TIdent, 
        typ: TypeId, 
        span: Span,
    },
}

impl TExpr {
    pub fn empty() -> TExpr {
        TExpr::Nil {
            typ: TYPE_NIL,
            span: Span::new(0, 0),
        }
    }
    
    pub fn access(loc: TLoc) -> TExpr {
        let typ = loc.typ;
        let span = loc.span;
        TExpr::Access {
            loc: loc, 
            typ: typ, 
            span: span,
        }
    }
    
    pub fn new_do(exprs: Vec<TExpr>) -> TExpr {
        let span = exprs.first().and_then(|f| {
            exprs.last().map(|l| {
                f.span().until(l.span())
            }).or_else(|| Some(f.span()))
        }).unwrap_or_else(|| Span::new(0, 0));
        let typ = exprs.last().map(|e| {
               e.typ().clone()
        }).unwrap_or_else(|| TYPE_NIL);
        TExpr::Do {
            body: exprs, 
            typ, 
            span,
        }
    }
    
    pub fn panic(message: String, span: Span) -> TExpr {
        TExpr::Panic {
            inner: Box::new(TExpr::empty()), 
            message, 
            loc: PanicLoc::Before, 
            typ: TYPE_PANIC,
            span,
        }
    }
    
    pub fn wrap_in_panic(&mut self, msg: String, span: Span, loc: PanicLoc) {
        let inner = mem::replace(self, TExpr::empty());
        *self = TExpr::Panic {
            inner: Box::new(inner), 
            message: msg, 
            loc, 
            span,
            typ: TYPE_PANIC,
        };
    }
    
    pub fn loc(&self) -> Option<&TLoc> {
        match *self {
            TExpr::Access { ref loc, .. } => Some(loc),
            TExpr::Reassign { ref loc, .. } => Some(loc),
            _ => None,
        }
    }
    
    pub fn typ(&self) -> TypeId {
        use self::TExpr::*;
        match *self {
            Integer { typ, .. }
            | Bool { typ, .. }
            | Panic { typ, .. }
            | Nil { typ, .. }
            | Let { typ, .. }
            | Assign { typ, .. }
            | While { typ, .. }
            | FunDec { typ, .. }
            | For { typ, .. }
            | TagVar { typ, .. }
            | Binary { typ, .. }
            | Access { typ, .. }
            | Unary { typ, .. }
            | If { typ, .. }
            | Call { typ, .. }
            | CallBuiltin { typ, .. }
            | Constructor { typ, .. }
            | Do { typ, .. }
            | Reassign { typ, .. }
            | Union { typ, .. }
            | UnwrapUnion  { typ, .. } => typ
        }
    }
    
    pub fn typ_mut(&mut self) -> &mut TypeId {
        use self::TExpr::*;
        match *self {
            Integer { ref mut typ, .. }
            | Bool { ref mut typ, .. }
            | Panic { ref mut typ, .. }
            | Nil { ref mut typ, .. }
            | Let { ref mut typ, .. }
            | Assign { ref mut typ, .. }
            | While { ref mut typ, .. }
            | FunDec { ref mut typ, .. }
            | For { ref mut typ, .. }
            | TagVar { ref mut typ, .. }
            | Binary { ref mut typ, .. }
            | Access { ref mut typ, .. }
            | Unary { ref mut typ, .. }
            | If { ref mut typ, .. }
            | Call { ref mut typ, .. }
            | CallBuiltin { ref mut typ, .. }
            | Constructor { ref mut typ, .. }
            | Do { ref mut typ, .. }
            | Reassign { ref mut typ, .. }
            | Union { ref mut typ, .. }
            | UnwrapUnion  { ref mut typ, .. } => typ
        }
    }
    
    pub fn span(&self) -> Span {
        use self::TExpr::*;
        match *self {
            Integer { span, .. }
            | Bool { span, .. }
            | Panic { span, .. }
            | Nil { span, .. }
            | Let { span, .. }
            | Assign { span, .. }
            | While { span, .. }
            | FunDec { span, .. }
            | For { span, .. }
            | TagVar { span, .. }
            | Binary { span, .. }
            | Access { span, .. }
            | Unary { span, .. }
            | If { span, .. }
            | Call { span, .. }
            | CallBuiltin { span, .. }
            | Constructor { span, .. }
            | Do { span, .. }
            | Reassign { span, .. }
            | Union { span, .. }
            | UnwrapUnion  { span, .. } => span
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Builtin {
    NewVec(bool),
    VecPush,
    VecPop,
    VecLen,
    GetIndex,
    SetIndex,
}

impl Builtin {
    pub fn fmt(&self) -> Cow<'static, str> {
        use self::Builtin::*;
        match *self {
            NewVec(is_obj_vec)  => format!("new_vec({})", is_obj_vec).into(),
            VecPush             => "vec_push".into(),
            VecPop              => "vec_pop".into(),
            VecLen              => "vec_len".into(),
            GetIndex            => "get_index".into(),
            SetIndex            => "set_index".into(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct TIterable {
    pub kind: TIterableKind,
    pub span: Span,
    pub item_type: TypeId,
}

#[derive(Debug, Clone)]
pub enum TIterableKind {
    Range(Box<TExpr>, Box<TExpr>),
    Expr(Box<TExpr>),
}

#[derive(Debug, Clone)]
pub struct TLoc {
    pub kind: TLocKind,
    pub span: Span,
    pub typ: TypeId,
}

impl TLoc {
    pub fn panic(loc: TLoc, msg: String, span: Span) -> TLoc {
        let loc_span = loc.span;
        // The type is known, but the usage is invalid.
        let typ = loc.typ.clone();
        TLoc {
            kind: TLocKind::Panic(Box::new(loc), msg, span),
            span: loc_span,
            typ: typ,
        }
    }
    
    pub fn empty() -> TLoc {
        TLoc {
            kind: TLocKind::Missing,
            span: Span::new(0, 0),
            typ: TYPE_PANIC,
        }
    }
    
    pub fn will_panic(&self) -> bool {
        match self.kind {
            TLocKind::Panic(_, _, _) => true,
            _ => false,
        }
    }
    
    pub fn span(&self) -> Span {
        self.span
    }
    
    pub fn typ(&self) -> TypeId {
        self.typ
    }
}

#[derive(Debug, Clone)]
pub enum TLocKind {
    Var(TIdent),
    Indexing(Box<TExpr>, Box<TExpr>),
    Field(Box<TExpr>, usize),
    Missing,
    Panic(Box<TLoc>, String, Span),
}

#[derive(Debug, Clone)]
pub struct TIdent {
    pub text: String,
    pub id: IdentId,
    pub span: Span,
}
