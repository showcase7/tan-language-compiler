use crate::util::Span;
use crate::ast::Builtin;

use std::rc::Rc;
use std::borrow::Cow;

#[derive(Debug, Clone)]
pub struct Label {
    pub context_name: Rc<Cow<'static, str>>,
    pub context_instance: usize,
    pub name: Rc<Cow<'static, str>>,
    pub id: usize,
    pub context_depth: usize,
}

impl Label {
    pub fn fmt(&self) -> String {
        if *self.name == "" {
            format!("{}[{}]", *self.context_name, self.context_instance)
        } else {
            format!("{}[{}].{}", *self.context_name, self.context_instance, *self.name)
        }
    }
}

#[derive(Debug, Clone)]
pub enum SymInstrKind {
    NonSym(Instr),
    Label(Label),
    Bnz(Label),
    Bez(Label),
    Jmp(Label),
    Call(Label, usize),
}

#[derive(Debug, Clone)]
pub struct SymInstr {
    pub kind: SymInstrKind,
    pub span: Span,
}

#[allow(unused)]
#[derive(Debug, Clone)]
pub enum Instr {
    PushI(i64),
    Alloc(usize),
    
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    
    Pop,
    Dealloc(usize),
    // Duplicates the top n values of the stack.
    Dup(usize),
    Swap,
    // Moves the top value n indices down the stack
    MoveDown(usize),
    // Allocates a struct of the given type and size on the heap.
    AllocStruct(i64, usize),
    
    Ldi(usize),
    // Pops a stack index and n values from the stack and stores the values
    // starting at that index.
    Sti(usize),
    // [.., ptr] Gets the value at field n of an object
    FieldGet(usize),
    // [.., ptr, value] Sets field n of an object to 'value' 
    // and pushes the old value [.., old_value]
    FieldSet(usize),
    
    Eq,
    Lt,
    Gt,
    
    RefIncr,
    RefDecr,
    
    Not,
    Neg,
    
    // Pop value, if value != 0: jump
    Bnz(i64),
    // Pop value, if value == 0: jump
    Bez(i64),
    // Jump
    Jmp(i64),
    
    GetBasePtr,
    // Pops n return values, a bp and a ret addr, jumps there and pushes the
    // return values back.
    Ret(usize),
    // Moves the top N values by 2 and puts [ret: pc+1, bp] at the
    // bottom, then jumps by the offset.
    Call(i64, usize),
    // Calls a builtin function.
    CallBuiltin(Builtin),
    
    Panic(String),
}

impl Instr {
    pub fn fmt(&self) -> Cow<'static, str> {
        use self::Instr::*;
        match *self {
            PushI(value) => format!("push {}", value).into(),
            Alloc(n) => format!("alloc {}", n).into(),
    
            Add => "add".into(),
            Sub => "sub".into(),
            Mul => "mul".into(),
            Div => "div".into(),
            Mod => "mod".into(),
    
            Pop => "pop".into(),
            Dealloc(n) => format!("dealloc {}", n).into(),
            Dup(n) => format!("dup {}", n).into(),
            Swap => "swap".into(),
            MoveDown(indices) => format!("move_down {}", indices).into(),
            AllocStruct(id, field_count) => format!("alloc_struct {} {}", id, field_count).into(),
    
            Ldi(n) => format!("ldi {}", n).into(),
            Sti(n) => format!("sti {}", n).into(),
            FieldSet(i) => format!("field_set {}", i).into(),
            FieldGet(i) => format!("field_get {}", i).into(),
    
            Eq => "eq".into(),
            Lt => "lt".into(),
            Gt => "gt".into(),
    
            RefIncr => "ref_incr".into(),
            RefDecr => "ref_decr".into(),
    
            Not => "not".into(),
            Neg => "neg".into(),
    
            // Pop value, if value != 0: jump
            Bnz(offset) => format!("bnz {}", offset).into(),
            // Pop value, if value == 0: jump
            Bez(offset) => format!("bez {}", offset).into(),
            // Jump
            Jmp(offset) => format!("jmp {}", offset).into(),
    
            GetBasePtr => "get_base_ptr".into(),
            // Pops a bp and a ret addr, jumps there
            Ret(n) => format!("ret {}", n).into(),
            // Moves the top N values by 2 and puts [ret: pc+1, bp] at the
            // bottom, then jumps by the offset.
            Call(offset, nargs) => format!("call {} ({} ret/arg slots)", offset, nargs).into(),
            // Calls a builtin function.
            CallBuiltin(builtin) => builtin.fmt().into(),
    
            Panic(ref msg) => format!("panic \"{}\"", msg).into(),
        }
    }
}


